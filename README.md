PeopleSdk_Php_Library
=====================



Description
-----------

Library contains base components, 
used in other SDK components.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
--------------------------------------------

1. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all standard rules, 
        added on its rule collection.
        
    - Validator rules implementation (or equivalent): 
    
        Validator provided on entities, 
        must contain all validator rules, 
        added on its rule collection.
        Each validator rule must use validator,
        with same implementation as validator provided on entities.
        
2. Library repository: liberty_code/http: version 1.0

    - Request factory implementation: 

        Default persistor provided on entity repository, 
        must be able to use request factory features, 
        allowing to provide HTTP request instances.

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root directory
    
    ```sh
    cd "<project_root_dir_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require people_sdk/library ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_dir_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_dir_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "people_sdk/library": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root directory.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

TODO

---



Test
----

#### Unit test

Unit tests allows to test components features, 
and to automate their validation.

1. Requirement
    
    - Composer
    
        It requires composer installation.
        For more information: https://getcomposer.org
        
    - Command: Dependencies installation
    
        ```sh
        php composer.phar install
        ```
    
2. Command: Run unit tests
    
    ```sh
    vendor\bin\phpunit
    ```

3. Note

    It uses PHPUnit to handle unit tests.
    For more information: https://phpunit.readthedocs.io

---


