<?php

namespace people_sdk\library\test\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use liberty_code\model\entity\library\ConstEntity;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;



/**
 *
 * @property null|integer $intAttrCritEqualId
 * @property null|string $strAttrCritEqualLogin
 * @property null|boolean $boolAttrCritIsBlocked
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortLogin
 */
class UserProfileBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute criteria equal id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'intAttrCritEqualId',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'crit-equal-id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria equal login
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrCritEqualLogin',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'crit-equal-login',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria is blocked
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'boolAttrCritIsBlocked',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'crit-is-blocked',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrSortId',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'sort-id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrSortLogin',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'sort-login',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = array(
            'intAttrCritEqualId' => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                    ]
                ]
            ],
            'strAttrCritEqualLogin' => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                        'error_message_pattern' => '%1$s is empty.'
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a string not empty.'
                    ]
                ]
            ],
            'boolAttrCritIsBlocked' => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-boolean' => [
                                [
                                    'type_boolean',
                                    [
                                        'integer_enable_require' => false,
                                        'string_enable_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a boolean.'
                    ]
                ]
            ],
            'strAttrSortId' => ToolBoxValidBrowserEntity::getTabSortRuleConfig(),
            'strAttrSortLogin' => ToolBoxValidBrowserEntity::getTabSortRuleConfig()
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'boolAttrCritIsBlocked':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}