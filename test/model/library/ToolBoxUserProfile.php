<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\model\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\test\model\model\permission\PermissionEntityFactory;
use people_sdk\library\test\model\model\user\UserProfileEntity;
use people_sdk\library\test\model\model\user\UserProfileEntityCollection;
use people_sdk\library\test\model\model\user\UserProfileEntityFactory;



class ToolBoxUserProfile extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified user profile entity collection,
     * from specified attribute values.
     *
     * Attribute values array format:
     * [
     *     intAttrId(optional): integer id,
     *
     *     strAttrLogin(optional): "string login",
     *
     *     strAttrEmail(optional): "string email",
     *
     *     objAttrProfileAttrEntity(optional): [
     *         strAttrName(optional): "string name",
     *         strAttrFirstName(optional): "string first name"
     *     ],
     *
     *     objAttrPermEntityCollection(optional): [
     *         // Permission 1
     *         [
     *             intAttrId(optional): integer id,
     *             strAttrKey(optional): "string key",
     *             boolAttrValue(optional): true / false
     *         ],
     *
     *         ...,
     *
     *         // Permission N
     *         [...]
     *     ]
     * ]
     *
     * @param UserProfileEntity $objUserProfileEntity
     * @param PermissionEntityFactory $objPermissionEntityFactory
     * @param array $tabValue
     */
    public static function hydrateUserProfileEntity(
        UserProfileEntity $objUserProfileEntity,
        PermissionEntityFactory $objPermissionEntityFactory,
        array $tabValue
    )
    {
        // Get attribute entity array of values
        $tabAttrEntityValue = null;
        if(array_key_exists('objAttrProfileAttrEntity', $tabValue))
        {
            $tabAttrEntityValue = $tabValue['objAttrProfileAttrEntity'];
            unset($tabValue['objAttrProfileAttrEntity']);
        }

        // Get permission entity collection array of values
        $tabPermEntityCollectionValue = null;
        if(array_key_exists('objAttrPermEntityCollection', $tabValue))
        {
            $tabPermEntityCollectionValue = $tabValue['objAttrPermEntityCollection'];
            unset($tabValue['objAttrPermEntityCollection']);
        }

        // Hydrate user profile entity
        $objUserProfileEntity->hydrate($tabValue);

        if(!is_null($tabAttrEntityValue))
        {
            $objUserProfileEntity->objAttrProfileAttrEntity->hydrate($tabAttrEntityValue);
        }

        if(!is_null($tabPermEntityCollectionValue))
        {
            $tabPermissionEntity = array_map(
                function($tabPermEntityCollectionValue) use ($objPermissionEntityFactory) {
                    return $objPermissionEntityFactory->getObjEntity($tabPermEntityCollectionValue);
                },
                $tabPermEntityCollectionValue
            );
            $objUserProfileEntity->objAttrPermEntityCollection->setTabItem($tabPermissionEntity);
        }
    }



    /**
     * Hydrate specified user profile entity collection,
     * from specified entity attribute values.
     *
     * Entity attribute values array format:
     * [
     *     // User profile 1
     *     [
     *         @see hydrateUserProfileEntity() attribute values array format
     *     ],
     *
     *     ...,
     *
     *     // User profile N
     *     [...]
     * ]
     *
     * @param UserProfileEntityCollection $objUserProfileEntityCollection
     * @param PermissionEntityFactory $objPermissionEntityFactory
     * @param UserProfileEntityFactory $objUserProfileEntityFactory
     * @param array $tabValue
     */
    public static function hydrateUserProfileEntityCollection(
        UserProfileEntityCollection $objUserProfileEntityCollection,
        PermissionEntityFactory $objPermissionEntityFactory,
        UserProfileEntityFactory $objUserProfileEntityFactory,
        array $tabValue
    )
    {
        // Init var
        $tabUserProfileEntity = array_map(
            function($tabValue) use (
                $objPermissionEntityFactory,
                $objUserProfileEntityFactory
            )
            {
                // Init var
                $result = $objUserProfileEntityFactory->getObjEntity();

                // Hydrate user profile entity
                static::hydrateUserProfileEntity(
                    $result,
                    $objPermissionEntityFactory,
                    $tabValue
                );

                // Return result
                return $result;
            },
            $tabValue
        );

        // Hydrate user profile entity collection
        $objUserProfileEntityCollection->setTabItem($tabUserProfileEntity);
    }



}