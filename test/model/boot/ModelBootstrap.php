<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../..';

// Load
require($strRootAppPath . '/requisition/request/info/factory/boot/SndInfoFactoryBootstrap.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;
use people_sdk\library\test\model\model\permission\PermissionEntityCollection;
use people_sdk\library\test\model\model\permission\PermissionEntityFactory;
use people_sdk\library\test\model\model\user\UserProfileEntityCollection;
use people_sdk\library\test\model\model\user\UserProfileEntityFactory;



// Init permission entity
$objPermissionEntityCollection = new PermissionEntityCollection();
$objPermissionEntityFactory = new PermissionEntityFactory(
    $objProvider,
    $objPermissionEntityCollection
);

$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'permission_entity_factory',
        'source' => PermissionEntityFactory::class,
        'set' =>  ['type' => 'instance', 'value' => $objPermissionEntityFactory],
        'option' => [
            'shared' => true
        ]
    )));



// Init user profile entity
$objUserProfileEntityCollection = new UserProfileEntityCollection();
$objUserProfileEntityFactory = new UserProfileEntityFactory(
    $objProvider,
    $objUserProfileEntityCollection
);

$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'user_profile_entity_factory',
        'source' => UserProfileEntityFactory::class,
        'set' =>  ['type' => 'instance', 'value' => $objUserProfileEntityCollection],
        'option' => [
            'shared' => true
        ]
    )));


