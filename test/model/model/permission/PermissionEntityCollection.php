<?php

namespace people_sdk\library\test\model\model\permission;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\library\test\model\model\permission\PermissionEntity;



/**
 * @method null|PermissionEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(PermissionEntity $objEntity) @inheritdoc
 */
class PermissionEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return PermissionEntity::class;
    }



}