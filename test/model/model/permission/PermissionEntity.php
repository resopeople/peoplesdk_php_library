<?php

namespace people_sdk\library\test\model\model\permission;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use people_sdk\library\model\entity\library\ToolBoxEntity;



/**
 * @property null|integer $intAttrId
 * @property null|string $strAttrKey
 * @property null|boolean $boolAttrValue
 */
class PermissionEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Return result
        return (
            (!is_null($result = $this->getAttributeValue('intAttrId'))) ?
                strval($result) :
                parent::getStrBaseKey()
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'intAttrId',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'id',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute key
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrKey',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'key',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute value
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'boolAttrValue',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'value',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



}