<?php

namespace people_sdk\library\test\model\model\user;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\library\test\model\model\user\UserProfileEntity;



/**
 * @method null|UserProfileEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(UserProfileEntity $objEntity) @inheritdoc
 */
class UserProfileEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return UserProfileEntity::class;
    }



}