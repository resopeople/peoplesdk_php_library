<?php

namespace people_sdk\library\test\model\model\user;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\test\model\model\permission\PermissionEntityCollection;
use people_sdk\library\test\model\model\permission\PermissionEntityFactory;
use people_sdk\library\test\model\model\user\attribute\UserProfileAttrEntity;



/**
 * @property null|integer $intAttrId
 * @property null|string $strAttrLogin
 * @property null|string $strAttrEmail
 * @property null|UserProfileAttrEntity $objAttrProfileAttrEntity
 * @property null|PermissionEntityCollection $objAttrPermEntityCollection
 */
class UserProfileEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Permission entity factory instance.
     * @var null|PermissionEntityFactory
     */
    protected $objPermissionEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionEntityFactory $objPermissionEntityFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        PermissionEntityFactory $objPermissionEntityFactory = null
    )
    {
        // Init properties
        $this->objPermissionEntityFactory = $objPermissionEntityFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Return result
        return (
            (!is_null($result = $this->getAttributeValue('intAttrId'))) ?
                strval($result) :
                parent::getStrBaseKey()
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'intAttrId',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'id',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute login
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrLogin',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'login',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute email
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrEmail',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'email',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile attribute entity
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'objAttrProfileAttrEntity',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'attribute',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new UserProfileAttrEntity()
            ],

            // Attribute permission entity collection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'objAttrPermEntityCollection',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'permission',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new PermissionEntityCollection()
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'objAttrProfileAttrEntity':
                /** @var UserProfileAttrEntity $value */
                $result = $value->getTabDataSave();
                break;

            case 'objAttrPermEntityCollection':
                /** @var PermissionEntityCollection $value */
                $result = ToolBoxEntity::getTabEntityCollectionDataSave($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'objAttrProfileAttrEntity':
                $result = null;
                if(is_array($value)) {
                    $result = new UserProfileAttrEntity();
                    $result->hydrateSave($value);
                }
                break;

            case 'objAttrPermEntityCollection':
                $result = null;
                if(!is_null($this->objPermissionEntityFactory) && is_array($value))
                {
                    $result = new PermissionEntityCollection();
                    ToolBoxEntity::hydrateSaveEntityCollection(
                        $result,
                        $this->objPermissionEntityFactory,
                        $value
                    );
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}