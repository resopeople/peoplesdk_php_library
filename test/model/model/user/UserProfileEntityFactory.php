<?php

namespace people_sdk\library\test\model\model\user;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\library\test\model\model\user\UserProfileEntity;



/**
 * @method UserProfileEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class UserProfileEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserProfileEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => 'intAttrId'
        );
    }



}