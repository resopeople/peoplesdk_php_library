<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\factory\model\HttpResponseFactory;
use people_sdk\library\requisition\response\library\ToolBoxResponse;



/**
 * @cover ToolBoxResponse
 */
class ResponseTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var HttpResponseFactory */
    public static $objHttpResponseFactory;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../..';
        require($strRootAppPath . '/vendor/liberty_code/http/test/requisition/response/factory/boot/HttpResponseFactoryBootstrap.php');

        // Init properties
        /** @var HttpResponseFactory $objHttpResponseFactory */
        static::$objHttpResponseFactory = $objHttpResponseFactory;
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can get response data.
     *
     * @param integer $intStatusCode
     * @param array $tabBody
     * @param array $tabExpectResult
     * @param array $tabExpectError
     * @param array $tabExpectWarn
     * @dataProvider providerGetResponseResultData
     * @dataProvider providerGetResponseErrorData
     */
    public function testCanGetResponseData(
        $intStatusCode,
        array $tabBody,
        array $tabExpectResult,
        array $tabExpectError,
        array $tabExpectWarn
    )
    {
        // Get response
        /** @var DataHttpResponse $objResponse */
        $objResponse = static::$objHttpResponseFactory->getObjResponse(array(
            'type' => 'http_data',
            'body_parser_config' => [
                'type' => 'string_table_json'
            ],
            'rcp_info' => [
                'status_code' => $intStatusCode,
                'header' => [
                    'Content-type' => 'application/json',
                ],
                'body' => json_encode($tabBody)
            ]
        ));

        // Set assertions (check result data)
        $tabResultData = ToolBoxResponse::getTabResultData($objResponse);
        $tabExpectResultData = $tabExpectResult[0];
        $this->assertEquals($tabExpectResultData, $tabResultData);
        $this->assertEquals((!is_null($tabResultData)), ToolBoxResponse::checkIsResult($objResponse));

        // Set assertions (check error data)
        $tabErrorData = ToolBoxResponse::getTabErrorData($objResponse);
        $tabExpectErrorData = $tabExpectError[0];
        $intExpectErrorCode = $tabExpectError[1];
        $strExpectErrorMsg = $tabExpectError[2];
        $this->assertEquals($tabExpectErrorData, $tabErrorData);
        $this->assertEquals((!is_null($tabErrorData)), ToolBoxResponse::checkIsError($objResponse));
        $this->assertEquals($intExpectErrorCode, ToolBoxResponse::getIntErrorCode($objResponse));
        $this->assertEquals($strExpectErrorMsg, ToolBoxResponse::getStrErrorMsg($objResponse));

        // Set assertions (check warning data)
        $tabWarnData = ToolBoxResponse::getTabWarnData($objResponse);
        $tabExpectWarnData = $tabExpectWarn[0];
        $this->assertEquals($tabExpectWarnData, $tabWarnData);
        $this->assertEquals((count($tabWarnData) > 0), ToolBoxResponse::checkHasWarning($objResponse));

        $tabWarnData = array_map(
            function(array $tabWarnData) {
                return array(
                    ToolBoxResponse::getIntWarnCodeFromData($tabWarnData),
                    ToolBoxResponse::getStrWarnMsgFromData($tabWarnData)
                );
            },
            $tabWarnData
        );
        $tabExpectWarnData = $tabExpectWarn[1];
        $this->assertEquals($tabExpectWarnData, $tabWarnData);

        // Print
        /*
        echo('Get result data: ' . PHP_EOL);var_dump($tabResultData);echo(PHP_EOL);
        echo('Get error data: ' . PHP_EOL);var_dump($tabErrorData);echo(PHP_EOL);
        echo('Get warning data: ' . PHP_EOL);var_dump($tabWarnData);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can get response result data.
     *
     * @return array
     */
    public function providerGetResponseResultData()
    {
        // Return result
        return array(
            'Get response data: fail to get data' => [
                200,
                [
                    'test'
                ],
                [
                    null
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: fail to get result data (invalid body data)' => [
                200,
                [
                    'result' => 'test'
                ],
                [
                    null
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: fail to get result data (invalid status code)' => [
                400,
                [
                    'result' => ['test']
                ],
                [
                    null
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: success to get result data' => [
                200,
                [
                    'result' => ['test']
                ],
                [
                    ['test']
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: success to get result data, with warning data' => [
                200,
                [
                    'result' => ['test'],
                    'warning' => [
                        'Warning invalid 1',
                        [
                            'code' => 400,
                            'message' => 'Warning message 1'
                        ],
                        [
                            'code' => 400,
                            'message' => 'Warning message 2'
                        ],
                        7,
                        [
                            'code' => 400,
                            'message' => 'Warning message 3'
                        ]
                    ]
                ],
                [
                    ['test']
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [
                        [
                            'code' => 400,
                            'message' => 'Warning message 1'
                        ],
                        [
                            'code' => 400,
                            'message' => 'Warning message 2'
                        ],
                        [
                            'code' => 400,
                            'message' => 'Warning message 3'
                        ]
                    ],
                    [
                        [
                            400,
                            'Warning message 1'
                        ],
                        [
                            400,
                            'Warning message 2'
                        ],
                        [
                            400,
                            'Warning message 3'
                        ]
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can get response error data.
     *
     * @return array
     */
    public function providerGetResponseErrorData()
    {
        // Return result
        return array(
            'Get response data: fail to get error data (invalid body data)' => [
                400,
                [
                    'error' => 'test'
                ],
                [
                    null
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: fail to get error data (invalid status code)' => [
                200,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Error message'
                    ]
                ],
                [
                    null
                ],
                [
                    null,
                    null,
                    null
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: success to get error data' => [
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Error message'
                    ]
                ],
                [
                    null
                ],
                [
                    [
                        'code' => 400,
                        'message' => 'Error message'
                    ],
                    400,
                    'Error message'
                ],
                [
                    [],
                    []
                ]
            ],
            'Get response data: success to get error data, with warning data' => [
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Error message'
                    ],
                    'warning' => [
                        'Warning invalid 1',
                        [
                            'code' => 400,
                            'message' => 'Warning message 1'
                        ],
                        [
                            'code' => 400,
                            'message' => 'Warning message 2'
                        ],
                        7,
                        [
                            'code' => 400,
                            'message' => 'Warning message 3'
                        ]
                    ]
                ],
                [
                    null
                ],
                [
                    [
                        'code' => 400,
                        'message' => 'Error message'
                    ],
                    400,
                    'Error message'
                ],
                [
                    [
                        [
                            'code' => 400,
                            'message' => 'Warning message 1'
                        ],
                        [
                            'code' => 400,
                            'message' => 'Warning message 2'
                        ],
                        [
                            'code' => 400,
                            'message' => 'Warning message 3'
                        ]
                    ],
                    [
                        [
                            400,
                            'Warning message 1'
                        ],
                        [
                            400,
                            'Warning message 2'
                        ],
                        [
                            400,
                            'Warning message 3'
                        ]
                    ]
                ]
            ]
        );
    }



}