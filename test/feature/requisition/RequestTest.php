<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\config\config\register\model\RegisterConfig;
use people_sdk\library\requisition\request\info\factory\multi\model\MultiSndInfoFactory;
use people_sdk\library\requisition\request\library\ToolBoxRequest;



/**
 * @cover ToolBoxRequest
 */
class RequestTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var RegisterConfig */
    public static $objConfig;



    /** @var MultiSndInfoFactory */
    public static $objSndInfoFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/requisition/request/info/factory/boot/SndInfoFactoryBootstrap.php');

        // Init properties
        /** @var RegisterConfig $objConfig */
        static::$objConfig = $objConfig;
        /** @var MultiSndInfoFactory $objMultiRequestSndInfoFactory */
        static::$objSndInfoFactory = $objMultiRequestSndInfoFactory;
    }



    public static function tearDownAfterClass(): void
    {
        // Clear registered items
        static::$objConfig->getObjRegister()->removeItemAll();
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can get URL.
     *
     * @param array $tabConfig
     * @param string $strRoute
     * @param null|array $tabRouteArg
     * @param null|array $tabRouteArgSpec
     * @param null|string|array $arg
     * @param null|string $expectResult
     * @dataProvider providerGetUrl
     */
    public function testCanGetUrl(
        array $tabConfig,
        $strRoute,
        $tabRouteArg,
        $tabRouteArgSpec,
        $arg,
        $expectResult
    )
    {
        // Init config
        static::$objConfig->getObjRegister()->hydrateItem(
            $tabConfig,
            true,
            true
        );

        // Set assertions
        $strResultUrl = ToolBoxRequest::getStrUrl(
            static::$objSndInfoFactory,
            $strRoute,
            $tabRouteArg,
            $tabRouteArgSpec,
            $arg
        );
        $this->assertEquals($expectResult, $strResultUrl);

        // Print
        /*
        echo('Get result URL: ' . PHP_EOL);var_dump($strResultUrl);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can get URL.
     *
     * @return array
     */
    public function providerGetUrl()
    {
        // Return result
        return array(
            'Get request URL: fail to get URL, without configuration' => [
                [],
                '/path/to/resource',
                null,
                null,
                null,
                null
            ],
            'Get request URL: fail to get URL, with configuration' => [
                [
                    'snd_info' => [
                        'url_schema' => 'https'
                    ]
                ],
                '/path/to/resource',
                null,
                null,
                null,
                null
            ],
            'Get request URL: success to get URL, with minimal configuration' => [
                [
                    'snd_info' => [
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com'
                    ]
                ],
                '/path/to/resource',
                null,
                null,
                null,
                'https://www.test.com/path/to/resource'
            ],
            'Get request URL: success to get URL, with some configuration' => [
                [
                    'snd_info' => [
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/app%1$s'
                    ]
                ],
                '/path/to/resource',
                null,
                null,
                'url_argument' => [
                    'arg1' => 'value1',
                    'arg2' => 'value2',
                    'arg3' => 'value3'
                ],
                'https://www.test.com/app/path/to/resource?arg1=value1&arg2=value2&arg3=value3'
            ]
        );
    }



}