<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use DateTimeZone;
use liberty_code\config\config\register\model\RegisterConfig;
use liberty_code\http\multipart\library\ToolBoxMultipart;
use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;
use people_sdk\library\table\library\ToolBoxTable;
use people_sdk\library\requisition\request\authentication\library\ToolBoxBasicAuthentication;
use people_sdk\library\requisition\request\authentication\library\ToolBoxApiKeyAuthentication;
use people_sdk\library\requisition\request\authentication\library\ToolBoxBearerAuthentication;
use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\request\info\factory\model\DefaultSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\config\exception\SrcConfigValueInvalidFormatException;
use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\config\base\model\BaseConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\multi\model\MultiSndInfoFactory;



/**
 * @cover ToolBoxTable
 * @cover ToolBoxBasicAuthentication
 * @cover ToolBoxApiKeyAuthentication
 * @cover ToolBoxBearerAuthentication
 * @cover ToolBoxSndInfo
 * @cover SndInfoFactoryInterface
 * @cover DefaultSndInfoFactory
 * @cover DefaultConfigSndInfoFactory
 * @cover BaseConfigSndInfoFactory
 * @cover MultiSndInfoFactory
 */
class RequestSndInfoFactoryTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var RegisterConfig */
    public static $objConfig;



    /** @var BaseConfigSndInfoFactory */
    public static $objSndInfoFactory1;



    /** @var MultiSndInfoFactory */
    public static $objMultiSndInfoFactory;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/requisition/request/info/factory/boot/SndInfoFactoryBootstrap.php');

        // Init properties
        /** @var RegisterConfig $objConfig */
        static::$objConfig = $objConfig;
        /** @var BaseConfigSndInfoFactory $objRequestSndInfoFactory1 */
        static::$objSndInfoFactory1 = $objRequestSndInfoFactory1;
        /** @var MultiSndInfoFactory $objMultiRequestSndInfoFactory */
        static::$objMultiSndInfoFactory = $objMultiRequestSndInfoFactory;
    }



    public static function tearDownAfterClass(): void
    {
        // Clear registered items
        static::$objConfig->getObjRegister()->removeItemAll();
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can get new or specified sending information array.
     *
     * @param array $tabConfig
     * @param string $strSndInfoFactoryNm
     * @param null|array $tabInfo
     * @param string|array $expectResult
     * @dataProvider providerGetSndInfoFromConfigSndInfoFactory
     * @dataProvider providerGetSndInfoFromMultiSndInfoFactory
     */
    public function testCanGetSndInfo(
        array $tabConfig,
        $strSndInfoFactoryNm,
        $tabInfo,
        $expectResult
    )
    {
        // Init var
        /** @var SndInfoFactoryInterface $objSndInfoFactory */
        $objSndInfoFactory = static::${$strSndInfoFactoryNm};
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init config
        static::$objConfig->getObjRegister()->hydrateItem(
            $tabConfig,
            true,
            true
        );

        // Get sending information array
        $tabInfo = $objSndInfoFactory->getTabSndInfo($tabInfo);

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertions
            $this->assertEquals($expectResult, $tabInfo);

            // Print
            /*
            echo('Get sending info: ' . PHP_EOL);var_dump($tabInfo);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can get new or specified sending information array,
     * from configuration sending information factory.
     *
     * @return array
     */
    public function providerGetSndInfoFromConfigSndInfoFactory()
    {
        // Init var
        $strBoundary = ToolBoxMultipart::getStrNewBoundary();

        // Return result
        return array(
            'Get request sending information (from config sending info factory): 
            fail to get sending information (config invalid: timezone name invalid)' => [
                [
                    'dt_tz_nm' => new DateTimeZone('America/New_York'),
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'content_type' => ToolBoxSndInfo::getStrContentTypeMultipartForm($strBoundary)
                ],
                'objSndInfoFactory1',
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET'
                ],
                SrcConfigValueInvalidFormatException::class
            ],
            'Get request sending information (from config sending info factory): 
            fail to get sending information (config invalid: content type boundary invalid)' => [
                [
                    'dt_tz_nm' => new DateTimeZone('America/New_York'),
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'content_type' => ToolBoxSndInfo::getStrContentTypeMultipartForm(' ' . $strBoundary)
                ],
                'objSndInfoFactory1',
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET'
                ],
                SrcConfigValueInvalidFormatException::class
            ],
            'Get request sending information (from config sending info factory): 
            fail to get sending information (sending information invalid)' => [
                [
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'content_type' => ToolBoxSndInfo::getStrContentTypeMultipartForm($strBoundary)
                ],
                'objSndInfoFactory1',
                [
                    'url' => 'https://www.test.com',
                    'method' => 7
                ],
                SndInfoInvalidFormatException::class
            ],
            'Get request sending information (from config sending info factory): 
            success to get sending information' => [
                [
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'content_type' => ToolBoxSndInfo::getStrContentTypeMultipartForm($strBoundary)
                ],
                'objSndInfoFactory1',
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET'
                ],
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET',
                    'header' => [
                        'Content-Type' => ToolBoxSndInfo::getStrContentTypeMultipartForm($strBoundary)
                    ],
                    'url_argument' => [
                        'dt-config-get-tz-name' => 'America/New_York',
                        'dt-config-get-dt-format' => 'd/m/Y H:i:s.u (e)',
                        'dt-config-set-tz-name' => 'America/New_York',
                        'dt-config-set-dt-format' => 'd/m/Y H:i:s.u (e)'
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can get new or specified sending information array,
     * from multi sending information factory.
     *
     * @return array
     */
    public function providerGetSndInfoFromMultiSndInfoFactory()
    {
        // Return result
        return array(
            'Get request sending information (from multi sending info factory): 
            fail to get sending information (config invalid: invalid authentication type)' => [
                [
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'spc_nm' => 'space_test',
                    'spc_auth_key' => 'space_auth_test',
                    'spc_nm_2' => 'space_test_2',
                    'spc_auth_key_2' => 'space_auth_test_2',
                    'auth_type' => 'test',
                    'auth_identifier' => 'user_test',
                    'auth_secret' => 'pw_test'
                ],
                'objMultiSndInfoFactory',
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET'
                ],
                SrcConfigValueInvalidFormatException::class
            ],
            'Get request sending information (from multi sending info factory): 
            success to get sending information (with valid user basic authentication)' => [
                [
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'spc_nm' => 'space_test',
                    'spc_auth_key' => 'space_auth_test',
                    'spc_nm_2' => 'space_test_2',
                    'spc_auth_key_2' => 'space_auth_test_2',
                    'auth_type' => 'user_basic',
                    'auth_identifier' => 'user_test',
                    'auth_secret' => 'pw_test'
                ],
                'objMultiSndInfoFactory',
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET'
                ],
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET',
                    'header' => [
                        'Space-Connection-Name' => 'space_test',
                        'Space-Connection-Key' => 'space_auth_test',
                        'Authentication-Type' => 'user_basic',
                        'Authorization' => ToolBoxBasicAuthentication::getStrBasic(
                            'user_test',
                            'pw_test'
                        )
                    ],
                    'url_argument' => [
                        'dt-config-get-tz-name' => 'America/New_York',
                        'dt-config-get-dt-format' => 'd/m/Y H:i:s.u (e)',
                        'dt-config-set-tz-name' => 'America/New_York',
                        'dt-config-set-dt-format' => 'd/m/Y H:i:s.u (e)'
                    ]
                ]
            ],
            'Get request sending information (from multi sending info factory): 
            success to get sending information (with valid application API key authentication)' => [
                [
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'spc_nm' => 'space_test',
                    'spc_auth_key' => 'space_auth_test',
                    'auth_type' => 'app_api_key',
                    'auth_identifier' => 'app_api_key_test',
                    'auth_secret' => 'app_secret_test'
                ],
                'objMultiSndInfoFactory',
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET'
                ],
                [
                    'url' => 'https://www.test.com',
                    'method' => 'GET',
                    'header' => [
                        'Space-Connection-Name' => 'space_test',
                        'Space-Connection-Key' => 'space_auth_test',
                        'Authentication-Type' => 'app_api_key',
                        'Api-Key' => ToolBoxApiKeyAuthentication::getStrApiKey('app_api_key_test')
                    ],
                    'url_argument' => [
                        'dt-config-get-tz-name' => 'America/New_York',
                        'dt-config-get-dt-format' => 'd/m/Y H:i:s.u (e)',
                        'dt-config-set-tz-name' => 'America/New_York',
                        'dt-config-set-dt-format' => 'd/m/Y H:i:s.u (e)'
                    ]
                ]
            ],
            'Get request sending information (from multi sending info factory): 
            fail to get sending information (config invalid: invalid identifier)' => [
                [
                    'snd_info' => [
                        'url_schema' => 'https',
                        'url_host' => '%s.test.com',
                        'url_route' => '/app%1$s'
                    ],
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'spc_nm' => 'space_test',
                    'spc_auth_key' => 'space_auth_test',
                    'auth_type' => 'user_bearer',
                    'auth_identifier' => 7
                ],
                'objMultiSndInfoFactory',
                [
                    'url_host' => 'sub-domain',
                    'url_route' => '/module/entity',
                    'method' => 'GET'
                ],
                SrcConfigValueInvalidFormatException::class
            ],
            'Get request sending information (from multi sending info factory): 
            success to get sending information (with valid user bearer authentication)' => [
                [
                    'snd_info' => [
                        'url_schema' => 'https',
                        'url_host' => '%s.test.com',
                        'url_route' => '/app%1$s'
                    ],
                    'dt_tz_nm' => 'America/New_York',
                    'dt_format' => 'd/m/Y H:i:s.u (e)',
                    'spc_nm' => 'space_test',
                    'spc_auth_key' => 'space_auth_test',
                    'auth_type' => 'user_bearer',
                    'auth_identifier' => 'user_token_test'
                ],
                'objMultiSndInfoFactory',
                [
                    'url_host' => 'sub-domain',
                    'url_route' => '/module/entity',
                    'method' => 'GET'
                ],
                [
                    'url_schema' => 'https',
                    'url_host' => 'sub-domain.test.com',
                    'url_route' => '/app/module/entity',
                    'method' => 'GET',
                    'header' => [
                        'Space-Connection-Name' => 'space_test',
                        'Space-Connection-Key' => 'space_auth_test',
                        'Authentication-Type' => 'user_bearer',
                        'Authorization' => ToolBoxBearerAuthentication::getStrBearer('user_token_test')
                    ],
                    'url_argument' => [
                        'dt-config-get-tz-name' => 'America/New_York',
                        'dt-config-get-dt-format' => 'd/m/Y H:i:s.u (e)',
                        'dt-config-set-tz-name' => 'America/New_York',
                        'dt-config-set-dt-format' => 'd/m/Y H:i:s.u (e)'
                    ]
                ]
            ]
        );
    }



}