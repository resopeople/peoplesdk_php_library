<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\browser;

use PHPUnit\Framework\TestCase;

use liberty_code\validation\validator\standard\model\StandardValidator;
use people_sdk\library\browser\library\ConstBrowser;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\library\browser\model\DefaultBrowserEntity;
use people_sdk\library\test\browser\model\UserProfileBrowserEntity;



/**
 * @cover ToolBoxValidBrowserEntity
 * @cover DefaultBrowserEntity
 */
class BrowserEntityTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var StandardValidator */
    public static $objValidator;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/../vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

        // Init properties
        /** @var StandardValidator $objValidator */
        static::$objValidator = $objValidator;
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can use browser entity object.
     *
     * @param array $tabValue
     * @param array $tabExpectResult
     * @dataProvider providerUseBrowserEntity
     */
    public function testCanUseBrowserEntity(
        array $tabValue,
        array $tabExpectResult
    )
    {
        // Get hydrated browser entity
        $objBrowserEntity = new UserProfileBrowserEntity(
            array(),
            static::$objValidator
        );
        $objBrowserEntity->hydrate($tabValue);

        // Validate browser entity
        $tabError = array();
        $boolValid = $objBrowserEntity->checkAttributeValid(null, $tabError);

        // Get browser entity selection query, if required
        $tabQuery = (
            $boolValid ?
                $objBrowserEntity->getTabQuery() :
                array()
        );

        // Set assertions (Check browser entity validation)
        $boolExpectValid = $tabExpectResult[0];
        $tabExpectError = $tabExpectResult[1];
        $this->assertEquals($boolExpectValid, $boolValid);
        $this->assertEquals($tabExpectError, $tabError);

        // Set assertions (Check browser entity selection query)
        $tabExpectQuery = $tabExpectResult[2];
        $this->assertEquals($tabExpectQuery, $tabQuery);

        // Print
        /*
        echo('Check valid: ' . PHP_EOL);var_dump($boolValid);echo(PHP_EOL);
        echo('Get errors: ' . PHP_EOL);var_dump($tabError);echo(PHP_EOL);
        echo('Get query: ' . PHP_EOL);var_dump($tabQuery);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can use browser entity object.
     *
     * @return array
     */
    public function providerUseBrowserEntity()
    {
        // Return result
        return array(
            'Use browser entity: success to validate and get selection query (with all values, with bool formatting)' => [
                [
                    'intAttrCritEqualId' => 1,
                    'strAttrCritEqualLogin' => 'user-1',
                    'boolAttrCritIsBlocked' => false,
                    'strAttrSortId' => ConstBrowser::SORT_ASC,
                    'strAttrSortLogin' => ConstBrowser::SORT_DESC
                ],
                [
                    true,
                    [],
                    [
                        'crit-equal-id' => 1,
                        'crit-equal-login' => 'user-1',
                        'crit-is-blocked' => 0,
                        'sort-id' => ConstBrowser::SORT_ASC,
                        'sort-login' => ConstBrowser::SORT_DESC
                    ]
                ]
            ],
            'Use browser entity: success to validate and get selection query (with bool formatting)' => [
                [
                    'intAttrCritEqualId' => 1,
                    'boolAttrCritIsBlocked' => true,
                    'strAttrSortId' => null,
                    'strAttrSortLogin' => ConstBrowser::SORT_DESC
                ],
                [
                    true,
                    [],
                    [
                        'crit-equal-id' => 1,
                        'crit-is-blocked' => 1,
                        'sort-login' => ConstBrowser::SORT_DESC
                    ]
                ]
            ],
            'Use browser entity: fail to validate (invalid sort values)' => [
                [
                    'intAttrCritEqualId' => 1,
                    'boolAttrCritIsBlocked' => true,
                    'strAttrSortId' => 7,
                    'strAttrSortLogin' => 'test'
                ],
                [
                    false,
                    [
                        'strAttrSortId' => [
                            'group_sub_rule_or' => sprintf(
                                '%1$s must be null or a valid sort string (\'%2$s\', \'%3$s\').',
                                'strAttrSortId',
                                ConstBrowser::SORT_ASC,
                                ConstBrowser::SORT_DESC
                            )
                        ],
                        'strAttrSortLogin' => [
                            'group_sub_rule_or' => sprintf(
                                '%1$s must be null or a valid sort string (\'%2$s\', \'%3$s\').',
                                'strAttrSortLogin',
                                ConstBrowser::SORT_ASC,
                                ConstBrowser::SORT_DESC
                            )
                        ]
                    ],
                    []
                ]
            ]
        );
    }



}