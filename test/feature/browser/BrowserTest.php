<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\browser;

use PHPUnit\Framework\TestCase;

use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\factory\model\HttpResponseFactory;
use people_sdk\library\requisition\response\library\ToolBoxResponse;
use people_sdk\library\browser\requisition\response\library\ToolBoxResponseBrowser;



/**
 * @cover ToolBoxResponse
 * @cover ToolBoxResponseBrowser
 */
class BrowserTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var HttpResponseFactory */
    public static $objHttpResponseFactory;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../..';
        require($strRootAppPath . '/vendor/liberty_code/http/test/requisition/response/factory/boot/HttpResponseFactoryBootstrap.php');

        // Init properties
        /** @var HttpResponseFactory $objHttpResponseFactory */
        static::$objHttpResponseFactory = $objHttpResponseFactory;
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can get response browser data.
     *
     * @param integer $intStatusCode
     * @param array $tabBody
     * @param array $tabExpectResult
     * @dataProvider providerGetResponseBrowserData
     */
    public function testCanGetResponseBrowserData(
        $intStatusCode,
        array $tabBody,
        array $tabExpectResult
    )
    {
        // Get response
        /** @var DataHttpResponse $objResponse */
        $objResponse = static::$objHttpResponseFactory->getObjResponse(array(
            'type' => 'http_data',
            'body_parser_config' => [
                'type' => 'string_table_json'
            ],
            'rcp_info' => [
                'status_code' => $intStatusCode,
                'header' => [
                    'Content-type' => 'application/json',
                ],
                'body' => json_encode($tabBody)
            ]
        ));

        // Set assertions (check browser data)
        $tabBrowserData = ToolBoxResponseBrowser::getTabBrowserData($objResponse);
        $tabExpectBrowserData = $tabExpectResult[0];
        $intExpectItemCount = $tabExpectResult[1];
        $intExpectItemCountPerPage = $tabExpectResult[2];
        $intExpectItemCountOnActivePage = $tabExpectResult[3];
        $intExpectPageCount = $tabExpectResult[4];
        $intExpectActivePageIndex = $tabExpectResult[5];
        $this->assertEquals($tabExpectBrowserData, $tabBrowserData);
        $this->assertEquals($intExpectItemCount, ToolBoxResponseBrowser::getIntItemCount($objResponse));
        $this->assertEquals($intExpectItemCountPerPage, ToolBoxResponseBrowser::getIntItemCountPerPage($objResponse));
        $this->assertEquals($intExpectItemCountOnActivePage, ToolBoxResponseBrowser::getIntItemCountOnActivePage($objResponse));
        $this->assertEquals($intExpectPageCount, ToolBoxResponseBrowser::getIntPageCount($objResponse));
        $this->assertEquals($intExpectActivePageIndex, ToolBoxResponseBrowser::getIntActivePageIndex($objResponse));

        // Print
        /*
        echo('Get browser data: ' . PHP_EOL);var_dump($tabBrowserData);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can get response browser data.
     *
     * @return array
     */
    public function providerGetResponseBrowserData()
    {
        // Return result
        return array(
            'Get response browser data: fail to get data (invalid body result data)' => [
                200,
                [
                    'test'
                ],
                [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ]
            ],
            'Get response browser data: fail to get data (invalid body browser data)' => [
                200,
                [
                    'result' => [
                        'browsing' => 'test'
                    ]
                ],
                [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ]
            ],
            'Get response browser data: success to get data (with invalid sub-data)' => [
                200,
                [
                    'result' => [
                        'browsing' => [
                            'item_count' => -7,
                            'item_count_page' => 0,
                            'item_count_active_page' => 'test',
                            'page_count' => -1,
                            'page_index' => true,
                            'test'
                        ]
                    ]
                ],
                [
                    [
                        'item_count' => -7,
                        'item_count_page' => 0,
                        'item_count_active_page' => 'test',
                        'page_count' => -1,
                        'page_index' => true,
                        'test'
                    ],
                    null,
                    null,
                    null,
                    null,
                    null
                ]
            ],
            'Get response browser data: success to get data' => [
                200,
                [
                    'result' => [
                        'browsing' => [
                            'item_count' => 70,
                            'item_count_page' => 30,
                            'item_count_active_page' => 30,
                            'page_count' => 3,
                            'page_index' => 0,
                            'test'
                        ]
                    ]
                ],
                [
                    [
                        'item_count' => 70,
                        'item_count_page' => 30,
                        'item_count_active_page' => 30,
                        'page_count' => 3,
                        'page_index' => 0,
                        'test'
                    ],
                    70,
                    30,
                    30,
                    3,
                    0
                ]
            ]
        );
    }



}