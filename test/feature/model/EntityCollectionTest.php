<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\model;

use PHPUnit\Framework\TestCase;

use people_sdk\library\model\entity\requisition\response\library\ToolBoxResponseEntityCollection;



/**
 * @cover ToolBoxResponseEntityCollection
 */
class EntityCollectionTest extends TestCase
{
    // Methods test
    // ******************************************************************************

    /**
     * Test can get response entity collection data.
     *
     * @param array $tabCollectionData
     * @param array $tabExpectResult
     * @dataProvider providerGetResponseEntityCollectionData
     */
    public function testCanGetEntityCollectionData(
        array $tabCollectionData,
        array $tabExpectResult
    )
    {
        // Set assertions (check browser data)
        $intEntityCount = ToolBoxResponseEntityCollection::getIntEntityCountFromData($tabCollectionData);
        $tabEntityData = ToolBoxResponseEntityCollection::getTabEntityDataFromData($tabCollectionData);
        $intExpectEntityCount = $tabExpectResult[0];
        $tabExpectEntityData = $tabExpectResult[1];
        $this->assertEquals($intEntityCount, $intExpectEntityCount);
        $this->assertEquals($tabEntityData, $tabExpectEntityData);

        // Print
        /*
        echo('Get entity count: ' . PHP_EOL);var_dump($intEntityCount);echo(PHP_EOL);
        echo('Get entity data: ' . PHP_EOL);var_dump($tabEntityData);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can get response entity collection data.
     *
     * @return array
     */
    public function providerGetResponseEntityCollectionData()
    {
        // Return result
        return array(
            'Get response entity collection data: fail to get count and data (not found)' => [
                [
                    'test'
                ],
                [
                    null,
                    []
                ]
            ],
            'Get response entity collection data: fail to get count and data (invalid format)' => [
                [
                    'count' => 'test',
                    'data' => 'test'
                ],
                [
                    null,
                    []
                ]
            ],
            'Get response entity collection data: success to get count and data' => [
                [
                    'count' => 3,
                    'data' => [
                        [
                            'key-1' => 'Value 1 1',
                            'key-2' => 'Value 1 2',
                            'key-3' => 'Value 1 3'
                        ],
                        'test',
                        [
                            'key-1' => 'Value 2 1',
                            'key-2' => 'Value 2 2',
                            'key-3' => 'Value 2 3'
                        ],
                        true,
                        [
                            'key-1' => 'Value 3 1',
                            'key-2' => 'Value 3 2',
                            'key-3' => 'Value 3 3'
                        ],
                        7
                    ]
                ],
                [
                    3,
                    [
                        [
                            'key-1' => 'Value 1 1',
                            'key-2' => 'Value 1 2',
                            'key-3' => 'Value 1 3'
                        ],
                        [
                            'key-1' => 'Value 2 1',
                            'key-2' => 'Value 2 2',
                            'key-3' => 'Value 2 3'
                        ],
                        [
                            'key-1' => 'Value 3 1',
                            'key-2' => 'Value 3 2',
                            'key-3' => 'Value 3 3'
                        ]
                    ]
                ]
            ]
        );
    }



}