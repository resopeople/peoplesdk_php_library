<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\model;

use PHPUnit\Framework\TestCase;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\DefaultItemEntity;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\factory\model\HttpResponseFactory;
use people_sdk\library\requisition\response\library\ToolBoxResponse;
use people_sdk\library\model\entity\requisition\response\library\ToolBoxResponseEntityError;
use people_sdk\library\model\entity\requisition\response\library\ToolBoxResponseValidEntity;
use people_sdk\library\model\entity\requisition\response\exception\ValidEntityFailException;
use people_sdk\library\model\entity\requisition\response\exception\ValidEntityCollectionFailException;
use people_sdk\library\test\model\library\ToolBoxUserProfile;
use people_sdk\library\test\model\model\permission\PermissionEntityFactory;
use people_sdk\library\test\model\model\user\UserProfileEntity;
use people_sdk\library\test\model\model\user\UserProfileEntityCollection;
use people_sdk\library\test\model\model\user\UserProfileEntityFactory;



/**
 * @cover ToolBoxResponse
 * @cover ToolBoxResponseEntityError
 * @cover ToolBoxResponseValidEntity
 */
class ValidEntityTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var HttpResponseFactory */
    public static $objHttpResponseFactory;



    /** @var PermissionEntityFactory */
    public static $objPermissionEntityFactory;



    /** @var UserProfileEntityFactory */
    public static $objUserProfileEntityFactory;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../..';
        require($strRootAppPath . '/vendor/liberty_code/http/test/requisition/response/factory/boot/HttpResponseFactoryBootstrap.php');
        $strRootAppPath = dirname(__FILE__) . '/../../..';
        require($strRootAppPath . '/test/model/boot/ModelBootstrap.php');

        // Init properties
        /** @var HttpResponseFactory $objHttpResponseFactory */
        static::$objHttpResponseFactory = $objHttpResponseFactory;
        /** @var PermissionEntityFactory $objPermissionEntityFactory */
        static::$objPermissionEntityFactory = $objPermissionEntityFactory;
        /** @var UserProfileEntityFactory $objUserProfileEntityFactory */
        static::$objUserProfileEntityFactory = $objUserProfileEntityFactory;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set entity error assertions.
     *
     * @param UserProfileEntity $objEntity
     * @param array $tabError
     * @param array $tabExpectError
     */
    protected function setEntityErrorAssert(
        UserProfileEntity $objEntity,
        array $tabError,
        array $tabExpectError
    )
    {
        // Init var
        $tabAttrEntityError = null;
        if(array_key_exists('objAttrProfileAttrEntity', $tabError))
        {
            $tabAttrEntityError = $tabError['objAttrProfileAttrEntity'];
            unset($tabError['objAttrProfileAttrEntity']);
        }

        $tabPermEntityCollectionError = null;
        if(array_key_exists('objAttrPermEntityCollection', $tabError))
        {
            $tabPermEntityCollectionError = $tabError['objAttrPermEntityCollection'];
            unset($tabError['objAttrPermEntityCollection']);
        }

        $tabExpectAttrEntityError = null;
        if(array_key_exists('objAttrProfileAttrEntity', $tabExpectError))
        {
            $tabExpectAttrEntityError = $tabExpectError['objAttrProfileAttrEntity']['validation_entity'];
            unset($tabExpectError['objAttrProfileAttrEntity']);
        }

        $tabExpectPermEntityCollectionError = null;
        if(array_key_exists('objAttrPermEntityCollection', $tabExpectError))
        {
            $tabExpectPermEntityCollectionError = $tabExpectError['objAttrPermEntityCollection']['validation_entity_collection'];
            unset($tabExpectError['objAttrPermEntityCollection']);
        }

        // Set user profile entity assertion
        $this->assertEquals($tabExpectError, $tabError);

        // Set attribute entity assertions
        if(!is_null($tabExpectAttrEntityError))
        {
            $this->assertEquals(
                true,
                (
                    isset($tabAttrEntityError['validation_entity']) &&
                    is_object($tabAttrEntityError['validation_entity']) &&
                    ($tabAttrEntityError['validation_entity'] instanceof ValidEntityFailException)
                )
            );

            /** @var ValidEntityFailException $objAttrEntityException */
            $objAttrEntityException = $tabAttrEntityError['validation_entity'];
            $this->assertEquals(
                $objEntity->objAttrProfileAttrEntity,
                $objAttrEntityException->getObjEntity()
            );

            $this->assertEquals(
                $tabExpectAttrEntityError,
                $objAttrEntityException->getTabError()
            );
        }

        // Set permission entity collection assertions
        if(!is_null($tabExpectPermEntityCollectionError))
        {
            $this->assertEquals(
                true,
                (
                    isset($tabPermEntityCollectionError['validation_entity_collection']) &&
                    is_object($tabPermEntityCollectionError['validation_entity_collection']) &&
                    ($tabPermEntityCollectionError['validation_entity_collection'] instanceof ValidEntityCollectionFailException)
                )
            );

            /** @var ValidEntityCollectionFailException $objPermEntityCollectionException */
            $objPermEntityCollectionException = $tabPermEntityCollectionError['validation_entity_collection'];
            $this->assertEquals(
                $objEntity->objAttrPermEntityCollection,
                $objPermEntityCollectionException->getObjEntityCollection()
            );

            $this->assertEquals(
                $tabExpectPermEntityCollectionError,
                $objPermEntityCollectionException->getTabError()
            );
        }
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can check if entity is valid,
     * from response error data.
     *
     * @param array $tabValue
     * @param integer $intStatusCode
     * @param array $tabBody
     * @param null|string|string[] $attrKey
     * @param array $tabExpectResult
     * @dataProvider providerCheckEntityIsValidFromResponseErrorData
     */
    public function testCanCheckEntityIsValidFromResponseErrorData(
        array $tabValue,
        $intStatusCode,
        array $tabBody,
        $attrKey,
        array $tabExpectResult
    )
    {
        // Get response
        /** @var DataHttpResponse $objResponse */
        $objResponse = static::$objHttpResponseFactory->getObjResponse(array(
            'type' => 'http_data',
            'body_parser_config' => [
                'type' => 'string_table_json'
            ],
            'rcp_info' => [
                'status_code' => $intStatusCode,
                'header' => [
                    'Content-type' => 'application/json',
                ],
                'body' => json_encode($tabBody)
            ]
        ));

        // Get entity
        $objEntity = static::$objUserProfileEntityFactory->getObjEntity();
        ToolBoxUserProfile::hydrateUserProfileEntity(
            $objEntity,
            static::$objPermissionEntityFactory,
            $tabValue
        );

        // Check entity is valid
        $tabError = array();
        $boolValid = ToolBoxResponseValidEntity::checkEntityIsValid(
            $objEntity,
            $objResponse,
            $tabError,
            $attrKey
        );

        // Set assertions (check validation)
        $boolExpectValid = $tabExpectResult[0];
        $this->assertEquals($boolExpectValid, $boolValid);

        // Set assertions (check error)
        $tabExpectError = $tabExpectResult[1];
        $this->setEntityErrorAssert($objEntity, $tabError, $tabExpectError);

        // Print
        /*
        echo('Check: '. PHP_EOL);var_dump($boolValid);echo(PHP_EOL);
        // echo('Get error: ' . PHP_EOL);var_dump($tabError);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can check if entity is valid,
     * from response error data.
     *
     * @return array
     */
    public function providerCheckEntityIsValidFromResponseErrorData()
    {
        // Init var
        $tabValue = array(
            'intAttrId' => 1,
            'strAttrLogin' => 'user-lg-1',
            'strAttrEmail' => 'user-1@test.com',
            'objAttrProfileAttrEntity' => [
                'strAttrName' => 'user-nm-1',
                'strAttrFirstName' => 'user-fnm-1'
            ],
            'objAttrPermEntityCollection' => [
                [
                    'intAttrId' => 1,
                    'strAttrKey' => 'perm-key-1',
                    'boolAttrValue' => true
                ],
                [
                    'intAttrId' => 2,
                    'strAttrKey' => 'perm-key-2',
                    'boolAttrValue' => false
                ],
                [
                    'intAttrId' => 3,
                    'strAttrKey' => 'perm-key-3',
                    'boolAttrValue' => true
                ]
            ]
        );
        $tabBodyErrorData = array(
            'login' => [
                'value' => 'user-lg-1',
                'error' => [
                    'rule_1' => 'error rule 1',
                    'rule_2' => 'error rule 2',
                    'rule_3' => 'error rule 3'
                ]
            ],
            'email' => [
                'value' => 'user-1@test.com',
                'error' => [
                    'rule_1' => 'error rule 1'
                ]
            ],
            'attribute' => [
                'error' => [
                    'validation_entity' => [
                        'name' => [
                            'value' => 'user-nm-1',
                            'error' => [
                                'rule_1' => 'error rule 1',
                                'rule_3' => 'error rule 3'
                            ]
                        ],
                        'first-name' => [
                            'value' => 'user-fnm-1',
                            'error' => [
                                'rule_1' => 'error rule 1',
                                'rule_2' => 'error rule 2'
                            ]
                        ]
                    ]
                ]
            ],
            'permission' => [
                'error' => [
                    'validation_entity_collection' => [
                        '#1' => [
                            'key' => [
                                'value' => 'perm-key-2',
                                'error' => [
                                    'rule_2' => 'error rule 2',
                                    'rule_3' => 'error rule 3'
                                ]
                            ],
                            'value' => [
                                'value' => false,
                                'error' => [
                                    'rule_1' => 'error rule 1',
                                    'rule_3' => 'error rule 3'
                                ]
                            ]
                        ],
                        '#2' => [
                            'key' => [
                                'value' => 'perm-key-3',
                                'error' => [
                                    'rule_2' => 'error rule 2'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        );
        $tabError = array(
            'strAttrLogin' => [
                'rule_1' => 'error rule 1',
                'rule_2' => 'error rule 2',
                'rule_3' => 'error rule 3'
            ],
            'strAttrEmail' => [
                'rule_1' => 'error rule 1'
            ],
            'objAttrProfileAttrEntity' => [
                'validation_entity' => [
                    'strAttrName' => [
                        'rule_1' => 'error rule 1',
                        'rule_3' => 'error rule 3'
                    ],
                    'strAttrFirstName' => [
                        'rule_1' => 'error rule 1',
                        'rule_2' => 'error rule 2'
                    ]
                ]
            ],
            'objAttrPermEntityCollection' => [
                'validation_entity_collection' => [
                    DefaultItemEntity::getStrFormatKey('2') => [
                        'strAttrKey' => [
                            'rule_2' => 'error rule 2',
                            'rule_3' => 'error rule 3'
                        ],
                        'boolAttrValue' => [
                            'rule_1' => 'error rule 1',
                            'rule_3' => 'error rule 3'
                        ]
                    ],
                    DefaultItemEntity::getStrFormatKey('3') => [
                        'strAttrKey' => [
                            'rule_2' => 'error rule 2'
                        ],
                    ]
                ]
            ]
        );

        // Return result
        return array(
            'Check entity, from response error data: success to valid entity user 1 (error not found)' => [
                $tabValue,
                200,
                [
                    'result' => []
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity, from response error data: success to valid entity user 1 (error data not found)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profile failed!'
                    ]
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity, from response error data: success to valid entity user 1 (error data empty)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profile failed!',
                        'data' => []
                    ]
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity, from response error data: fail to valid entity user 1' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profile failed!',
                        'data' => $tabBodyErrorData
                    ]
                ],
                null,
                [
                    false,
                    $tabError
                ]
            ],
            'Check entity, from response error data: success to valid entity user 1 (error data found, with collection data)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profile failed!',
                        'data' => [
                            '#0' => [
                                $tabBodyErrorData
                            ],
                            '#1' => [
                                'login' => [
                                    'value' => 'user-lg-2',
                                    'error' => [
                                        'rule_1' => 'error rule 1',
                                        'rule_2' => 'error rule 2',
                                        'rule_3' => 'error rule 3'
                                    ]
                                ],
                                'email' => [
                                    'value' => 'user-2@test.com',
                                    'error' => [
                                        'rule_1' => 'error rule 1'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity, from response error data: fail to valid entity user 1 (error data found, with collection data)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profile failed!',
                        'data' => [
                            '#0' => $tabBodyErrorData
                        ]
                    ]
                ],
                null,
                [
                    false,
                    $tabError
                ]
            ],
            'Check entity, from response error data: fail to valid entity user 1 (filter on attribute email)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profile failed!',
                        'data' => $tabBodyErrorData
                    ]
                ],
                'strAttrEmail',
                [
                    false,
                    [
                        'strAttrEmail' => [
                            'rule_1' => 'error rule 1'
                        ]
                    ]
                ]
            ]
        );
    }



    /**
     * Test can check if entity collection is valid,
     * from response error data.
     *
     * @param array $tabValue
     * @param integer $intStatusCode
     * @param array $tabBody
     * @param null|array $tabConfig
     * @param array $tabExpectResult
     * @dataProvider providerCheckEntityCollectionIsValidFromResponseErrorData
     */
    public function testCanCheckEntityCollectionIsValidFromResponseErrorData(
        array $tabValue,
        $intStatusCode,
        array $tabBody,
        $tabConfig,
        array $tabExpectResult
    )
    {
        // Get response
        /** @var DataHttpResponse $objResponse */
        $objResponse = static::$objHttpResponseFactory->getObjResponse(array(
            'type' => 'http_data',
            'body_parser_config' => [
                'type' => 'string_table_json'
            ],
            'rcp_info' => [
                'status_code' => $intStatusCode,
                'header' => [
                    'Content-type' => 'application/json',
                ],
                'body' => json_encode($tabBody)
            ]
        ));

        // Get entity collection
        $objEntityCollection = new UserProfileEntityCollection();
        ToolBoxUserProfile::hydrateUserProfileEntityCollection(
            $objEntityCollection,
            static::$objPermissionEntityFactory,
            static::$objUserProfileEntityFactory,
            $tabValue
        );

        // Check entity collection is valid
        $tabError = array();
        $boolValid = ToolBoxResponseValidEntity::checkEntityCollectionIsValid(
            $objEntityCollection,
            $objResponse,
            $tabError,
            $tabConfig
        );

        // Set assertions (check validation)
        $boolExpectValid = $tabExpectResult[0];
        $this->assertEquals($boolExpectValid, $boolValid);

        // Set assertions (check error)
        $tabExpectError = $tabExpectResult[1];
        $this->assertEquals($tabExpectError, $tabError);

        // Print
        /*
        echo('Check: '. PHP_EOL);var_dump($boolValid);echo(PHP_EOL);
        // echo('Get error: ' . PHP_EOL);var_dump($tabError);echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can check if entity collection is valid,
     * from response error data.
     *
     * @return array
     */
    public function providerCheckEntityCollectionIsValidFromResponseErrorData()
    {
        // Init var
        $tabValue = array(
            [
                'intAttrId' => 1,
                'strAttrLogin' => 'user-lg-1',
                'strAttrEmail' => 'user-1@test.com'
            ],
            [
                'intAttrId' => 2,
                'strAttrLogin' => 'user-lg-2',
                'strAttrEmail' => 'user-2@test.com'
            ],
            [
                'intAttrId' => 3,
                'strAttrLogin' => 'user-lg-3',
                'strAttrEmail' => 'user-3@test.com'
            ]
        );
        $tabBodyErrorData = array(
            '#0' => [
                'login' => [
                    'value' => 'user-lg-1',
                    'error' => [
                        'rule_1' => 'error rule 1',
                        'rule_2' => 'error rule 2',
                        'rule_3' => 'error rule 3'
                    ]
                ],
                'email' => [
                    'value' => 'user-1@test.com',
                    'error' => [
                        'rule_1' => 'error rule 1'
                    ]
                ]
            ],
            '#2' => [
                'login' => [
                    'value' => 'user-lg-2',
                    'error' => [
                        'rule_1' => 'error rule 1',
                        'rule_3' => 'error rule 3'
                    ]
                ],
                'email' => [
                    'value' => 'user-2@test.com',
                    'error' => [
                        'rule_1' => 'error rule 1',
                        'rule_2' => 'error rule 2'
                    ]
                ]
            ]
        );
        $tabError = array(
            DefaultItemEntity::getStrFormatKey('1') => [
                'strAttrLogin' => [
                    'rule_1' => 'error rule 1',
                    'rule_2' => 'error rule 2',
                    'rule_3' => 'error rule 3'
                ],
                'strAttrEmail' => [
                    'rule_1' => 'error rule 1'
                ]
            ],
            DefaultItemEntity::getStrFormatKey('3') => [
                'strAttrLogin' => [
                    'rule_1' => 'error rule 1',
                    'rule_3' => 'error rule 3'
                ],
                'strAttrEmail' => [
                    'rule_1' => 'error rule 1',
                    'rule_2' => 'error rule 2'
                ]
            ]
        );

        // Return result
        return array(
            'Check entity collection, from response error data: success to valid entity collection (error not found)' => [
                $tabValue,
                400,
                [
                    'result' => []
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity collection, from response error data: success to valid entity collection (error data not found)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profiles failed!'
                    ]
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity collection, from response error data: success to valid entity collection (error data empty)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profiles failed!',
                        'data' => []
                    ]
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity collection, from response error data: success to valid entity collection (success status code found)' => [
                $tabValue,
                200,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profiles failed!',
                        'data' => $tabBodyErrorData
                    ]
                ],
                null,
                [
                    true,
                    []
                ]
            ],
            'Check entity collection, from response error data: fail to valid entity collection' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profiles failed!',
                        'data' => $tabBodyErrorData
                    ]
                ],
                null,
                [
                    false,
                    $tabError
                ]
            ],
            'Check entity collection, from response error data: fail to valid entity collection (filter on entity user 3)' => [
                $tabValue,
                400,
                [
                    'error' => [
                        'code' => 400,
                        'message' => 'Validation user profiles failed!',
                        'data' => $tabBodyErrorData
                    ]
                ],
                [
                    [
                        ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrLogin',
                        ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                        ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => 'user-lg-3'
                    ]
                ],
                [
                    false,
                    [
                        DefaultItemEntity::getStrFormatKey('3') => [
                            'strAttrLogin' => [
                                'rule_1' => 'error rule 1',
                                'rule_3' => 'error rule 3'
                            ],
                            'strAttrEmail' => [
                                'rule_1' => 'error rule 1',
                                'rule_2' => 'error rule 2'
                            ]
                        ]
                    ]
                ]
            ]
        );
    }



}