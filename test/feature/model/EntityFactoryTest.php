<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\test\feature\model;

use PHPUnit\Framework\TestCase;

use liberty_code\model\entity\library\ConstEntity;
use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;
use people_sdk\library\model\entity\factory\exception\ExecConfigInvalidFormatException;
use people_sdk\library\test\model\model\user\UserProfileEntity;
use people_sdk\library\test\model\model\user\UserProfileEntityFactory;



/**
 * @cover DefaultEntityFactory
 */
class EntityFactoryTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var UserProfileEntityFactory */
    public static $objUserProfileEntityFactory;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/model/boot/ModelBootstrap.php');

        // Init properties
        /** @var UserProfileEntityFactory $objUserProfileEntityFactory */
        static::$objUserProfileEntityFactory = $objUserProfileEntityFactory;
    }



    public static function tearDownAfterClass(): void
    {
        // Clear registered items
        static::$objUserProfileEntityFactory->getObjEntityCollection()->removeItemAll();
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can get new or existing entity object.
     *
     * @param array $tabValue
     * @param null|array $tabConfig
     * @param string|array $expectResult
     * @dataProvider providerGetEntity
     * @dataProvider providerGetEntityWithSelectEntityId
     * @dataProvider providerGetEntityWithSelectEntityCollectionGetConfig
     */
    public function testCanGetEntity(
        array $tabValue,
        $tabConfig,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get entity
        $objEntity = static::$objUserProfileEntityFactory->getObjEntity($tabValue, $tabConfig);
        $strClassPath = get_class($objEntity);

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertions (check entity)
            $strExpectClassPath = $expectResult[0];
            $this->assertEquals($strExpectClassPath, $strClassPath);

            // Set assertions (check entity data)
            $tabExpectData = $expectResult[1];
            $this->assertEquals($tabExpectData, array_intersect_key($objEntity->getTabData(), $tabExpectData));

            // Set assertions (check entity selection)
            $intExpectId = $expectResult[2];
            if(!is_null($intExpectId))
            {
                $tabExpectEntity = array_values(static::$objUserProfileEntityFactory
                    ->getObjEntityCollection()
                    ->getTabItem(array(
                        [
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => 'intAttrId',
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => $intExpectId
                        ]
                    )));
                $objExpectEntity = $tabExpectEntity[0];
                $this->assertEquals(true, ($objExpectEntity === $objEntity));
            }

            // Print
            /*
            echo('Get class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
            echo('Get entity data: ' . PHP_EOL);var_dump($objEntity->getTabData());echo(PHP_EOL);
            foreach(static::$objUserProfileEntityFactory->getObjEntityCollection()->getTabKey() as $strKey)
            {
                echo('Get collection entity data: ' . PHP_EOL);
                var_dump(
                    static::$objUserProfileEntityFactory
                        ->getObjEntityCollection()
                        ->getItem($strKey)
                        ->getTabData()
                );
                echo(PHP_EOL);
            }
            //*/
        }
    }



    /**
     * Data provider,
     * to test can get new entity object (without entity selection).
     *
     * @return array
     */
    public function providerGetEntity()
    {
        // Return result
        return array(
            'Create|get entity: success to get new entity user 1' => [
                [
                    'intAttrId' => 1,
                    'strAttrLogin' => 'user-lg-1',
                    'strAttrEmail' => 'user-1@test.com'
                ],
                null,
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 1,
                        'strAttrLogin' => 'user-lg-1',
                        'strAttrEmail' => 'user-1@test.com'
                    ],
                    null
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can get new or existing entity object,
     * using entity selection, based on id.
     *
     * @return array
     */
    public function providerGetEntityWithSelectEntityId()
    {
        // Return result
        return array(
            'Create|get entity: success to get new entity user 2' => [
                [
                    'intAttrId' => 2,
                    'strAttrLogin' => 'user-lg-2'
                ],
                [
                    'select_entity_id' => 2
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 2,
                        'strAttrLogin' => 'user-lg-2',
                        'strAttrEmail' => null
                    ],
                    null
                ]
            ],
            'Create|get entity: fail to get only existing entity user 2 (config invalid)' => [
                [
                    'intAttrId' => 2,
                    'strAttrLogin' => 'user-lg-2-upd'
                ],
                [
                    'select_entity_id' => 2,
                    'select_entity_create_require' => 'test'
                ],
                ExecConfigInvalidFormatException::class
            ],
            'Create|get entity: fail to get only existing entity user 2 (selection fails)' => [
                [
                    'intAttrId' => 2,
                    'strAttrLogin' => 'user-lg-2-upd'
                ],
                [
                    'select_entity_id' => 2,
                    'select_entity_create_require' => false
                ],
                ExecConfigInvalidFormatException::class
            ],
            'Create|get entity: fail to get new entity user 3 (set on collection)' => [
                [
                    'intAttrId' => 3,
                    'strAttrLogin' => 'user-lg-3',
                    'strAttrEmail' => 'user-3@test.com'
                ],
                [
                    'select_entity_id' => 3,
                    'select_entity_collection_set_require' => 'test'
                ],
                ExecConfigInvalidFormatException::class
            ],
            'Create|get entity: success to get new entity user 3 (set on collection)' => [
                [
                    'intAttrId' => 3,
                    'strAttrLogin' => 'user-lg-3',
                    'strAttrEmail' => 'user-3@test.com'
                ],
                [
                    'select_entity_id' => 3,
                    'select_entity_collection_set_require' => true
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 3,
                        'strAttrLogin' => 'user-lg-3',
                        'strAttrEmail' => 'user-3@test.com'
                    ],
                    null
                ]
            ],
            'Create|get entity: success to get existing entity user 3' => [
                [
                    'intAttrId' => 3,
                    'strAttrLogin' => 'user-lg-3-upd-2'
                ],
                [
                    'select_entity_id' => 3
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 3,
                        'strAttrLogin' => 'user-lg-3-upd-2',
                        'strAttrEmail' => 'user-3@test.com'
                    ],
                    3
                ]
            ],
            'Create|get entity: success to get only existing entity user 3' => [
                [
                    'intAttrId' => 3,
                    'strAttrLogin' => 'user-lg-3-upd'
                ],
                [
                    'select_entity_id' => 3,
                    'select_entity_create_require' => false
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 3,
                        'strAttrLogin' => 'user-lg-3-upd',
                        'strAttrEmail' => 'user-3@test.com'
                    ],
                    3
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can get new or existing entity object,
     * using entity selection, based on selection configuration.
     *
     * @return array
     */
    public function providerGetEntityWithSelectEntityCollectionGetConfig()
    {
        // Return result
        return array(
            'Create|get entity: success to get new entity user 4' => [
                [
                    'intAttrId' => 4,
                    'strAttrLogin' => 'user-lg-4'
                ],
                [
                    'select_entity_collection_get_config' => [
                        [
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrLogin',
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => 'user-lg-4'
                        ]
                    ]
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 4,
                        'strAttrLogin' => 'user-lg-4',
                        'strAttrEmail' => null
                    ],
                    null
                ]
            ],
            'Create|get entity: fail to get only existing entity user 4 (selection fails)' => [
                [
                    'intAttrId' => 4,
                    'strAttrLogin' => 'user-lg-4-upd'
                ],
                [
                    'select_entity_collection_get_config' => [
                        [
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrLogin',
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => 'user-lg-4'
                        ]
                    ],
                    'select_entity_create_require' => false
                ],
                ExecConfigInvalidFormatException::class
            ],
            'Create|get entity: success to get new entity user 5 (set on collection)' => [
                [
                    'intAttrId' => 5,
                    'strAttrLogin' => 'user-lg-5',
                    'strAttrEmail' => 'user-5@test.com'
                ],
                [
                    'select_entity_collection_get_config' => [
                        [
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrLogin',
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => 'user-lg-5'
                        ]
                    ],
                    'select_entity_collection_set_require' => true
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 5,
                        'strAttrLogin' => 'user-lg-5',
                        'strAttrEmail' => 'user-5@test.com'
                    ],
                    null
                ]
            ],
            'Create|get entity: success to get only existing entity user 5' => [
                [
                    'strAttrLogin' => 'user-lg-5-upd'
                ],
                [
                    'select_entity_collection_get_config' => [
                        [
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => 'strAttrLogin',
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => 'user-lg-5'
                        ]
                    ],
                    'select_entity_create_require' => false
                ],
                [
                    UserProfileEntity::class,
                    [
                        'intAttrId' => 5,
                        'strAttrLogin' => 'user-lg-5-upd',
                        'strAttrEmail' => 'user-5@test.com'
                    ],
                    5
                ]
            ]
        );
    }



}