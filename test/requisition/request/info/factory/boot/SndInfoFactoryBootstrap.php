<?php

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\config\config\register\model\RegisterConfig;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\provider\model\DefaultProvider;
use people_sdk\library\requisition\request\info\factory\config\base\model\BaseConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\multi\model\MultiSndInfoFactory;



// Init config
$objRegister = new DefaultTableRegister();
$objConfig = new RegisterConfig($objRegister);

// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

// Init sending information factory 1
$objRequestSndInfoFactory1 = new BaseConfigSndInfoFactory(
    $objConfig,
    array(
        'loc_support_type' => 'url_argument',
        'loc_get_timezone_name_config_key' => 'dt_tz_nm',
        'loc_get_datetime_format_config_key' => 'dt_format',
        'loc_set_timezone_name_config_key' => 'dt_tz_nm',
        'loc_set_datetime_format_config_key' => 'dt_format',
        'content_support_type' => 'header',
        'content_type_config_key' => 'content_type'
    )
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'request_snd_info_1',
        'source' => BaseConfigSndInfoFactory::class,
        'set' =>  ['type' => 'instance', 'value' => $objRequestSndInfoFactory1],
        'option' => [
            'shared' => true
        ]
    )));

// Init sending information factory 2
$objRequestSndInfoFactory2 = new BaseConfigSndInfoFactory(
    $objConfig,
    array(
        'loc_support_type' => 'header',
        'space_connect_name_config_key' => 'spc_nm',
        'space_connect_auth_key_config_key' => 'spc_auth_key'
    )
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'request_snd_info_2',
        'source' => BaseConfigSndInfoFactory::class,
        'set' =>  ['type' => 'instance', 'value' => $objRequestSndInfoFactory2],
        'option' => [
            'shared' => true
        ]
    )));

// Init sending information factory 3
$objRequestSndInfoFactory3 = new BaseConfigSndInfoFactory(
    $objConfig,
    array(
        'snd_info_config_key' => 'snd_info',
        'space_connect_name_config_key' => 'spc_nm_2',
        'space_connect_auth_key_config_key' => 'spc_auth_key_2',
        'auth_type_config_key' => 'auth_type',
        'auth_user_profile_login_config_key' => 'auth_identifier',
        'auth_user_profile_pw_config_key' => 'auth_secret',
        'auth_user_profile_api_key_config_key' => 'auth_identifier',
        'auth_user_profile_token_config_key' => 'auth_identifier',
        'auth_app_profile_name_config_key' => 'auth_identifier',
        'auth_app_profile_secret_config_key' => 'auth_secret',
        'auth_app_profile_api_key_config_key' => 'auth_identifier',
        'auth_app_profile_token_config_key' => 'auth_identifier',
        'auth_super_admin_profile_name_config_key' => 'auth_identifier',
        'auth_super_admin_profile_secret_config_key' => 'auth_secret',
    )
);

$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'request_snd_info_3',
        'source' => BaseConfigSndInfoFactory::class,
        'set' =>  ['type' => 'instance', 'value' => $objRequestSndInfoFactory3],
        'option' => [
            'shared' => true
        ]
    )));

// Init multi sending information factory
$objMultiRequestSndInfoFactory = new MultiSndInfoFactory(
    array(
        'response_cache_key_pattern' => 'response_%s',
        'snd_info_factory' => [
            [
                'snd_info_factory' => $objRequestSndInfoFactory1
            ],
            [
                'snd_info_factory' => 'request_snd_info_2',
            ],
            [
                'snd_info_factory' => 'request_snd_info_3',
                'order' => -1
            ]
        ]
    ),
    $objProvider
);


