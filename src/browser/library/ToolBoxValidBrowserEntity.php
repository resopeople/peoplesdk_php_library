<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\browser\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\model\ValidatorConfigEntity;
use people_sdk\library\browser\library\ConstBrowser;



class ToolBoxValidBrowserEntity extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get entity attribute rule configuration array,
     * to validate sorting value.
     *
     * Return array format:
     * Attribute rule configuration array format,
     * from @see ValidatorConfigEntity::getTabRuleConfig() return array format.
     *
     * @return array
     */
    public static function getTabSortRuleConfig()
    {
        // Init var
        $tabSort = ConstBrowser::getTabSort();
        $strTabSort = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabSort
        ));
        $result = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-sort' => [
                            [
                                'compare_in',
                                [
                                    'compare_value' => $tabSort
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid sort string (' . $strTabSort . ').'
                ]
            ]
        );

        // Return result
        return $result;
    }



}