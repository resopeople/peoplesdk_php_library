<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\browser\library;



class ConstBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ITEM_COUNT_PAGE = 'intAttrItemCountPage';
    const ATTRIBUTE_KEY_PAGE_INDEX = 'intAttrPageIndex';

    const ATTRIBUTE_ALIAS_ITEM_COUNT_PAGE = 'item-count-page';
    const ATTRIBUTE_ALIAS_PAGE_INDEX = 'page-index';



    // Sort configuration
    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of sorts.
     *
     * @return array
     */
    public static function getTabSort()
    {
        // Return result
        return array(
            self::SORT_ASC,
            self::SORT_DESC
        );
    }



}