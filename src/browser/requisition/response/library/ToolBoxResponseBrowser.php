<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\browser\requisition\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use people_sdk\library\requisition\response\library\ToolBoxResponse;
use people_sdk\library\browser\requisition\response\library\ConstResponseBrowser;



class ToolBoxResponseBrowser extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get browser data array,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|array
     */
    public static function getTabBrowserData(DataHttpResponse $objResponse)
    {
        // Init var
        $tabData = ToolBoxResponse::getTabResultData($objResponse);
        $result = (
            (
                isset($tabData[ConstResponseBrowser::RESULT_KEY_BROWSE]) &&
                is_array($tabData[ConstResponseBrowser::RESULT_KEY_BROWSE])
            ) ?
                $tabData[ConstResponseBrowser::RESULT_KEY_BROWSE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get total item count,
     * from specified response browser data.
     *
     * @param array $tabBrowserData
     * @return null|integer
     */
    public static function getIntItemCountFromData(array $tabBrowserData)
    {
        // Init var
        $result = (
            (
                isset($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT]) &&
                is_numeric($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT]) &&
                (intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT]) >= 0)
            ) ?
                intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get total item count,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|integer
     */
    public static function getIntItemCount(DataHttpResponse $objResponse)
    {
        // Init var
        $tabBrowserData = static::getTabBrowserData($objResponse);
        $result = (
            (!is_null($tabBrowserData)) ?
                static::getIntItemCountFromData($tabBrowserData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get item count, per page,
     * from specified response browser data.
     *
     * @param array $tabBrowserData
     * @return null|integer
     */
    public static function getIntItemCountPerPageFromData(array $tabBrowserData)
    {
        // Init var
        $result = (
            (
                isset($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_PAGE]) &&
                is_numeric($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_PAGE]) &&
                (intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_PAGE]) > 0)
            ) ?
                intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_PAGE]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get item count, per page,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|integer
     */
    public static function getIntItemCountPerPage(DataHttpResponse $objResponse)
    {
        // Init var
        $tabBrowserData = static::getTabBrowserData($objResponse);
        $result = (
            (!is_null($tabBrowserData)) ?
                static::getIntItemCountPerPageFromData($tabBrowserData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get item count, on active page,
     * from specified response browser data.
     *
     * @param array $tabBrowserData
     * @return null|integer
     */
    public static function getIntItemCountOnActivePageFromData(array $tabBrowserData)
    {
        // Init var
        $result = (
            (
                isset($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_ACTIVE_PAGE]) &&
                is_numeric($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_ACTIVE_PAGE]) &&
                (intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_ACTIVE_PAGE]) > 0)
            ) ?
                intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_ITEM_COUNT_ACTIVE_PAGE]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get item count, on active page,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|integer
     */
    public static function getIntItemCountOnActivePage(DataHttpResponse $objResponse)
    {
        // Init var
        $tabBrowserData = static::getTabBrowserData($objResponse);
        $result = (
            (!is_null($tabBrowserData)) ?
                static::getIntItemCountOnActivePageFromData($tabBrowserData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get total page count,
     * from specified response browser data.
     *
     * @param array $tabBrowserData
     * @return null|integer
     */
    public static function getIntPageCountFromData(array $tabBrowserData)
    {
        // Init var
        $result = (
            (
                isset($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_COUNT]) &&
                is_numeric($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_COUNT]) &&
                (intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_COUNT]) >= 0)
            ) ?
                intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_COUNT]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get total page count,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|integer
     */
    public static function getIntPageCount(DataHttpResponse $objResponse)
    {
        // Init var
        $tabBrowserData = static::getTabBrowserData($objResponse);
        $result = (
            (!is_null($tabBrowserData)) ?
                static::getIntPageCountFromData($tabBrowserData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get active page index,
     * from specified response browser data.
     *
     * @param array $tabBrowserData
     * @return null|integer
     */
    public static function getIntActivePageIndexFromData(array $tabBrowserData)
    {
        // Init var
        $result = (
            (
                isset($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_INDEX]) &&
                is_numeric($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_INDEX]) &&
                (intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_INDEX]) >= 0)
            ) ?
                intval($tabBrowserData[ConstResponseBrowser::RESULT_BROWSE_KEY_PAGE_INDEX]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get active page index,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|integer
     */
    public static function getIntActivePageIndex(DataHttpResponse $objResponse)
    {
        // Init var
        $tabBrowserData = static::getTabBrowserData($objResponse);
        $result = (
            (!is_null($tabBrowserData)) ?
                static::getIntActivePageIndexFromData($tabBrowserData) :
                null
        );

        // Return result
        return $result;
    }



}