<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\browser\requisition\response\library;



class ConstResponseBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Result configuration
    const RESULT_KEY_BROWSE = 'browsing';

    // Result browser configuration
    const RESULT_BROWSE_KEY_ITEM_COUNT = 'item_count';
    const RESULT_BROWSE_KEY_ITEM_COUNT_PAGE = 'item_count_page';
    const RESULT_BROWSE_KEY_ITEM_COUNT_ACTIVE_PAGE = 'item_count_active_page';
    const RESULT_BROWSE_KEY_PAGE_COUNT = 'page_count';
    const RESULT_BROWSE_KEY_PAGE_INDEX = 'page_index';



}