<?php
/**
 * This class allows to define default browser entity class.
 * Default browser entity allows to define attributes,
 * to search specific entities.
 * Can be consider is base of all browser entity type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\browser\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use liberty_code\http\requisition\request\persistence\model\PersistorHttpRequest;
use people_sdk\library\browser\library\ConstBrowser;



/**
 * @property null|integer $intAttrItemCountPage
 * @property null|integer $intAttrPageIndex
 */
abstract class DefaultBrowserEntity extends ValidatorConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute page count
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstBrowser::ATTRIBUTE_ALIAS_ITEM_COUNT_PAGE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute page index
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstBrowser::ATTRIBUTE_KEY_PAGE_INDEX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstBrowser::ATTRIBUTE_ALIAS_PAGE_INDEX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = array(
            ConstBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_between',
                                    [
                                        'greater_compare_value' => 0,
                                        'greater_equal_enable_require' => false,
                                        'less_compare_value' => 100
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid strict positive integer.'
                    ]
                ]
            ],
            ConstBrowser::ATTRIBUTE_KEY_PAGE_INDEX => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    ['compare_value' => 0]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid positive integer.'
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE:
            case ConstBrowser::ATTRIBUTE_KEY_PAGE_INDEX:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified attribute value required,
     * for selection query.
     * Overwrite it to implement specific feature.
     *
     * @param string $strKey
     * @param mixed $value
     * @return boolean
     */
    protected function checkAttributeValueRequiredOnQuery($strKey, $value)
    {
        // Return result
        return (!is_null($value));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified attribute formatted value,
     * for selection query.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get selection query array,
     * from attribute values.
     * Selection query array can be used as query,
     * on @see CollectionRepositoryInterface::search() method,
     * using @see DefaultPersistor .
     *
     * Return array format:
     * @see PersistorHttpRequest::setQuery() query array format:
     * [string attribute alias => mixed formatted attribute value, for selection query]
     *
     * @return array
     */
    public function getTabQuery()
    {
        // Init var
        $result = array();

        // Run each attribute
        foreach($this->getTabAttributeKey() as $strKey)
        {
            // Get info
            $value = parent::beanGetData($strKey);

            // Get and register query data, if required
            if($this->checkAttributeValueRequiredOnQuery($strKey, $value))
            {
                $strQueryKey = $this->getAttributeAlias($strKey);
                $queryValue = $this->getAttributeValueQueryFormat($strKey, $value);
                $result[$strQueryKey] = $queryValue;
            }
        }

        // Return result
        return $result;
    }



}