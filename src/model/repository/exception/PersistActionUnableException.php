<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\exception;

use Exception;

use people_sdk\library\model\repository\library\ConstRepository;



class PersistActionUnableException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param string $strPersistActionType
     */
    public function __construct($strPersistActionType)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstRepository::EXCEPT_MSG_PERSIST_ACTION_UNABLE,
            mb_strimwidth(strval($strPersistActionType), 0, 50, "...")
        );
    }
	
	
	
}