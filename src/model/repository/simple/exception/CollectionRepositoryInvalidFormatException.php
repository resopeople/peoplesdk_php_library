<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\simple\exception;

use Exception;

use people_sdk\library\model\repository\simple\library\ConstSimpleRepository;
use people_sdk\library\model\repository\simple\model\SimpleRepository;



class CollectionRepositoryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $repository
     */
	public function __construct($repository)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(
		    ConstSimpleRepository::EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT,
            mb_strimwidth(strval($repository), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified repository has valid format.
	 *
     * @param mixed $repository
     * @return boolean
     * @throws static
     */
    static public function setCheck($repository)
    {
        // Init var
        $result = (
            (is_null($repository)) ||
            ($repository instanceof SimpleRepository)
        );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($repository);
        }

        // Return result
        return $result;
    }
	
	
	
}