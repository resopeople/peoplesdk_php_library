<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\simple\library;



class ConstSimpleRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REQUEST_SND_INFO_FACTORY = 'objRequestSndInfoFactory';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the simple repository configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the adequat simple repository execution configuration standard.';
    const EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT =
        'Following repository "%1$s" invalid! It must be a simple repository object.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the simple collection repository configuration standard.';
    const EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the adequat simple collection repository execution configuration standard.';



}