<?php
/**
 * Description :
 * This class allows to define simple collection repository class.
 * Simple collection repository is fixed simple collection repository,
 * containing all information to use persistence configuration and sub-actions,
 * and handling HTTP request sending information,
 * to save in requisition persistence.
 * Specified requisition persistence must handle HTTP request sending and HTTP response reception.
 * Can be consider is base of all simple collection repository type.
 *
 * Simple collection repository allows to specify parts of configuration:
 * [
 *     Fixed simple collection repository configuration,
 *
 *     @see ToolBoxRepository::checkCollectionRepoConfigIsValid() configuration array format
 * ]
 *
 * Note:
 * -> Persistence action enable:
 * Persistence action is enabled, if at least,
 * basic persistence configuration or one additional persistence configuration (for one specific sub-action),
 * is configured.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\simple\model;

use liberty_code\model\repository\simple\fix\model\FixSimpleCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\api\EntityFactoryInterface;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\library\model\repository\library\ToolBoxRepository;
use people_sdk\library\model\repository\library\ToolBoxPersistor;
use people_sdk\library\model\repository\exception\PersistActionUnableException;
use people_sdk\library\model\repository\simple\library\ToolBoxSimpleRepository;
use people_sdk\library\model\repository\simple\exception\CollectionRepositoryInvalidFormatException;
use people_sdk\library\model\repository\simple\exception\CollectionConfigInvalidFormatException;
use people_sdk\library\model\repository\simple\exception\CollectionExecConfigInvalidFormatException;
use people_sdk\library\model\repository\simple\model\SimpleRepository;



/**
 * @method null|SimpleRepository getObjRepository() @inheritdoc
 * @method null|DefaultPersistor getObjPersistor() @inheritdoc
 */
abstract class SimpleCollectionRepository extends FixSimpleCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SimpleRepository $objRepository = null
     */
    public function __construct(
        EntityFactoryInterface $objEntityFactory = null,
        SimpleRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct($objEntityFactory, $objRepository);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case BaseConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY:
                    CollectionRepositoryInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case BaseConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG:
                    CollectionConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabData)
    {
        // Return result
        return ToolBoxRepository::getObjCollectionRepoEntityNew(
            $this->getObjEntityFactory(),
            $this->getTabConfig(false),
            $tabData
        );
    }





    // Methods persistence
    // ******************************************************************************

    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixSimpleCollectionRepository::getTabConfigPersistGet() configuration array format,
     *
     *     @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     *     for persistence action get
     * ]
     *
     * @inheritdoc
     * @throws CollectionExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistGet(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistGet($objCollection, $tabExecConfig);
        CollectionExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkCollectionRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_GET
        ))
        {
            throw new CollectionExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkCollectionRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_GET
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_GET);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigGetTabData(
            $tabConfig,
            ToolBoxSimpleRepository::getTabCollectionRepoExecConfigPersistConfigPath(
                ConstRepository::PERSIST_ACTION_TYPE_GET
            ),
            $tabExecConfig,
            $this->getObjRepository()->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixSimpleCollectionRepository::getTabConfigPersistSearch() configuration array format,
     *
     *     @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     *     for persistence action search
     * ]
     *
     * @inheritdoc
     * @throws CollectionExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistSearch(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistSearch($objCollection, $tabExecConfig);
        CollectionExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkCollectionRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_SEARCH
        ))
        {
            throw new CollectionExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkCollectionRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_SEARCH
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_SEARCH);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigSearchTabData(
            $tabConfig,
            ToolBoxSimpleRepository::getTabCollectionRepoExecConfigPersistConfigPath(
                ConstRepository::PERSIST_ACTION_TYPE_SEARCH
            ),
            $tabExecConfig,
            $this->getObjRepository()->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixSimpleCollectionRepository::getTabConfigPersistCreate() configuration array format,
     *
     *     @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     *     for persistence action create
     * ]
     *
     * @inheritdoc
     * @throws CollectionExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistCreate(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistCreate($objCollection, $tabExecConfig);
        CollectionExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkCollectionRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_CREATE
        ))
        {
            throw new CollectionExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkCollectionRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_CREATE
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_CREATE);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigCreateTabData(
            $tabConfig,
            ToolBoxSimpleRepository::getTabCollectionRepoExecConfigPersistConfigPath(
                ConstRepository::PERSIST_ACTION_TYPE_CREATE
            ),
            $tabExecConfig,
            $this->getObjRepository()->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixSimpleCollectionRepository::getTabConfigPersistUpdate() configuration array format,
     *
     *     @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     *     for persistence action update
     * ]
     *
     * @inheritdoc
     * @throws CollectionExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistUpdate(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistUpdate($objCollection, $tabExecConfig);
        CollectionExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkCollectionRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_UPDATE
        ))
        {
            throw new CollectionExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkCollectionRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_UPDATE
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_UPDATE);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigUpdateTabData(
            $tabConfig,
            ToolBoxSimpleRepository::getTabCollectionRepoExecConfigPersistConfigPath(
                ConstRepository::PERSIST_ACTION_TYPE_UPDATE
            ),
            $tabExecConfig,
            $this->getObjRepository()->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixSimpleCollectionRepository::getTabConfigPersistDelete() configuration array format,
     *
     *     @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     *     for persistence action delete
     * ]
     *
     * @inheritdoc
     * @throws CollectionExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistDelete(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistDelete($objCollection, $tabExecConfig);
        CollectionExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkCollectionRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_DELETE
        ))
        {
            throw new CollectionExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkCollectionRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_DELETE
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_DELETE);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigDeleteTabData(
            $tabConfig,
            ToolBoxSimpleRepository::getTabCollectionRepoExecConfigPersistConfigPath(
                ConstRepository::PERSIST_ACTION_TYPE_DELETE
            ),
            $tabExecConfig,
            $this->getObjRepository()->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



}