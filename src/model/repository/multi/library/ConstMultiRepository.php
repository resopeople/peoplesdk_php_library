<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\multi\library;



class ConstMultiRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REQUEST_SND_INFO_FACTORY = 'objRequestSndInfoFactory';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the multi repository configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the adequat multi repository execution configuration standard.';
    const EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT =
        'Following repository "%1$s" invalid! It must be a multi repository object.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the multi collection repository configuration standard.';
    const EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the adequat multi collection repository execution configuration standard.';



}