<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\multi\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\http\requisition\request\model\HttpRequest;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\library\model\repository\library\ToolBoxRepository;
use people_sdk\library\model\repository\multi\model\MultiRepository;
use people_sdk\library\model\repository\multi\model\MultiCollectionRepository;



class ToolBoxMultiRepository extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get additional persistence execution configuration path,
     * to retrieve its data, from multi repository execution configuration.
     *
     * @param string $strPersistActionType
     * @return null|string
     */
    public static function getTabRepoExecConfigPersistConfigPath($strPersistActionType)
    {
        // Init var
        $result = null;

        // Get configuration key, if required
        if(is_string($strPersistActionType))
        {
            switch($strPersistActionType)
            {
                case ConstRepository::PERSIST_ACTION_TYPE_GET:
                case ConstRepository::PERSIST_ACTION_TYPE_CREATE:
                case ConstRepository::PERSIST_ACTION_TYPE_UPDATE:
                case ConstRepository::PERSIST_ACTION_TYPE_DELETE:
                    $result = BaseConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG;
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get additional persistence execution configuration path,
     * to retrieve its data, from multi collection repository execution configuration.
     *
     * @param string $strPersistActionType
     * @return null|string
     */
    public static function getTabCollectionRepoExecConfigPersistConfigPath($strPersistActionType)
    {
        // Init var
        $result = null;

        // Get configuration key, if required
        if(is_string($strPersistActionType))
        {
            switch($strPersistActionType)
            {
                case ConstRepository::PERSIST_ACTION_TYPE_GET:
                case ConstRepository::PERSIST_ACTION_TYPE_SEARCH:
                case ConstRepository::PERSIST_ACTION_TYPE_CREATE:
                case ConstRepository::PERSIST_ACTION_TYPE_UPDATE:
                case ConstRepository::PERSIST_ACTION_TYPE_DELETE:
                    $result = BaseConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG;
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get multi repository execution configuration,
     * with specified request sending information.
     *
     * Configuration array format:
     * @see checkRepoConfigIsValid() configuration array format,
     * for @see MultiRepository.
     *
     * Request sending information array format:
     * @see HttpRequest sending information array format.
     *
     * Execution configuration array format:
     * Null
     * or
     * @see checkRepoExecConfigIsValid() configuration array format,
     * for @see MultiRepository.
     *
     * Return format:
     * @see checkRepoExecConfigIsValid() configuration array format,
     * for @see MultiRepository.
     *
     * @param array $tabConfig
     * @param string $strPersistActionType
     * @param array $tabRequestSndInfo
     * @param array $tabExecConfig = null
     * @return array
     */
    public static function getTabRepoExecConfigWithRequestSndInfo(
        array $tabConfig,
        $strPersistActionType,
        array $tabRequestSndInfo,
        array $tabExecConfig = null
    )
    {
        // Init var
        $strExecConfigPersistConfigPath = static::getTabRepoExecConfigPersistConfigPath($strPersistActionType);
        $strConfigKeyRequestSndInfoPath = (
            ToolBoxRepository::checkRepoPersistActionIsValid($strPersistActionType) ?
                ConstRepository::getStrConfigKeyPersistConfigRequestSndInfoPath($strPersistActionType) :
                null
        );
        $result = ToolBoxRepository::getTabExecConfigWithRequestSndInfoEngine(
            $tabConfig,
            $strExecConfigPersistConfigPath,
            $strConfigKeyRequestSndInfoPath,
            $tabRequestSndInfo,
            $tabExecConfig
        );

        // Return result
        return $result;
    }



    /**
     * Get multi collection repository execution configuration,
     * with specified request sending information.
     *
     * Configuration array format:
     * @see checkCollectionRepoConfigIsValid() configuration array format,
     * for @see MultiCollectionRepository.
     *
     * Request sending information array format:
     * @see HttpRequest sending information array format.
     *
     * Execution configuration array format:
     * Null
     * or
     * @see checkCollectionRepoExecConfigIsValid() configuration array format,
     * for @see MultiCollectionRepository.
     *
     * Return format:
     * @see checkCollectionRepoExecConfigIsValid() configuration array format,
     * for @see MultiCollectionRepository.
     *
     * @param array $tabConfig
     * @param string $strPersistActionType
     * @param array $tabRequestSndInfo
     * @param array $tabExecConfig = null
     * @return array
     */
    public static function getTabCollectionRepoExecConfigWithRequestSndInfo(
        array $tabConfig,
        $strPersistActionType,
        array $tabRequestSndInfo,
        array $tabExecConfig = null
    )
    {
        // Init var
        $strExecConfigPersistConfigPath = static::getTabCollectionRepoExecConfigPersistConfigPath($strPersistActionType);
        $strConfigKeyRequestSndInfoPath = (
            ToolBoxRepository::checkCollectionRepoPersistActionIsValid($strPersistActionType) ?
                ConstRepository::getStrConfigKeyPersistConfigRequestSndInfoPath($strPersistActionType) :
                null
        );
        $result = ToolBoxRepository::getTabExecConfigWithRequestSndInfoEngine(
            $tabConfig,
            $strExecConfigPersistConfigPath,
            $strConfigKeyRequestSndInfoPath,
            $tabRequestSndInfo,
            $tabExecConfig
        );

        // Return result
        return $result;
    }



}