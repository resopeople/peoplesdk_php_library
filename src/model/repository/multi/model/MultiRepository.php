<?php
/**
 * Description :
 * This class allows to define multi repository class.
 * Multi repository is fixed multi repository,
 * containing all information to use persistence configuration and sub-actions,
 * and handling HTTP request sending information,
 * to save in requisition persistence.
 * Specified requisition persistence must handle HTTP request sending and HTTP response reception.
 * Can be consider is base of all multi repository type.
 *
 * Multi repository allows to specify parts of configuration:
 * [
 *     Fixed multi repository configuration,
 *
 *     @see ToolBoxRepository::checkRepoConfigIsValid() configuration array format
 * ]
 *
 * Note:
 * -> Persistence action enable:
 * Persistence action is enabled, if at least,
 * basic persistence configuration or one additional persistence configuration (for one specific sub-action),
 * is configured.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\multi\model;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\library\model\repository\library\ToolBoxRepository;
use people_sdk\library\model\repository\library\ToolBoxPersistor;
use people_sdk\library\model\repository\exception\RequestSndInfoFactoryInvalidFormatException;
use people_sdk\library\model\repository\exception\PersistActionUnableException;
use people_sdk\library\model\repository\multi\library\ConstMultiRepository;
use people_sdk\library\model\repository\multi\library\ToolBoxMultiRepository;
use people_sdk\library\model\repository\multi\exception\ConfigInvalidFormatException;
use people_sdk\library\model\repository\multi\exception\ExecConfigInvalidFormatException;



/**
 * @method null|DefaultPersistor getObjPersistor() @inheritdoc
 * @method null|SndInfoFactoryInterface getObjRequestSndInfoFactory() Get request sending information factory object.
 * @method void setObjRequestSndInfoFactory(null|SndInfoFactoryInterface $objFactory) Set request sending information factory object.
 */
abstract class MultiRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DefaultPersistor $objPersistor = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     */
    public function __construct(
        DefaultPersistor $objPersistor = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Call parent constructor
        parent::__construct($objPersistor);

        // Init request sending information factory
        $this->setObjRequestSndInfoFactory($objRequestSndInfoFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMultiRepository::DATA_KEY_DEFAULT_REQUEST_SND_INFO_FACTORY))
        {
            $this->__beanTabData[ConstMultiRepository::DATA_KEY_DEFAULT_REQUEST_SND_INFO_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMultiRepository::DATA_KEY_DEFAULT_REQUEST_SND_INFO_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMultiRepository::DATA_KEY_DEFAULT_REQUEST_SND_INFO_FACTORY:
                    RequestSndInfoFactoryInvalidFormatException::setCheck($value);
                    break;

                case BaseConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return DefaultPersistor::class;
    }





    // Methods persistence
    // ******************************************************************************

    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixMultiRepository::getTabConfigPersistGet() configuration array format,
     *
     *     @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     *     for persistence action get
     * ]
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistGet(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistGet($objEntity, $tabExecConfig);
        ExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_GET
        ))
        {
            throw new ExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_GET
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_GET);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigGetData(
            $tabConfig,
            ToolBoxMultiRepository::getTabRepoExecConfigPersistConfigPath(ConstRepository::PERSIST_ACTION_TYPE_GET),
            $tabExecConfig,
            $this->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixMultiRepository::getTabConfigPersistCreate() configuration array format,
     *
     *     @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     *     for persistence action create
     * ]
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistCreate(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistCreate($objEntity, $tabExecConfig);
        ExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_CREATE
        ))
        {
            throw new ExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_CREATE
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_CREATE);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigCreateData(
            $tabConfig,
            ToolBoxMultiRepository::getTabRepoExecConfigPersistConfigPath(ConstRepository::PERSIST_ACTION_TYPE_CREATE),
            $tabExecConfig,
            $this->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixMultiRepository::getTabConfigPersistUpdate() configuration array format,
     *
     *     @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     *     for persistence action update
     * ]
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistUpdate(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistUpdate($objEntity, $tabExecConfig);
        ExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_UPDATE
        ))
        {
            throw new ExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_UPDATE
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_UPDATE);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigUpdateData(
            $tabConfig,
            ToolBoxMultiRepository::getTabRepoExecConfigPersistConfigPath(ConstRepository::PERSIST_ACTION_TYPE_UPDATE),
            $tabExecConfig,
            $this->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     @see FixMultiRepository::getTabConfigPersistDelete() configuration array format,
     *
     *     @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     *     for persistence action delete
     * ]
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     * @throws PersistActionUnableException
     */
    protected function getTabConfigPersistDelete(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set check arguments
        parent::getTabConfigPersistDelete($objEntity, $tabExecConfig);
        ExecConfigInvalidFormatException::setCheck($tabExecConfig);
        if(!ToolBoxRepository::checkRepoExecConfigIsValidForPersistAction(
            $tabExecConfig,
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_DELETE
        ))
        {
            throw new ExecConfigInvalidFormatException((is_array($tabExecConfig) ? serialize($tabExecConfig) : $tabExecConfig));
        }
        if(!ToolBoxRepository::checkRepoPersistActionEnabled(
            $tabConfig,
            ConstRepository::PERSIST_ACTION_TYPE_DELETE
        ))
        {
            throw new PersistActionUnableException(ConstRepository::PERSIST_ACTION_TYPE_DELETE);
        }

        // Get persistence execution config (array always returned, due to validation above)
        /** @var array $result */
        $result = ToolBoxPersistor::getTabExecConfigDeleteData(
            $tabConfig,
            ToolBoxMultiRepository::getTabRepoExecConfigPersistConfigPath(ConstRepository::PERSIST_ACTION_TYPE_DELETE),
            $tabExecConfig,
            $this->getObjRequestSndInfoFactory()
        );

        // Return result
        return $result;
    }



}