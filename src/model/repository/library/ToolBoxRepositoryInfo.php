<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\repository\library\ConstRepository;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\http\requisition\request\model\HttpRequest;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;



class ToolBoxRepositoryInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get request,
     * from specified info array.
     *
     * @param array $tabInfo
     * @return null|HttpRequest
     */
    public static function getObjRequest(array $tabInfo)
    {
        // Return result
        return (
            (
                isset($tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO][ConstPersistor::TAB_INFO_KEY_REQUEST]) &&
                ($tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO][ConstPersistor::TAB_INFO_KEY_REQUEST] instanceof HttpRequest)
            ) ?
                $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO][ConstPersistor::TAB_INFO_KEY_REQUEST] :
                null
            );
    }



    /**
     * Get response,
     * from specified info array.
     *
     * @param array $tabInfo
     * @return null|DataHttpResponse
     */
    public static function getObjResponse(array $tabInfo)
    {
        // Return result
        return (
            (
                isset($tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO][ConstPersistor::TAB_INFO_KEY_RESPONSE]) &&
                ($tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO][ConstPersistor::TAB_INFO_KEY_RESPONSE] instanceof DataHttpResponse)
            ) ?
                $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO][ConstPersistor::TAB_INFO_KEY_RESPONSE] :
                null
        );
    }



}