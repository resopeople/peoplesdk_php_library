<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\data\data\table\path\library\ToolBoxPathTableData;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;
use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\library\model\repository\library\ToolBoxRepository;



class ToolBoxPersistor extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get persistence execution configuration engine,
     * from specified repository|collection repository configuration,
     * and specified repository|collection repository execution configuration.
     *
     * @param string $strConfigKeyPersistConfig
     * @param string $strConfigKeySubActionConfig
     * @param string $strConfigKeySubActionType
     * @param string $strConfigKeyRequestSndInfoPath
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param array $tabExecConfig = null
     * @param SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    protected static function getTabExecConfigEngine(
        $strConfigKeyPersistConfig,
        $strConfigKeySubActionConfig,
        $strConfigKeySubActionType,
        $strConfigKeyRequestSndInfoPath,
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = null;

        if(
            is_string($strConfigKeyPersistConfig) &&
            is_string($strConfigKeySubActionConfig) &&
            is_string($strConfigKeySubActionType) &&
            is_string($strConfigKeyRequestSndInfoPath) &&
            is_string($strExecConfigPersistConfigPath)
        )
        {
            // Set persistence configuration, if required
            $result = (
                (
                    isset($tabConfig[$strConfigKeyPersistConfig]) &&
                    is_array($tabConfig[$strConfigKeyPersistConfig])
                ) ?
                    $tabConfig[$strConfigKeyPersistConfig] :
                    $result
            );

            // Set additional persistence configuration, from sub-action, if required
            $strSubActionType = (
                isset($tabExecConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE]) ?
                    $tabExecConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE] :
                    (
                        isset($tabConfig[$strConfigKeySubActionType]) ?
                            $tabConfig[$strConfigKeySubActionType] :
                            null
                    )
            );
            $result = (
                (
                    (!is_null($strSubActionType)) &&
                    isset($tabConfig[$strConfigKeySubActionConfig][$strSubActionType]) &&
                    is_array($tabConfig[$strConfigKeySubActionConfig][$strSubActionType])
                ) ?
                    (
                        (!is_null($result)) ?
                            ToolBoxTable::getTabMerge($result, $tabConfig[$strConfigKeySubActionConfig][$strSubActionType]) :
                            $tabConfig[$strConfigKeySubActionConfig][$strSubActionType]
                    ) :
                    $result
            );

            // Set formatted request sending information, if required
            $strPathSeparator = ToolBoxRepository::getStrPersistConfigPathSeparatorEngine($tabConfig);
            $strRequestSndInfoPath = ToolBoxRepository::getStrPersistConfigRequestSndInfoPathEngine(
                $tabConfig,
                $strConfigKeyRequestSndInfoPath
            );
            $tabRequestSndInfo = (
                (
                    (!is_null($result)) &&
                    is_array($tabRequestSndInfo = ToolBoxPathTableData::getValue(
                        $result,
                        $strRequestSndInfoPath,
                        $strPathSeparator
                    ))
                ) ?
                    $tabRequestSndInfo :
                    null
            );
            if(
                (!is_null($objRequestSndInfoFactory)) &&
                (!is_null($tabRequestSndInfo))
            )
            {
                $tabRequestSndInfo = $objRequestSndInfoFactory->getTabSndInfo($tabRequestSndInfo);
                ToolBoxPathTableData::setValue($result, $strRequestSndInfoPath, $tabRequestSndInfo, $strPathSeparator);
            }

            // Set additional persistence configuration, if required
            $tabPersistConfig = (
                (
                    (!is_null($tabExecConfig)) &&
                    is_array($tabPersistConfig = ToolBoxPathTableData::getValue(
                        $tabExecConfig,
                        $strExecConfigPersistConfigPath,
                        $strPathSeparator
                    ))
                ) ?
                    $tabPersistConfig :
                    null
            );
            if(!is_null($tabPersistConfig))
            {
                if(!is_null($result))
                {
                    $result = ToolBoxTable::getTabMerge($tabPersistConfig, $result);

                    // Format request sending information, if required
                    if(
                        (!is_null($tabRequestSndInfo)) &&
                        is_array($tabPersistConfigRequestSndInfo = ToolBoxPathTableData::getValue(
                            $tabPersistConfig,
                            $strRequestSndInfoPath,
                            $strPathSeparator
                        ))
                    )
                    {
                        $tabRequestSndInfo = ToolBoxSndInfo::getTabMergeSndInfo($tabPersistConfigRequestSndInfo, $tabRequestSndInfo);
                        ToolBoxPathTableData::setValue($result, $strRequestSndInfoPath, $tabRequestSndInfo, $strPathSeparator);
                    }
                }
                else
                {
                    $result = $tabPersistConfig;
                }
            }

            // Check persistence execution configuration is valid, if required
            $result = (
                (
                    is_null($result) ||
                    SndInfoInvalidFormatException::checkInfoIsValid($tabRequestSndInfo)
                ) ?
                    $result :
                    null
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to get data,
     * from specified repository configuration,
     * and specified repository execution configuration.
     *
     * Repository configuration array format:
     * @see ToolBoxRepository::checkRepoConfigIsValid() configuration array format.
     *
     * Repository execution configuration array format:
     * @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     * for persistence action get.
     *
     * Return format:
     * Null or @see DefaultPersistor::getData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigGetData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to get index array of data.
     * from specified collection repository configuration,
     * and specified collection repository execution configuration.
     *
     * Collection repository configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Collection repository execution configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     * for persistence action get.
     *
     * Return format:
     * Null or @see DefaultPersistor::getTabData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigGetTabData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkCollectionRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkCollectionRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to get index array of searched data.
     * from specified collection repository configuration,
     * and specified collection repository execution configuration.
     *
     * Collection repository configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Collection repository execution configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     * for persistence action search.
     *
     * Return format:
     * Null or @see DefaultPersistor::getTabSearchData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigSearchTabData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkCollectionRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkCollectionRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to create data.
     * from specified repository configuration,
     * and specified repository execution configuration.
     *
     * Repository configuration array format:
     * @see ToolBoxRepository::checkRepoConfigIsValid() configuration array format.
     *
     * Repository execution configuration array format:
     * @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     * for persistence action create.
     *
     * Return format:
     * Null or @see DefaultPersistor::createData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigCreateData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to create index array of data.
     * from specified collection repository configuration,
     * and specified collection repository execution configuration.
     *
     * Collection repository configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Collection repository execution configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     * for persistence action create.
     *
     * Return format:
     * Null or @see DefaultPersistor::createTabData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigCreateTabData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkCollectionRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkCollectionRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to update data.
     * from specified repository configuration,
     * and specified repository execution configuration.
     *
     * Repository configuration array format:
     * @see ToolBoxRepository::checkRepoConfigIsValid() configuration array format.
     *
     * Repository execution configuration array format:
     * @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     * for persistence action update.
     *
     * Return format:
     * Null or @see DefaultPersistor::updateData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigUpdateData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to update index array of data.
     * from specified collection repository configuration,
     * and specified collection repository execution configuration.
     *
     * Collection repository configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Collection repository execution configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     * for persistence action update.
     *
     * Return format:
     * Null or @see DefaultPersistor::updateTabData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigUpdateTabData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkCollectionRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkCollectionRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to delete data.
     * from specified repository configuration,
     * and specified repository execution configuration.
     *
     * Repository configuration array format:
     * @see ToolBoxRepository::checkRepoConfigIsValid() configuration array format.
     *
     * Repository execution configuration array format:
     * @see ToolBoxRepository::checkRepoExecConfigIsValid() configuration array format,
     * for persistence action delete.
     *
     * Return format:
     * Null or @see DefaultPersistor::deleteData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigDeleteData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get persistence execution configuration, to delete index array of data.
     * from specified collection repository configuration,
     * and specified collection repository execution configuration.
     *
     * Collection repository configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Collection repository execution configuration array format:
     * @see ToolBoxRepository::checkCollectionRepoExecConfigIsValid() configuration array format,
     * for persistence action delete.
     *
     * Return format:
     * Null or @see DefaultPersistor::deleteTabData() execution configuration array.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param null|array $tabExecConfig = null
     * @param null|SndInfoFactoryInterface $objRequestSndInfoFactory = null
     * @return null|array
     */
    public static function getTabExecConfigDeleteTabData(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        array $tabExecConfig = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null
    )
    {
        // Init var
        $result = (
            (
                ToolBoxRepository::checkCollectionRepoConfigIsValid($tabConfig) &&
                ToolBoxRepository::checkCollectionRepoExecConfigIsValid($tabExecConfig)
            ) ?
                static::getTabExecConfigEngine(
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE,
                    ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG_REQUEST_SND_INFO_PATH,
                    $tabConfig,
                    $strExecConfigPersistConfigPath,
                    $tabExecConfig,
                    $objRequestSndInfoFactory
                ) :
                null
        );

        // Return result
        return $result;
    }



}