<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\library;



class ConstRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR = 'persist_config_path_separator';
    const TAB_CONFIG_KEY_PERSIST_GET_CONFIG = 'persist_get_config';
    const TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG = 'persist_get_sub_action_config';
    const TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE = 'persist_get_sub_action_type';
    const TAB_CONFIG_KEY_PERSIST_GET_CONFIG_REQUEST_SND_INFO_PATH = 'persist_get_config_request_snd_info_path';
    const TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG = 'persist_search_config';
    const TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG = 'persist_search_sub_action_config';
    const TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_TYPE = 'persist_search_sub_action_type';
    const TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG_REQUEST_SND_INFO_PATH = 'persist_search_config_request_snd_info_path';
    const TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG = 'persist_create_config';
    const TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG = 'persist_create_sub_action_config';
    const TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE = 'persist_create_sub_action_type';
    const TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG_REQUEST_SND_INFO_PATH = 'persist_create_config_request_snd_info_path';
    const TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG = 'persist_update_config';
    const TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG = 'persist_update_sub_action_config';
    const TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE = 'persist_update_sub_action_type';
    const TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG_REQUEST_SND_INFO_PATH = 'persist_update_config_request_snd_info_path';
    const TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG = 'persist_delete_config';
    const TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG = 'persist_delete_sub_action_config';
    const TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE = 'persist_delete_sub_action_type';
    const TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG_REQUEST_SND_INFO_PATH = 'persist_delete_config_request_snd_info_path';
    const TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID = 'select_entity_attribute_name_save_id';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE = 'persist_sub_action_type';

    // Persistence action type configuration
    const PERSIST_ACTION_TYPE_GET = 'get';
    const PERSIST_ACTION_TYPE_SEARCH = 'search';
    const PERSIST_ACTION_TYPE_CREATE = 'create';
    const PERSIST_ACTION_TYPE_UPDATE = 'update';
    const PERSIST_ACTION_TYPE_DELETE = 'delete';


	
    // Exception message constants
    const EXCEPT_MSG_REQUEST_SND_INFO_FACTORY_INVALID_FORMAT =
        'Following request sending information factory "%1$s" invalid! It must be a request sending information factory.';
    const EXCEPT_MSG_PERSIST_ACTION_UNABLE = 'Following persistence action "%1$s" unable!';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of persistence action types,
     * for repository, or collection repository.
     *
     * @param boolean $boolCollectionRequired = false
     * @return array
     */
    public static function getTabPersistActionType($boolCollectionRequired = false)
    {
        // Init var
        $boolCollectionRequired = (is_bool($boolCollectionRequired) ? $boolCollectionRequired : false);
        $result = array(
            self::PERSIST_ACTION_TYPE_GET,
            self::PERSIST_ACTION_TYPE_CREATE,
            self::PERSIST_ACTION_TYPE_UPDATE,
            self::PERSIST_ACTION_TYPE_DELETE
        );

        // Add action types, for collection repository, if required
        if($boolCollectionRequired)
        {
            array_splice($result, 1, 0, self::PERSIST_ACTION_TYPE_SEARCH);
        }

        // Return result
        return $result;
    }



    /**
     * Get configuration key,
     * to get persistence configuration,
     * from specified persistence action type.
     *
     * @param string $strPersistActionType
     * @return null|string
     */
    public static function getStrConfigKeyPersistConfig($strPersistActionType)
    {
        // Init var
        $result = null;

        // Get configuration key, if required
        if(is_string($strPersistActionType))
        {
            switch($strPersistActionType)
            {
                case self::PERSIST_ACTION_TYPE_GET:
                    $result = self::TAB_CONFIG_KEY_PERSIST_GET_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_SEARCH:
                    $result = self::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_CREATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_UPDATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_DELETE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG;
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get configuration key,
     * to get persistence sub-action configuration,
     * from specified persistence action type.
     *
     * @param string $strPersistActionType
     * @return null|string
     */
    public static function getStrConfigKeyPersistSubActionConfig($strPersistActionType)
    {
        // Init var
        $result = null;

        // Get configuration key, if required
        if(is_string($strPersistActionType))
        {
            switch($strPersistActionType)
            {
                case self::PERSIST_ACTION_TYPE_GET:
                    $result = self::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_SEARCH:
                    $result = self::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_CREATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_UPDATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG;
                    break;

                case self::PERSIST_ACTION_TYPE_DELETE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG;
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get configuration key,
     * to get persistence sub-action type,
     * from specified persistence action type.
     *
     * @param string $strPersistActionType
     * @return null|string
     */
    public static function getStrConfigKeyPersistSubActionType($strPersistActionType)
    {
        // Init var
        $result = null;

        // Get configuration key, if required
        if(is_string($strPersistActionType))
        {
            switch($strPersistActionType)
            {
                case self::PERSIST_ACTION_TYPE_GET:
                    $result = self::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE;
                    break;

                case self::PERSIST_ACTION_TYPE_SEARCH:
                    $result = self::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_TYPE;
                    break;

                case self::PERSIST_ACTION_TYPE_CREATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE;
                    break;

                case self::PERSIST_ACTION_TYPE_UPDATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE;
                    break;

                case self::PERSIST_ACTION_TYPE_DELETE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE;
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get configuration key,
     * to get request sending information path,
     * used to retrieve its data, from persistence configuration,
     * from specified persistence action type.
     *
     * @param string $strPersistActionType
     * @return null|string
     */
    public static function getStrConfigKeyPersistConfigRequestSndInfoPath($strPersistActionType)
    {
        // Init var
        $result = null;

        // Get configuration key, if required
        if(is_string($strPersistActionType))
        {
            switch($strPersistActionType)
            {
                case self::PERSIST_ACTION_TYPE_GET:
                    $result = self::TAB_CONFIG_KEY_PERSIST_GET_CONFIG_REQUEST_SND_INFO_PATH;
                    break;

                case self::PERSIST_ACTION_TYPE_SEARCH:
                    $result = self::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG_REQUEST_SND_INFO_PATH;
                    break;

                case self::PERSIST_ACTION_TYPE_CREATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG_REQUEST_SND_INFO_PATH;
                    break;

                case self::PERSIST_ACTION_TYPE_UPDATE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG_REQUEST_SND_INFO_PATH;
                    break;

                case self::PERSIST_ACTION_TYPE_DELETE:
                    $result = self::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG_REQUEST_SND_INFO_PATH;
                    break;
            }
        }

        // Return result
        return $result;
    }



}