<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\data\data\table\path\library\ToolBoxPathTableData;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\entity\factory\api\EntityFactoryInterface;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\request\factory\model\DefaultRequestFactory;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use people_sdk\library\table\library\ToolBoxTable;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;
use people_sdk\library\model\repository\library\ConstRepository;



class ToolBoxRepository extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified persistence action is valid,
     * for repository.
     *
     * @param string $strPersistActionType
     * @return boolean
     */
    public static function checkRepoPersistActionIsValid($strPersistActionType)
    {
        // Return result
        return (
            is_string($strPersistActionType) &&
            in_array(
                $strPersistActionType,
                ConstRepository::getTabPersistActionType(false)
            )
        );
    }



    /**
     * Check if specified persistence action is valid,
     * for collection repository.
     *
     * @param string $strPersistActionType
     * @return boolean
     */
    public static function checkCollectionRepoPersistActionIsValid($strPersistActionType)
    {
        // Return result
        return (
            is_string($strPersistActionType) &&
            in_array(
                $strPersistActionType,
                ConstRepository::getTabPersistActionType(true)
            )
        );
    }



    /**
     * Check if specified persistence configuration, has valid format engine.
     *
     * @param mixed $config
     * @param string $strConfigKeyConfig
     * @param string $strConfigKeySubActionConfig
     * @param string $strConfigKeySubActionType
     * @param string $strConfigKeyRequestSndInfoPath
     * @return boolean
     */
    protected static function checkPersistConfigIsValidEngine(
        $config,
        $strConfigKeyConfig,
        $strConfigKeySubActionConfig,
        $strConfigKeySubActionType,
        $strConfigKeyRequestSndInfoPath
    )
    {
        // Init sub-action configuration array check function
        $checkTabSubActionConfigIsValid = function($tabSubActionConfig)
        {
            $result = is_array($tabSubActionConfig);

            // Check each sub-action configuration valid, if required
            if($result)
            {
                $tabSubActionType = array_keys($tabSubActionConfig);
                for($intCpt = 0; ($intCpt < count($tabSubActionType)) && $result; $intCpt++)
                {
                    $strSubActionType = $tabSubActionType[$intCpt];
                    $tabPersistConfig = $tabSubActionConfig[$strSubActionType];
                    $result = (
                        // Check valid sub-action type configuration
                        is_string($strSubActionType) &&
                        (trim($strSubActionType) != '') &&

                        // Check valid sub-action persistence configuration
                        is_array($tabPersistConfig)
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            is_string($strConfigKeyConfig) &&
            is_string($strConfigKeySubActionConfig) &&
            is_string($strConfigKeySubActionType) &&
            is_array($config) &&

            // Check valid persistence configuration
            (
                (!isset($config[$strConfigKeyConfig])) ||
                is_array($config[$strConfigKeyConfig])
            ) &&

            // Check valid persistence sub-action configuration
            (
                (!isset($config[$strConfigKeySubActionConfig])) ||
                $checkTabSubActionConfigIsValid($config[$strConfigKeySubActionConfig])
            ) &&

            // Check valid persistence sub-action type
            (
                (!isset($config[$strConfigKeySubActionType])) ||
                (
                    is_string($config[$strConfigKeySubActionType]) &&
                    (trim($config[$strConfigKeySubActionType]) != '') &&
                    // Sub-action type must exists, on persistence sub-action configuration
                    isset($config[$strConfigKeySubActionConfig][$config[$strConfigKeySubActionType]])
                )
            ) &&

            // Check valid persistence configuration request sending information path
            (
                (!isset($config[$strConfigKeyRequestSndInfoPath])) ||
                (
                    is_string($config[$strConfigKeyRequestSndInfoPath]) &&
                    (trim($config[$strConfigKeyRequestSndInfoPath]) != '')
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified repository config has valid format.
     *
     * Configuration array format:
     * [
     *     persist_config_path_separator(optional: got '/', if not found):
     *         "string path separator,
     *         used to retrieve data, from persistence configuration",
     *
     *     persist_get_config(optional): [
     *         Basic persistence configuration, used for persistence action get:
     *         @see DefaultPersistor::getData() execution configuration array format
     *     ],
     *
     *     persist_get_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action get:
     *             @see DefaultPersistor::getData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_get_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action get.
     *         Scope of available types: persist_get_sub_action_config sub-action types",
     *
     *     persist_get_config_request_snd_info_path(optional: got 'get_request_config/snd_info'
     *     (@see DefaultPersistor::getData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action get",
     *
     *     persist_create_config(optional): [
     *         Basic persistence configuration, used for persistence action create:
     *         @see DefaultPersistor::createData() execution configuration array format
     *     ],
     *
     *     persist_create_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action create:
     *             @see DefaultPersistor::createData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_create_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action create.
     *         Scope of available types: persist_create_sub_action_config sub-action types",
     *
     *     persist_create_config_request_snd_info_path(optional: got 'create_request_config/snd_info'
     *     (@see DefaultPersistor::createData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action create",
     *
     *     persist_update_config(optional): [
     *         Basic persistence configuration, used for persistence action update:
     *         @see DefaultPersistor::updateData() execution configuration array format
     *     ],
     *
     *     persist_update_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action update:
     *             @see DefaultPersistor::updateData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_update_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action update.
     *         Scope of available types: persist_update_sub_action_config sub-action types",
     *
     *     persist_update_config_request_snd_info_path(optional: got 'update_request_config/snd_info'
     *     (@see DefaultPersistor::updateData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action update",
     *
     *     persist_delete_config(optional): [
     *         Basic persistence configuration, used for persistence action delete:
     *         @see DefaultPersistor::deleteData() execution configuration array format
     *     ],
     *
     *     persist_delete_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action delete:
     *             @see DefaultPersistor::deleteData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_delete_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action delete.
     *         Scope of available types: persist_delete_sub_action_config sub-action types",
     *
     *     persist_delete_config_request_snd_info_path(optional: got 'delete_request_config/snd_info'
     *     (@see DefaultPersistor::deleteData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action delete"
     * ]
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkRepoConfigIsValid($config)
    {
        // Init var
        $result =
            is_array($config) &&

            // Check valid persistence configuration path separator
            (
                (!isset($config[ConstRepository::TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR])) ||
                (
                    is_string($config[ConstRepository::TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR]) &&
                    (trim($config[ConstRepository::TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR]) != '')
                )
            ) &&

            // Check valid persistence configuration, for persistence action get
            static::checkPersistConfigIsValidEngine(
                $config,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG_REQUEST_SND_INFO_PATH
            ) &&

            // Check valid persistence configuration, for persistence action create
            static::checkPersistConfigIsValidEngine(
                $config,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG_REQUEST_SND_INFO_PATH
            ) &&

            // Check valid persistence configuration, for persistence action update
            static::checkPersistConfigIsValidEngine(
                $config,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG_REQUEST_SND_INFO_PATH
            ) &&

            // Check valid persistence configuration, for persistence action delete
            static::checkPersistConfigIsValidEngine(
                $config,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG_REQUEST_SND_INFO_PATH
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified collection repository config has valid format.
     *
     * Configuration array format:
     * [
     *     persist_config_path_separator(optional: got '/', if not found):
     *         "string path separator,
     *         used to retrieve data, from persistence configuration",
     *
     *     persist_get_config(optional): [
     *         Basic persistence configuration, used for persistence action get:
     *         @see DefaultPersistor::getTabData() execution configuration array format
     *     ],
     *
     *     persist_get_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action get:
     *             @see DefaultPersistor::getTabData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_get_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action get.
     *         Scope of available types: persist_get_sub_action_config sub-action types",
     *
     *     persist_get_config_request_snd_info_path(optional: got 'get_multi_request_config/snd_info'
     *     (@see DefaultPersistor::getTabData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action get",
     *
     *     persist_search_config(optional): [
     *         Basic persistence configuration, used for persistence action search:
     *         @see DefaultPersistor::getTabSearchData() execution configuration array format
     *     ],
     *
     *     persist_search_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action search:
     *             @see DefaultPersistor::getTabSearchData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_search_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action search.
     *         Scope of available types: persist_search_sub_action_config sub-action types",
     *
     *     persist_search_config_request_snd_info_path(optional: got 'search_multi_request_config/snd_info'
     *     (@see DefaultPersistor::getTabSearchData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action search",
     *
     *     persist_create_config(optional): [
     *         Basic persistence configuration, used for persistence action create:
     *         @see DefaultPersistor::createTabData() execution configuration array format
     *     ],
     *
     *     persist_create_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action create:
     *             @see DefaultPersistor::createTabData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_create_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action create.
     *         Scope of available types: persist_create_sub_action_config sub-action types",
     *
     *     persist_create_config_request_snd_info_path(optional: got 'create_multi_request_config/snd_info'
     *     (@see DefaultPersistor::createTabData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action create",
     *
     *     persist_update_config(optional): [
     *         Basic persistence configuration, used for persistence action update:
     *         @see DefaultPersistor::updateTabData() execution configuration array format
     *     ],
     *
     *     persist_update_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action update:
     *             @see DefaultPersistor::updateTabData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_update_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action update.
     *         Scope of available types: persist_update_sub_action_config sub-action types",
     *
     *     persist_update_config_request_snd_info_path(optional: got 'update_multi_request_config/snd_info'
     *     (@see DefaultPersistor::updateTabData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action update",
     *
     *      persist_delete_config(optional): [
     *         Basic persistence configuration, used for persistence action delete:
     *         @see DefaultPersistor::deleteTabData() execution configuration array format
     *     ],
     *
     *     persist_delete_sub_action_config(optional): [
     *         // Sub-action 1
     *         'string sub-action 1 type': [
     *             Additional persistence configuration, used for persistence action delete:
     *             @see DefaultPersistor::deleteTabData() execution configuration array format
     *         ],
     *         ...,
     *         // Sub-action N
     *         ...
     *     ],
     *
     *     persist_delete_sub_action_type(optional):
     *         "string sub-action type,
     *         to select additional persistence configuration, for persistence action delete.
     *         Scope of available types: persist_delete_sub_action_config sub-action types",
     *
     *     persist_delete_config_request_snd_info_path(optional: got 'delete_multi_request_config/snd_info'
     *     (@see DefaultPersistor::deleteTabData() execution configuration array format and
     *     @see DefaultRequestFactory configuration array format),
     *     if not found):
     *         "string request sending information path,
     *         used to retrieve its data, from persistence configuration, for persistence action delete",
     *
     *     select_entity_attribute_name_save_id(optional: no entity selection done, if not found):
     *          "string entity attribute save name id"
     * ]
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkCollectionRepoConfigIsValid($config)
    {
        // Init var
        $result =
            self::checkRepoConfigIsValid($config) &&

            // Check valid persistence configuration, for persistence action search
            self::checkPersistConfigIsValidEngine(
                $config,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_TYPE,
                ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG_REQUEST_SND_INFO_PATH
            ) &&

            // Check valid select entity attribute save name id
            (
                (!isset($config[ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID])) ||
                (
                    is_string($config[ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID]) &&
                    (trim($config[ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID]) != '')
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified repository execution config has valid format.
     *
     * Configuration array format:
     * Null
     * or
     * [
     *     Default repository configuration,
     *
     *     persist_sub_action_type(optional:
     *     got configured repository sub-action type, for adequate persistence action
     *     (@see ConstRepository::getStrConfigKeyPersistSubActionType() ),
     *     if not found):
     *         "string sub-action type,
     *         to select additional persistence configuration, for adequate persistence action.
     *         Scope of available types: configured repository sub-action types, for adequate persistence action
     *         (@see ConstRepository::getStrConfigKeyPersistSubActionConfig() )"
     * ]
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkRepoExecConfigIsValid($config)
    {
        // Init var
        $result = (
            is_null($config) ||

            // Check valid array
            (
                is_array($config) &&

                // Check valid persistence sub-action type
                (
                    (!isset($config[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE])) ||
                    (
                        is_string($config[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE]) &&
                        (trim($config[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE]) != '')
                    )
                )
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified collection repository execution config has valid format.
     *
     * Configuration array format:
     * Null
     * or
     * [
     *     Default collection repository configuration,
     *
     *     persist_sub_action_type(optional:
     *     got configured collection repository sub-action type, for adequate persistence action
     *     (@see ConstRepository::getStrConfigKeyPersistSubActionType() ),
     *     if not found):
     *         "string sub-action type,
     *         to select additional persistence configuration, for adequate persistence action.
     *         Scope of available types: configured collection repository sub-action types, for adequate persistence action
     *         (@see ConstRepository::getStrConfigKeyPersistSubActionConfig() )"
     * ]
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkCollectionRepoExecConfigIsValid($config)
    {
        // Return result
        return static::checkRepoExecConfigIsValid($config);
    }



    /**
     * Check if specified execution config has valid format engine,
     * for persistence action.
     *
     * @param mixed $config
     * @param array $tabConfig
     * @param string $strConfigKeySubActionConfig
     * @return boolean
     */
    protected static function checkExecConfigIsValidForPersistActionEngine(
        $config,
        array $tabConfig,
        $strConfigKeySubActionConfig
    )
    {
        // Init var
        $result =
            is_string($strConfigKeySubActionConfig) &&

            // Check valid persistence sub-action type
            (
                (!isset($config[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE])) ||
                (
                    is_string($config[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE]) &&
                    // Sub-action type must exists, on persistence sub-action configuration
                    isset(
                        $tabConfig[$strConfigKeySubActionConfig]
                        [$config[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE]]
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified repository execution config has valid format,
     * for specified persistence action,
     * and specified repository configuration.
     *
     * Configuration array format:
     * @see checkRepoExecConfigIsValid() configuration array format.
     *
     * Configuration array format:
     * @see checkRepoConfigIsValid() configuration array format.
     *
     * Persistence action type format:
     * String, from available scopes:
     * @see ConstRepository::getTabPersistActionType() returned array, for repository.
     *
     * @param mixed $config
     * @param array $tabConfig
     * @param string $strPersistActionType
     * @return boolean
     */
    public static function checkRepoExecConfigIsValidForPersistAction(
        $config,
        array $tabConfig,
        $strPersistActionType
    )
    {
        // Init var
        $strConfigKeySubActionConfig = ConstRepository::getStrConfigKeyPersistSubActionConfig($strPersistActionType);
        $result =
            // Check valid persistence action
            static::checkRepoPersistActionIsValid($strPersistActionType) &&

            // Check valid configuration
            (
                is_null($config) ||
                (
                    static::checkExecConfigIsValidForPersistActionEngine(
                        $config,
                        $tabConfig,
                        $strConfigKeySubActionConfig
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified collection repository execution config has valid format,
     * for specified persistence action,
     * and specified collection repository configuration.
     *
     * Configuration array format:
     * @see checkCollectionRepoExecConfigIsValid() configuration array format.
     *
     * Configuration array format:
     * @see checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Persistence action type format:
     * String, from available scopes:
     * @see ConstRepository::getTabPersistActionType() returned array, for collection repository.
     *
     * @param mixed $config
     * @param array $tabConfig
     * @param string $strPersistActionType
     * @return boolean
     */
    public static function checkCollectionRepoExecConfigIsValidForPersistAction(
        $config,
        array $tabConfig,
        $strPersistActionType
    )
    {
        // Init var
        $strConfigKeySubActionConfig = ConstRepository::getStrConfigKeyPersistSubActionConfig($strPersistActionType);
        $result =
            // Check valid persistence action
            static::checkCollectionRepoPersistActionIsValid($strPersistActionType) &&

            // Check valid configuration
            (
                is_null($config) ||
                (
                    static::checkExecConfigIsValidForPersistActionEngine(
                        $config,
                        $tabConfig,
                        $strConfigKeySubActionConfig
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified repository persistence action is enabled,
     * from specified repository configuration.
     *
     * Configuration array format:
     * @see checkRepoConfigIsValid() configuration array format.
     *
     * Persistence action type format:
     * String, from available scopes:
     * @see ConstRepository::getTabPersistActionType() returned array, for repository.
     *
     * @param array $tabConfig
     * @param string $strPersistActionType
     * @return boolean
     */
    public static function checkRepoPersistActionEnabled(
        array $tabConfig,
        $strPersistActionType
    )
    {
        // Init var
        $result =
            is_string($strPersistActionType) &&
            (
                // Check persistence action get enabled
                (
                    ($strPersistActionType == ConstRepository::PERSIST_ACTION_TYPE_GET) &&
                    (
                        isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG]) ||
                        (
                            isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG]) &&
                            is_array($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG]) &&
                            (count($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG]) > 0)
                        )
                    )
                ) ||

                // Check persistence action create enabled
                (
                    ($strPersistActionType == ConstRepository::PERSIST_ACTION_TYPE_CREATE) &&
                    (
                        isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG]) ||
                        (
                            isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG]) &&
                            is_array($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG]) &&
                            (count($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG]) > 0)
                        )
                    )
                ) ||

                // Check persistence action update enabled
                (
                    ($strPersistActionType == ConstRepository::PERSIST_ACTION_TYPE_UPDATE) &&
                    (
                        isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG]) ||
                        (
                            isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG]) &&
                            is_array($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG]) &&
                            (count($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG]) > 0)
                        )
                    )
                ) ||

                // Check persistence action delete enabled
                (
                    ($strPersistActionType == ConstRepository::PERSIST_ACTION_TYPE_DELETE) &&
                    (
                        isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG]) ||
                        (
                            isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG]) &&
                            is_array($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG]) &&
                            (count($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG]) > 0)
                        )
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified collection repository persistence action is enabled,
     * from specified collection repository configuration.
     *
     * Configuration array format:
     * @see checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Persistence action type format:
     * String, from available scopes:
     * @see ConstRepository::getTabPersistActionType() returned array, for collection repository.
     *
     * @param array $tabConfig
     * @param string $strPersistActionType
     * @return boolean
     */
    public static function checkCollectionRepoPersistActionEnabled(
        array $tabConfig,
        $strPersistActionType
    )
    {
        // Init var
        $result =
            is_string($strPersistActionType) &&
            (
                static::checkRepoPersistActionEnabled($tabConfig, $strPersistActionType) ||

                // Check persistence action search enabled
                (
                    ($strPersistActionType == ConstRepository::PERSIST_ACTION_TYPE_SEARCH) &&
                    (
                        isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG]) ||
                        (
                            isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG]) &&
                            is_array($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG]) &&
                            (count($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG]) > 0)
                        )
                    )
                )
            );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get path separator engine,
     * used to retrieve data, from persistence configuration,
     * from specified repository|collection repository configuration.
     *
     * Repository|collection repository configuration array format:
     * @see checkRepoConfigIsValid() configuration array format.
     * or
     * @see checkCollectionRepoConfigIsValid() configuration array format.
     *
     * @param array $tabConfig
     * @return string
     */
    public static function getStrPersistConfigPathSeparatorEngine(array $tabConfig)
    {
        // Return result
        return (
            (
                isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR]) &&
                is_string($tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR])
            ) ?
                $tabConfig[ConstRepository::TAB_CONFIG_KEY_PERSIST_CONFIG_PATH_SEPARATOR] :
                '/'
        );
    }



    /**
     * Get request sending information path engine,
     * used to retrieve its data, from persistence configuration,
     * from specified repository|collection repository configuration.
     *
     * Repository|collection repository configuration array format:
     * @see getStrPersistConfigPathSeparator() configuration array format.
     *
     * @param array $tabConfig
     * @param string $strConfigKeyRequestSndInfoPath = null
     * @return string
     */
    public static function getStrPersistConfigRequestSndInfoPathEngine(
        array $tabConfig,
        $strConfigKeyRequestSndInfoPath = null
    )
    {
        // Init var
        $strConfigKeyRequestSndInfoPath = (
            is_string($strConfigKeyRequestSndInfoPath) ?
                $strConfigKeyRequestSndInfoPath :
                null
        );
        $strPathSeparator = static::getStrPersistConfigPathSeparatorEngine($tabConfig);
        $result = (
            (
                (!is_null(($strConfigKeyRequestSndInfoPath))) &&
                isset($tabConfig[$strConfigKeyRequestSndInfoPath]) &&
                is_string($tabConfig[$strConfigKeyRequestSndInfoPath])
            ) ?
                $tabConfig[$strConfigKeyRequestSndInfoPath] :
                sprintf(
                    '%2$s%1$s%3$s',
                    $strPathSeparator,
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG,
                    ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get repository|collection repository execution configuration engine,
     * with specified persistence sub-action type.
     *
     * @param string $strPersistSubActionType
     * @param array $tabExecConfig = null
     * @return null|array
     */
    protected static function getTabExecConfigWithPersistSubActionTypeEngine(
        $strPersistSubActionType,
        array $tabExecConfig = null
    )
    {
        // Init var
        $result = (
            is_string($strPersistSubActionType) ?
                [ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE => $strPersistSubActionType] :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($result, $tabExecConfig);

        // Return result
        return $result;
    }



    /**
     * Get repository execution configuration,
     * with specified persistence sub-action type.
     *
     * Execution configuration array format:
     * Null or @see checkRepoExecConfigIsValid() configuration array format.
     *
     * Return format:
     * Execution configuration array format.
     *
     * @param string $strPersistSubActionType
     * @param array $tabExecConfig = null
     * @return null|array
     */
    public static function getTabRepoExecConfigWithPersistSubActionType(
        $strPersistSubActionType,
        array $tabExecConfig = null
    )
    {
        // Return result
        return static::getTabExecConfigWithPersistSubActionTypeEngine(
            $strPersistSubActionType,
            $tabExecConfig
        );
    }



    /**
     * Get collection repository execution configuration,
     * with specified persistence sub-action type.
     *
     * Execution configuration array format:
     * Null or @see checkCollectionRepoExecConfigIsValid() configuration array format.
     *
     * Return format:
     * Execution configuration array format.
     *
     * @param string $strPersistSubActionType
     * @param array $tabExecConfig = null
     * @return null|array
     */
    public static function getTabCollectionRepoExecConfigWithPersistSubActionType(
        $strPersistSubActionType,
        array $tabExecConfig = null
    )
    {
        // Return result
        return static::getTabExecConfigWithPersistSubActionTypeEngine(
            $strPersistSubActionType,
            $tabExecConfig
        );
    }



    /**
     * Get repository|collection repository execution configuration engine,
     * with specified request sending information.
     *
     * @param array $tabConfig
     * @param string $strExecConfigPersistConfigPath
     * @param string $strConfigKeyRequestSndInfoPath
     * @param array $tabRequestSndInfo
     * @param array $tabExecConfig = null
     * @return array
     */
    public static function getTabExecConfigWithRequestSndInfoEngine(
        array $tabConfig,
        $strExecConfigPersistConfigPath,
        $strConfigKeyRequestSndInfoPath,
        array $tabRequestSndInfo,
        array $tabExecConfig = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(array(), $tabExecConfig);

        // Set request sending information, if required
        if(is_string($strExecConfigPersistConfigPath) && is_string($strConfigKeyRequestSndInfoPath))
        {
            // Init request sending information relative path (from persistence execution configuration)
            $strRequestSndInfoPath = ToolBoxRepository::getStrPersistConfigRequestSndInfoPathEngine(
                $tabConfig,
                $strConfigKeyRequestSndInfoPath
            );

            // Init request sending information full path (from repository execution configuration)
            $strPathSeparator = ToolBoxRepository::getStrPersistConfigPathSeparatorEngine($tabConfig);
            $strRequestSndInfoPath = sprintf(
                '%2$s%1$s%3$s',
                $strPathSeparator,
                $strExecConfigPersistConfigPath,
                $strRequestSndInfoPath
            );

            // Set request sending information
            ToolBoxPathTableData::setValue(
                $result,
                $strRequestSndInfoPath,
                $tabRequestSndInfo,
                $strPathSeparator,
                true
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get entity attribute save name id,
     * used for collection repository entity selection,
     * from specified collection repository config.
     *
     * Configuration array format:
     * @see checkCollectionRepoConfigIsValid() configuration array format.
     *
     * @param array $tabConfig
     * @return null|string
     */
    public static function getStrCollectionRepoSelectEntityAttrNameSaveId(array $tabConfig)
    {
        // Return result
        return (
            (
                isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID]) &&
                is_string($tabConfig[ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID])
            ) ?
                $tabConfig[ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID] :
                null
        );
    }



    /**
     * Get new entity object,
     * for collection repository,
     * from specified entity factory,
     * and specified collection repository configuration,
     * and specified data.
     *
     * Configuration array format:
     * @see checkCollectionRepoConfigIsValid() configuration array format.
     *
     * Data array format:
     * @see CollectionRepositoryInterface::getObjEntityNew() data array format.
     *
     * @param EntityFactoryInterface $objEntityFactory
     * @param array $tabConfig
     * @param array $tabData
     * @return null|SaveEntityInterface
     */
    public static function getObjCollectionRepoEntityNew(
        EntityFactoryInterface $objEntityFactory,
        array $tabConfig,
        array $tabData
    )
    {
        // Init var
        $strAttrNmSaveId = static::getStrCollectionRepoSelectEntityAttrNameSaveId($tabConfig);
        $tabEntityFactoryExecConfig = (
            isset($tabConfig[BaseConstRepository::TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG]) ?
                $tabConfig[BaseConstRepository::TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG] :
                null
        );
        $tabEntityFactoryExecConfig  = (
            (
                ($objEntityFactory instanceof DefaultEntityFactory) &&
                (!is_null($strAttrNmSaveId))
            ) ?
                (
                    is_null($tabEntityFactoryExecConfig) ?
                        array(
                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID => $tabData[$strAttrNmSaveId]
                        ) :
                        array_merge(
                            array(
                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID => $tabData[$strAttrNmSaveId]
                            ),
                            $tabEntityFactoryExecConfig
                        )
                ) :
                $tabEntityFactoryExecConfig
        );
        $result = $objEntityFactory->getObjEntity(array(), $tabEntityFactoryExecConfig);
        $result = (($result instanceof SaveEntityInterface) ? $result : null);

        // Return result
        return $result;
    }



}