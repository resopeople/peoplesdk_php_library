<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\null_value\library;



class ConstNullValue
{
    // ******************************************************************************
    // Constants
    // ******************************************************************************

    // Null configuration
    const NULL_VALUE = '[NULL]';
    const NULL_ESCAPE_VALUE = '\\';



}