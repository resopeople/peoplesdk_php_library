<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\null_value\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\model\entity\null_value\library\ConstNullValue;



class ToolBoxNullValue extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();

	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified null escaped value.
     *
     * @param mixed $value
     * @return mixed
     */
    public static function getEscapeValue($value)
    {
        // Init var
        $strPattern = sprintf(
            '#^%2$s*%1$s$#',
            preg_quote(ConstNullValue::NULL_VALUE),
            preg_quote(ConstNullValue::NULL_ESCAPE_VALUE)
        );
        $result = (
            // Escape, if required (valid unescaped value detected)
            (
                is_string($value) &&
                (preg_match($strPattern, $value) === 1) &&
                (
                    (substr_count($value, ConstNullValue::NULL_ESCAPE_VALUE) == 0) ||
                    ((substr_count($value, ConstNullValue::NULL_ESCAPE_VALUE) % 2) > 0)
                )
            ) ?
                str_replace(
                    array(
                        ConstNullValue::NULL_ESCAPE_VALUE,
                        ConstNullValue::NULL_VALUE
                    ),
                    array(
                        ConstNullValue::NULL_ESCAPE_VALUE . ConstNullValue::NULL_ESCAPE_VALUE,
                        ConstNullValue::NULL_ESCAPE_VALUE . ConstNullValue::NULL_VALUE,
                    ),
                    $value
                ) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * Get specified null unescaped value.
     *
     * @param mixed $value
     * @return mixed
     */
    public static function getUnescapeValue($value)
    {
        // Init var
        $strPattern = sprintf(
            '#^%2$s+%1$s$#',
            preg_quote(ConstNullValue::NULL_VALUE),
            preg_quote(ConstNullValue::NULL_ESCAPE_VALUE)
        );
        $result = (
            // Unescape, if required (valid escaped value detected)
            (
                is_string($value) &&
                (preg_match($strPattern, $value) === 1) &&
                ((substr_count($value, ConstNullValue::NULL_ESCAPE_VALUE) % 2) > 0)
            ) ?
                str_replace(
                    array(
                        ConstNullValue::NULL_ESCAPE_VALUE . ConstNullValue::NULL_ESCAPE_VALUE,
                        ConstNullValue::NULL_ESCAPE_VALUE . ConstNullValue::NULL_VALUE,
                    ),
                    array(
                        ConstNullValue::NULL_ESCAPE_VALUE,
                        ConstNullValue::NULL_VALUE
                    ),
                    $value
                ) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * Get specified attribute formatted value, to save.
     *
     * @param mixed $value
     * @return mixed
     */
    public static function getAttributeValueSaveFormatGet($value)
    {
        // Return result
        return (
            (
                is_string($value) &&
                ($value == ConstNullValue::NULL_VALUE)
            ) ?
                null :
                static::getUnescapeValue($value)
        );
    }



    /**
     * Get specified attribute saved formatted value.
     *
     * @param mixed $value
     * @return mixed
     */
    public static function getAttributeValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_null($value) ?
                ConstNullValue::NULL_VALUE :
                static::getEscapeValue($value)
        );
    }



}