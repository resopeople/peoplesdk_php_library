<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\factory\exception;

use Exception;

use people_sdk\library\model\entity\factory\library\ConstEntityFactory;



class ExecConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstEntityFactory::EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid select entity id
            (
                (!isset($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID])) ||
                (!is_null(($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID])))
            ) &&

            // Check valid select entity creation required option
            (
                (!isset($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) ||
                    is_int($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) ||
                    (
                        is_string($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) &&
                        ctype_digit($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE])
                    )
                )
            ) &&

            // Check valid select entity collection selection configuration
            (
                (!isset($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_GET_CONFIG])) ||
                is_array($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_GET_CONFIG])
            ) &&

            // Check valid select entity collection setting required option
            (
                (!isset($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) ||
                    is_int($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) ||
                    (
                        is_string($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) &&
                        ctype_digit($config[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}