<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\factory\exception;

use Exception;

use liberty_code\model\entity\api\EntityCollectionInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;



class EntityCollectionInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $collection
     */
	public function __construct($collection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstEntityFactory::EXCEPT_MSG_ENTITY_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($collection), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified collection has valid format.
	 * 
     * @param mixed $collection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($collection)
    {
		// Init var
		$result = (
			(is_null($collection)) ||
			($collection instanceof EntityCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($collection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}