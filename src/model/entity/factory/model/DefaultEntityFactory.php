<?php
/**
 * Description :
 * This class allows to define entity factory class.
 * Entity factory is fix entity factory,
 * which can use a set of entities,
 * to provide new or existing entity instance.
 * Can be consider is base of all entity factory type.
 *
 * Entity factory allows to specify parts of configuration:
 * [
 *     Fix entity factory configuration,
 *
 *     select_entity_require(optional: got true, if not found):
 *         true / false,
 *         specify if entity selection required, instead of entity creation, to get entity instance,
 *
 *     select_entity_attribute_key_id(optional: no entity selection done (based on id), if not found):
 *         "string entity attribute key id",
 *
 *     select_entity_create_require(optional: got true, if not found):
 *         true / false,
 *         specify if entity creation required,
 *         if entity selection required and fails,
 *
 *     select_entity_collection_set_require(optional: got false, if not found):
 *         true / false,
 *         specify if entity setting required, on entity collection,
 *         if entity selection required and entity creation done
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\factory\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\ItemEntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\library\model\entity\factory\exception\EntityCollectionInvalidFormatException;
use people_sdk\library\model\entity\factory\exception\ConfigInvalidFormatException;
use people_sdk\library\model\entity\factory\exception\ExecConfigInvalidFormatException;



/**
 * @method null|EntityCollectionInterface getObjEntityCollection() Get entity collection object.
 * @method void setObjEntityCollection(null|EntityCollectionInterface $objEntityCollection) Set entity collection object.
 */
abstract class DefaultEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|EntityCollectionInterface $objEntityCollection = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init entity collection
        $this->setObjEntityCollection($objEntityCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstEntityFactory::DATA_KEY_DEFAULT_ENTITY_COLLECTION))
        {
            $this->__beanTabData[ConstEntityFactory::DATA_KEY_DEFAULT_ENTITY_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstEntityFactory::DATA_KEY_DEFAULT_ENTITY_COLLECTION,
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstEntityFactory::DATA_KEY_DEFAULT_ENTITY_COLLECTION:
                    EntityCollectionInvalidFormatException::setCheck($value);
                    break;

                case BaseConstEntityFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if entity selection required.
     *
     * @return boolean
     */
    public function checkSelectEntityRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjEntityCollection())) &&
            (
                (!isset($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_REQUIRE])) ||
                (intval($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check if entity creation required, if no entity found,
     * during entity selection.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntityNew() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkSelectEntityCreateRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkSelectEntityRequired() &&
            (
                isset($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) ?
                    (intval($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) != 0) :
                    (
                        isset($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) ?
                            (intval($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE]) != 0) :
                            true
                    )
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check if created entity setting required, on entity collection,
     * during entity selection.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntityNew() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkSelectEntityCollectionSetRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkSelectEntityRequired() &&
            (
                isset($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) ?
                    (intval($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) != 0) :
                    (
                        isset($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) ?
                            (intval($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE]) != 0) :
                            false
                    )
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity attribute key id,
     * used for entity selection.
     *
     * @return null|string
     */
    public function getStrSelectEntityAttrKeyId()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID]) ?
                $tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity id,
     * from specified execution configuration,
     * used for entity selection.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntityNew() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|mixed
     * @throws ExecConfigInvalidFormatException
     */
    protected function getSelectEntityId(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = (
            isset($tabConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID]) ?
                $tabConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity collection selection configuration,
     * from specified execution configuration,
     * used for entity selection.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntityNew() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabSelectEntityCollectionGetConfig(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = (
            isset($tabConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_GET_CONFIG]) ?
                $tabConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_GET_CONFIG] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get selected object instance entity,
     * from entity collection.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntityNew() configuration array format.
     *
     * @param array $tabConfig = null
     * @param boolean &$boolSelectRequire = false
     * @return null|EntityInterface
     * @throws ExecConfigInvalidFormatException
     */
    protected function getObjSelectEntity(
        array $tabConfig = null,
        &$boolSelectRequire = false
    )
    {
        // Init var
        $id = $this->getSelectEntityId($tabConfig);
        $tabGetConfig = $this->getTabSelectEntityCollectionGetConfig($tabConfig);
        $boolSelectRequire = ((!is_null($id)) || (!is_null($tabGetConfig)));
        $result = null;

        // Select entity, if required
        if($boolSelectRequire)
        {
            // Init var
            $objEntityCollection = $this->getObjEntityCollection();
            $strClassPath = $this->getStrEntityClassPath();

            // Check configuration
            if(!$this->checkSelectEntityRequired())
            {
                throw new ExecConfigInvalidFormatException(serialize($tabConfig));
            }

            // Select entity from id, if required
            if(!is_null($id))
            {
                // Init var
                $strAttrKeyId = $this->getStrSelectEntityAttrKeyId();

                // Check configuration
                if(is_null($strAttrKeyId))
                {
                    throw new ExecConfigInvalidFormatException(serialize($tabConfig));
                }

                // Run each entity, from entity collection
                $tabKey = $objEntityCollection->getTabKey();
                for($intCpt = 0; ($intCpt < count($tabKey)) && is_null($result); $intCpt++)
                {
                    // Init var
                    $strKey = $tabKey[$intCpt];
                    $objEntity = $objEntityCollection->getItem($strKey);

                    // Select and register entity, if required
                    $result = (
                        (
                            ($objEntity instanceof $strClassPath) &&
                            $objEntity->checkAttributeExists($strAttrKeyId) &&
                            ($objEntity->getAttributeValue($strAttrKeyId) === $id)
                        ) ?
                            $objEntity :
                            $result
                    );
                }
            }
            // Select entity from selection configuration, if required
            else if(!is_null($tabGetConfig))
            {
                // Select entities
                $tabEntity = array_values($objEntityCollection->getTabItem($tabGetConfig));

                // Run each selected entity, from entity collection
                for($intCpt = 0; ($intCpt < count($tabEntity)) && is_null($result); $intCpt++)
                {
                    // Init var
                    $objEntity = $tabEntity[$intCpt];

                    // Register entity, if required
                    $result = (($objEntity instanceof $strClassPath) ? $objEntity : $result);
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance entity engine.
     * Overwrite it to set specific feature.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntityNew() configuration array format.
     *
     * @param array $tabConfig = null
     * @return EntityInterface
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Return result
        return parent::getObjEntityNew($tabConfig);
    }



    /**
     * Get new or existing object instance entity.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * [
     *     Fix entity factory configuration,
     *
     *     select_entity_id(optional: no entity selection done (based on id), if not found):
     *         mixed entity id to search,
     *
     *     select_entity_create_require(optional: got configuration select_entity_create_require, if not found):
     *         true / false,
     *         specify if entity creation required,
     *         if entity selection required and fails,
     *
     *     select_entity_collection_get_config(optional: no entity selection done (based on selection configuration), if not found):
     *         [
     *             @see EntityCollectionInterface::getTabItem() configuration array format,
     *             where first valid entity found is returned
     *         ],
     *
     *     select_entity_collection_set_require(optional: got configuration select_entity_collection_set_require, if not found):
     *         true / false,
     *         specify if entity setting required, on entity collection,
     *         if entity selection required and entity creation done
     * ]
     *
     * @param array $tabConfig = null
     * @return EntityInterface
     * @throws ExecConfigInvalidFormatException
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $strClassPath = $this->getStrEntityClassPath();
        $boolSelectRequire = false;

        // Get selected entity if required
        $result = $this->getObjSelectEntity($tabConfig, $boolSelectRequire);

        // Get new entity if required
        if(is_null($result))
        {
            // Check configuration
            if($boolSelectRequire && (!$this->checkSelectEntityCreateRequired($tabConfig)))
            {
                throw new ExecConfigInvalidFormatException(serialize($tabConfig));
            }

            // Get new entity
            $result = $this->getObjEntityNewEngine($tabConfig);

            // Register entity on entity collection, if required
            if(
                $boolSelectRequire &&
                ($result instanceof $strClassPath) &&
                ($result instanceof ItemEntityInterface) &&
                $this->checkSelectEntityCollectionSetRequired($tabConfig)
            )
            {
                $this
                    ->getObjEntityCollection()
                    ->setItem($result);
            }
        }

        // Return result
        return $result;
    }



}