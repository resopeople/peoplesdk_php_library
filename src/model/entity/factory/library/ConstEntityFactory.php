<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\factory\library;



class ConstEntityFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_ENTITY_COLLECTION = 'objEntityCollection';



    // Configuration
    const TAB_CONFIG_KEY_SELECT_ENTITY_REQUIRE = 'select_entity_require';
    const TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID = 'select_entity_attribute_key_id';
    const TAB_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE = 'select_entity_create_require';
    const TAB_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE = 'select_entity_collection_set_require';

    // Execution configuration
    const TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID = 'select_entity_id';
    const TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_CREATE_REQUIRE = 'select_entity_create_require';
    const TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_GET_CONFIG = 'select_entity_collection_get_config';
    const TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_COLLECTION_SET_REQUIRE = 'select_entity_collection_set_require';



    // Exception message constants
    const EXCEPT_MSG_ENTITY_COLLECTION_INVALID_FORMAT =
        'Following collection "%1$s" invalid! It must be a valid entity collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default entity factory configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default entity factory execution configuration standard.';



}