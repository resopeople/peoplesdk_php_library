<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\requisition\response\exception;

use Exception;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\api\ValidEntityCollectionInterface;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;



class ValidEntityCollectionFailException extends Exception
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var EntityCollectionInterface */
    protected $objEntityCollection;



    /**
     * Errors array format:
     * @see ValidEntityCollectionInterface::checkValid() errors array format.
     *
     * @var array
     */
    protected $tabError;





	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     *
	 * @param EntityCollectionInterface $objEntityCollection
     * @param array $tabError
     */
	public function __construct(
        EntityCollectionInterface $objEntityCollection,
        array $tabError
    )
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstResponseEntity::EXCEPT_MSG_VALID_ENTITY_COLLECTION_FAIL;
        $this->objEntityCollection = $objEntityCollection;
        $this->tabError = $tabError;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity collection,
     * associated to this exception.
     *
     * @return EntityCollectionInterface
     */
    public function getObjEntityCollection()
    {
        // Return result
        return $this->objEntityCollection;
    }



    /**
     * Get array of errors,
     * associated to this exception.
     *
     * Return format:
     * @see ValidEntityCollectionFailException::$tabError format.
     *
     * @return array
     */
    public function getTabError()
    {
        // Return result
        return $this->tabError;
    }



}