<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\requisition\response\exception;

use Exception;

use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\ValidEntityInterface;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;



class ValidEntityFailException extends Exception
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var EntityInterface */
    protected $objEntity;



    /**
     * Errors array format:
     * @see ValidEntityInterface::checkAttributeValid() errors array format, for all attributes.
     *
     * @var array
     */
    protected $tabError;





	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     *
     * @param EntityInterface $objEntity
     * @param array $tabError
     */
	public function __construct(
        EntityInterface $objEntity,
        array $tabError
    )
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstResponseEntity::EXCEPT_MSG_VALID_ENTITY_FAIL;
        $this->objEntity = $objEntity;
        $this->tabError = $tabError;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity,
     * associated to this exception.
     *
     * @return EntityInterface
     */
    public function getObjEntity()
    {
        // Return result
        return $this->objEntity;
    }



    /**
     * Get array of errors,
     * associated to this exception.
     *
     * Return format:
     * @see ValidEntityFailException::$tabError format.
     *
     * @return array
     */
    public function getTabError()
    {
        // Return result
        return $this->tabError;
    }



}