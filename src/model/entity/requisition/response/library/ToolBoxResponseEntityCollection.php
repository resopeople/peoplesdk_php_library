<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\requisition\response\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;



class ToolBoxResponseEntityCollection extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get entity count,
     * from specified response entity collection data.
     *
     * @param array $tabCollectionData
     * @return null|integer
     */
    public static function getIntEntityCountFromData(array $tabCollectionData)
    {
        // Init var
        $result = (
            (
                isset($tabCollectionData[ConstResponseEntity::COLLECTION_KEY_COUNT]) &&
                is_numeric($tabCollectionData[ConstResponseEntity::COLLECTION_KEY_COUNT]) &&
                (intval($tabCollectionData[ConstResponseEntity::COLLECTION_KEY_COUNT]) >= 0)
            ) ?
                intval($tabCollectionData[ConstResponseEntity::COLLECTION_KEY_COUNT]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of entity data,
     * from specified response entity collection data.
     *
     * @param array $tabCollectionData
     * @return array
     */
    public static function getTabEntityDataFromData(array $tabCollectionData)
    {
        // Init var
        $result = (
            (
                isset($tabCollectionData[ConstResponseEntity::COLLECTION_KEY_DATA]) &&
                is_array($tabCollectionData[ConstResponseEntity::COLLECTION_KEY_DATA])
            ) ?
                $tabCollectionData[ConstResponseEntity::COLLECTION_KEY_DATA] :
                array()
        );
        $result = array_values(array_filter(
            $result,
            function($data) {return is_array($data);}
        ));

        // Return result
        return $result;
    }



}