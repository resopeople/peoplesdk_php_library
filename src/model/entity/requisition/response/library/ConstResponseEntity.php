<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\requisition\response\library;



class ConstResponseEntity
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Collection configuration
    const COLLECTION_KEY_COUNT = 'count';
    const COLLECTION_KEY_DATA = 'data';



    // Error configuration
    const ERROR_KEY_DATA = 'data';

    // Error data configuration
    const ERROR_DATA_INDEX_REGEXP_SELECT = '/#(\d+)/';
    const ERROR_DATA_KEY_ERROR = 'error';



    // Exception message constants
    const EXCEPT_MSG_VALID_ENTITY_FAIL = 'Entity fails validation.';
    const EXCEPT_MSG_VALID_ENTITY_COLLECTION_FAIL = 'Entity collection fails validation.';



}