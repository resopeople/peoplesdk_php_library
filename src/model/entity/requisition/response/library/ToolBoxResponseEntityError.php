<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\requisition\response\library;

use liberty_code\library\instance\model\Multiton;

use Exception;
use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\ValidEntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\api\ValidEntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use people_sdk\library\requisition\response\library\ToolBoxResponse;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;
use people_sdk\library\model\entity\requisition\response\exception\ValidEntityFailException;
use people_sdk\library\model\entity\requisition\response\exception\ValidEntityCollectionFailException;



class ToolBoxResponseEntityError extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();

	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get error data array,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|array
     */
    public static function getTabErrorData(DataHttpResponse $objResponse)
    {
        // Init var
        $tabData = ToolBoxResponse::getTabErrorData($objResponse);
        $result = (
            (
                isset($tabData[ConstResponseEntity::ERROR_KEY_DATA]) &&
                is_array($tabData[ConstResponseEntity::ERROR_KEY_DATA])
            ) ?
                $tabData[ConstResponseEntity::ERROR_KEY_DATA] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity index.
     *
     * @param string $strIndex
     * @return null|integer
     */
    protected static function getIntEntityIndex($strIndex)
    {
        // Init var
        $result = (
            (
                is_string($strIndex) &&
                (preg_match(
                    ConstResponseEntity::ERROR_DATA_INDEX_REGEXP_SELECT,
                        $strIndex,
                    $tabMatch
                ) !== false) &&
                isset($tabMatch[1])
            ) ?
                intval($tabMatch[1]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity error array engine,
     * from specified response error data.
     *
     * Error data array format:
     * [
     *     "string failed attribute save name 1": [
     *         error(required): [
     *             "string error key 1":
     *                 "string error message"
     *                 OR
     *                 [
     *                     @see getTabEntityErrorEngine() error data array format
     *                     OR
     *                     @see getTabEntityCollectionError() error data array format
     *                 ],
     *
     *             ...,
     *
     *             "string error key N": ...
     *         ],
     *
     *         ...
     *     ],
     *
     *     ...,
     *
     *     "string failed attribute save name N": [...]
     * ]
     *
     * Get new entity exception callable format:
     * mixed|Exception function(EntityInterface $objEntity, array $tabError, string $strParentAttrKey, string $strParentErrorKey):
     * Get new exception, from specified error array and specified entity.
     *
     * Get new entity collection exception callable format:
     * mixed|Exception function(EntityCollectionInterface $objEntityCollection, array $tabError, string $strParentAttrKey, string $strParentErrorKey):
     * Get new exception, from specified error array and specified entity collection.
     *
     * Return format:
     * @see ValidEntityInterface::checkAttributeValid() errors array format, for all attributes.
     *
     * @param EntityInterface $objEntity
     * @param array $tabErrorData
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return null|array
     */
    protected static function getTabEntityErrorFromDataEngine(
        EntityInterface $objEntity,
        array $tabErrorData,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init var
        $callableGetEntityExceptionNew = (
            (!is_null($callableGetEntityExceptionNew)) ?
                $callableGetEntityExceptionNew :
                function(EntityInterface $objEntity, array $tabError) {
                    return new ValidEntityFailException($objEntity, $tabError);
                }
        );
        $callableGetEntityCollectionExceptionNew = (
            (!is_null($callableGetEntityCollectionExceptionNew)) ?
                $callableGetEntityCollectionExceptionNew :
                function(EntityCollectionInterface $objEntityCollection, array $tabError) {
                    return new ValidEntityCollectionFailException($objEntityCollection, $tabError);
                }
        );
        $result = null;

        // Init array of attribute keys
        $tabAttrKey = $objEntity->getTabAttributeKey();
        $tabAttrKey = array_combine(
            array_map(
                function($strKey) use ($objEntity) {
                    return (
                        ($objEntity instanceof SaveEntityInterface) ?
                            $objEntity->getAttributeNameSave($strKey) :
                            ($objEntity->getAttributeAlias($strKey) ?? $strKey)
                    );
                },
                $tabAttrKey
            ),
            $tabAttrKey
        );

        // Run each attribute, from response error data
        foreach($tabErrorData as $strAttrSaveNm => $tabAttrErrorData)
        {
            // Get attribute info
            $strAttrKey = (
                (
                    is_string($strAttrSaveNm) &&
                    isset($tabAttrKey[$strAttrSaveNm])
                ) ?
                    $tabAttrKey[$strAttrSaveNm] :
                    null
            );
            $attrValue = (
                (!is_null($strAttrKey)) ?
                    $objEntity->getAttributeValue($strAttrKey) :
                    null
            );
            $tabAttrError = (
                (
                    isset($tabAttrErrorData[ConstResponseEntity::ERROR_DATA_KEY_ERROR]) &&
                    is_array($tabAttrErrorData[ConstResponseEntity::ERROR_DATA_KEY_ERROR])
                ) ?
                    $tabAttrErrorData[ConstResponseEntity::ERROR_DATA_KEY_ERROR] :
                    null
            );

            if((!is_null($strAttrKey)) && (!is_null($tabAttrError)))
            {
                // Run each attribute error
                foreach($tabAttrError as $strErrorKey => $error)
                {
                    // Format error, if required
                    if(is_object($attrValue) && is_array($error))
                    {
                        // Get entity exception, if required
                        if($attrValue instanceof EntityInterface)
                        {
                            $tabSubError = static::getTabEntityErrorFromDataEngine(
                                $attrValue,
                                $error,
                                $callableGetEntityExceptionNew,
                                $callableGetEntityCollectionExceptionNew
                            );
                            $error = (
                                (!is_null($tabSubError)) ?
                                    $callableGetEntityExceptionNew(
                                        $attrValue,
                                        $tabSubError,
                                        $strAttrKey,
                                        $strErrorKey
                                    ) :
                                    null
                            );
                        }
                        // Get entity collection exception, if required
                        else if($attrValue instanceof EntityCollectionInterface)
                        {
                            $tabSubError = static::getTabEntityCollectionErrorFromData(
                                $attrValue,
                                $error,
                                $callableGetEntityExceptionNew,
                                $callableGetEntityCollectionExceptionNew
                            );
                            $error = (
                                (!is_null($tabSubError)) ?
                                    $callableGetEntityCollectionExceptionNew(
                                        $attrValue,
                                        $tabSubError,
                                        $strAttrKey,
                                        $strErrorKey
                                    ) :
                                    null
                            );
                        }
                    }

                    // Register error, if required
                    if(is_string($error) || ($error instanceof Exception))
                    {
                        $result = ((!is_null($result)) ? $result : array());
                        $result = (isset($result[$strAttrKey]) ? $result : $result + array($strAttrKey => array()));
                        $result[$strAttrKey][$strErrorKey] = $error;
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get entity error array,
     * from specified response error data.
     *
     * Error data array format:
     * [
     *     @see getTabEntityErrorFromDataEngine() error data array format
     *
     *     OR
     *
     *     "string failed entity index 1 (must be: '#0')": [
     *         @see getTabEntityErrorFromDataEngine() error data array format
     *     ]
     * ]
     *
     * Get new entity exception callable format:
     * @see getTabEntityErrorFromDataEngine() get new entity exception callable format.
     *
     * Get new entity collection exception callable format:
     * @see getTabEntityErrorFromDataEngine() get new entity collection exception callable format.
     *
     * Return format:
     * @see getTabEntityErrorFromDataEngine() return format.
     *
     * @param EntityInterface $objEntity
     * @param array $tabErrorData
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return null|array
     */
    public static function getTabEntityErrorFromData(
        EntityInterface $objEntity,
        array $tabErrorData,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init error data array, if required (if error data contains only one entity error, get it)
        $tabKey = array_keys($tabErrorData);
        $tabErrorData = (
            (
                (count($tabKey) == 1) &&
                (!is_null($intEntityIndex = static::getIntEntityIndex($tabKey[0]))) &&
                ($intEntityIndex == 0)
            ) ?
                $tabErrorData[$tabKey[0]] :
                $tabErrorData
        );

        // Init var
        $result = static::getTabEntityErrorFromDataEngine(
            $objEntity,
            $tabErrorData,
            $callableGetEntityExceptionNew,
            $callableGetEntityCollectionExceptionNew
        );

        // Return result
        return $result;
    }



    /**
     * Get entity error array,
     * from specified response.
     *
     * Get new entity exception callable format:
     * @see getTabEntityErrorFromData() get new entity exception callable format.
     *
     * Get new entity collection exception callable format:
     * @see getTabEntityErrorFromData() get new entity collection exception callable format.
     *
     * Return format:
     * @see getTabEntityErrorFromData() return format.
     *
     * @param EntityInterface $objEntity
     * @param DataHttpResponse $objResponse
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return null|array
     */
    public static function getTabEntityError(
        EntityInterface $objEntity,
        DataHttpResponse $objResponse,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init var
        $tabErrorData = static::getTabErrorData($objResponse);
        $result = (
            (!is_null($tabErrorData)) ?
                static::getTabEntityErrorFromData(
                    $objEntity,
                    $tabErrorData,
                    $callableGetEntityExceptionNew,
                    $callableGetEntityCollectionExceptionNew
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity collection error array,
     * from specified response error data.
     *
     * Error data array format:
     * [
     *     "string failed entity index 1 (like '#\d', ex: '#0', ...'#N')": [
     *         @see getTabEntityErrorFromDataEngine() error data array format
     *     ],
     *
     *     ...,
     *
     *     "string failed entity index N": [...]
     * ]
     *
     * Get new entity exception callable format:
     * @see getTabEntityErrorFromDataEngine() get new entity exception callable format.
     *
     * Get new entity collection exception callable format:
     * @see getTabEntityErrorFromDataEngine() get new entity collection exception callable format.
     *
     * Return format:
     * @see ValidEntityCollectionInterface::checkValid() errors array format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param array $tabErrorData
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return null|array
     */
    public static function getTabEntityCollectionErrorFromData(
        EntityCollectionInterface $objEntityCollection,
        array $tabErrorData,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init var
        $tabEntityKey = $objEntityCollection->getTabKey();
        $result = null;

        // Run each entity, from response error data
        foreach($tabErrorData as $strEntityIndex => $tabEntityErrorData)
        {
            // Get entity, if required
            $intEntityIndex = static::getIntEntityIndex($strEntityIndex);
            $strEntityKey = (
                (
                    (!is_null($intEntityIndex)) &&
                    array_key_exists($intEntityIndex, $tabEntityKey)
                ) ?
                    $tabEntityKey[$intEntityIndex] :
                    null
            );
            $objEntity = (
                (!is_null($strEntityKey)) ?
                    $objEntityCollection->getItem($strEntityKey) :
                    null
            );

            // Get entity error, if required
            $tabEntityError = (
                (
                    (!is_null($objEntity)) &&
                    is_array($tabEntityErrorData)
                ) ?
                    static::getTabEntityErrorFromDataEngine(
                        $objEntity,
                        $tabEntityErrorData,
                        $callableGetEntityExceptionNew,
                        $callableGetEntityCollectionExceptionNew
                    ) :
                    null
            );

            // Register entity error, if required
            if(!is_null($tabEntityError))
            {
                $result = ((!is_null($result)) ? $result : array());
                $result[$strEntityKey] = $tabEntityError;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get entity collection error array,
     * from specified response.
     *
     * Error data array format:
     * @see getTabEntityCollectionErrorFromData() error data array format.
     *
     * Get new entity exception callable format:
     * @see getTabEntityCollectionErrorFromData() get new entity exception callable format.
     *
     * Get new entity collection exception callable format:
     * @see getTabEntityCollectionErrorFromData() get new entity collection exception callable format.
     *
     * Return format:
     * @see getTabEntityCollectionErrorFromData return format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param DataHttpResponse $objResponse
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return null|array
     */
    public static function getTabEntityCollectionError(
        EntityCollectionInterface $objEntityCollection,
        DataHttpResponse $objResponse,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init var
        $tabErrorData = static::getTabErrorData($objResponse);
        $result = (
            (!is_null($tabErrorData)) ?
                static::getTabEntityCollectionErrorFromData(
                    $objEntityCollection,
                    $tabErrorData,
                    $callableGetEntityExceptionNew,
                    $callableGetEntityCollectionExceptionNew
                ) :
                null
        );

        // Return result
        return $result;
    }



}