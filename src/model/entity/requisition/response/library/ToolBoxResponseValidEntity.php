<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\requisition\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\ValidEntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\api\ValidEntityCollectionInterface;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use people_sdk\library\model\entity\requisition\response\library\ToolBoxResponseEntityError;



class ToolBoxResponseValidEntity extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified entity is valid,
     * from specified response.
     * Array of errors can be returned.
     * Attribute key(s) selection can be provided.
     *
     * Errors array format:
     * @see ValidEntityInterface::checkAttributeValid() errors array format, for all attributes.
     *
     * Attribute key format:
     * - String attribute key, to select.
     * - Index array of attribute keys, to select.
     *
     * Get new entity exception callable format:
     * @see ToolBoxResponseEntityError::getTabEntityError() get new entity exception callable format.
     *
     * Get new entity collection exception callable format:
     * @see ToolBoxResponseEntityError::getTabEntityError() get new entity collection exception callable format.
     *
     * @param EntityInterface $objEntity
     * @param DataHttpResponse $objResponse
     * @param array &$tabError = array()
     * @param null|string|string[] $attrKey = null
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return boolean
     */
    public static function checkEntityIsValid(
        EntityInterface $objEntity,
        DataHttpResponse $objResponse,
        array &$tabError = array(),
        $attrKey = null,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init var
        $tabError = ToolBoxResponseEntityError::getTabEntityError(
            $objEntity,
            $objResponse,
            $callableGetEntityExceptionNew,
            $callableGetEntityCollectionExceptionNew
        );
        $tabError = ((!is_null($tabError)) ? $tabError : array());

        // Filter errors, if required
        $tabAttrKey = (
            is_string($attrKey) ?
                array($attrKey) :
                (
                    is_array($attrKey) ?
                        array_filter(
                            array_values($attrKey),
                            function($strAttrKey) {return is_string($strAttrKey);}
                        ) :
                        array()
                )
        );
        $tabError = (
            (count($tabAttrKey) > 0) ?
                array_filter(
                    $tabError,
                    function($strAttrKey) use($tabAttrKey) {
                        return in_array($strAttrKey, $tabAttrKey);
                    },
                    ARRAY_FILTER_USE_KEY
                ) :
                $tabError
        );

        // Check entity is valid (No error found)
        $result = (count($tabError) === 0);

        // Return result
        return $result;
    }



    /**
     * Check if specified entity collection is valid,
     * from specified response.
     * Array of errors can be provided.
     * Selection configuration can be provided.
     *
     * Errors array format:
     * @see ValidEntityCollectionInterface::checkValid() errors array format.
     *
     * Configuration array format:
     * @see EntityCollectionInterface::getTabKey() configuration format.
     *
     * Get new entity exception callable format:
     * @see ToolBoxResponseEntityError::getTabEntityCollectionError() get new entity exception callable format.
     *
     * Get new entity collection exception callable format:
     * @see ToolBoxResponseEntityError::getTabEntityCollectionError() get new entity collection exception callable format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param DataHttpResponse $objResponse
     * @param array &$tabError = array()
     * @param null|array $tabConfig = null
     * @param null|callable $callableGetEntityExceptionNew = null
     * @param null|callable $callableGetEntityCollectionExceptionNew = null
     * @return boolean
     */
    public static function checkEntityCollectionIsValid(
        EntityCollectionInterface $objEntityCollection,
        DataHttpResponse $objResponse,
        array &$tabError = array(),
        array $tabConfig = null,
        callable $callableGetEntityExceptionNew = null,
        callable $callableGetEntityCollectionExceptionNew = null
    )
    {
        // Init var
        $tabError = ToolBoxResponseEntityError::getTabEntityCollectionError(
            $objEntityCollection,
            $objResponse,
            $callableGetEntityExceptionNew,
            $callableGetEntityCollectionExceptionNew
        );
        $tabError = ((!is_null($tabError)) ? $tabError : array());

        // Filter errors
        $tabEntityKey = $objEntityCollection->getTabKey($tabConfig);
        $tabError = array_filter(
            $tabError,
            function($strEntityKey) use($tabEntityKey) {
                return in_array($strEntityKey, $tabEntityKey);
            },
            ARRAY_FILTER_USE_KEY
        );

        // Check entity collection is valid (No error found)
        $result = (count($tabError) === 0);

        // Return result
        return $result;
    }



}