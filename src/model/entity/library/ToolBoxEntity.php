<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\model\entity\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\entity\factory\api\EntityFactoryInterface;
use liberty_code\model\repository\library\ToolBoxEntityRepository;



class ToolBoxEntity extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified entity collection,
     * from specified index array of saved data array.
     *
     * Data array format:
     * [
     *     // Data 1
     *     [@see SaveEntityInterface::hydrateSave() data array format],
     *     ...,
     *     // Data N
     *     [...]
     * ]
     *
     * Get entity factory execution configuration callable format:
     * null|array function(array $tabData):
     * Get entity factory execution configuration,
     * from specified saved data array.
     *
     * Get formatted entity callable format:
     * mixed|SaveEntityInterface function(SaveEntityInterface $objEntity, array $tabData):
     * Get specified formatted entity,
     * from specified saved data array.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param EntityFactoryInterface $objEntityFactory
     * @param array $tabData
     * @param boolean $boolCheckExist = true
     * @param boolean $boolIsNew = false
     * @param null|array|callable $tabEntityFactoryExecConfig = null
     * @param null|callable $callableGetObjEntityFormat = null
     * @return boolean
     */
    public static function hydrateSaveEntityCollection(
        EntityCollectionInterface $objEntityCollection,
        EntityFactoryInterface $objEntityFactory,
        array $tabData,
        $boolCheckExist = true,
        $boolIsNew = false,
        $tabEntityFactoryExecConfig = null,
        callable $callableGetObjEntityFormat = null
    )
    {
        // Init var
        $boolCheckExist = (is_bool($boolCheckExist) ? $boolCheckExist : true);
        $boolIsNew = (is_bool($boolIsNew) ? $boolIsNew : false);
        $tabEntityFactoryExecConfig = (
            (is_callable($tabEntityFactoryExecConfig) || is_array($tabEntityFactoryExecConfig)) ?
                $tabEntityFactoryExecConfig :
                null
        );
        $result = ToolBoxEntityRepository::hydrateSaveEntityCollection(
            $objEntityCollection,
            function(array $tabData) use (
                $objEntityFactory,
                $boolCheckExist,
                $boolIsNew,
                $tabEntityFactoryExecConfig,
                $callableGetObjEntityFormat
            )
            {
                // Get new hydrated entity, if required
                $tabConfig = (
                    is_callable($tabEntityFactoryExecConfig) ?
                        $tabEntityFactoryExecConfig($tabData) :
                        (
                            is_array($tabEntityFactoryExecConfig) ?
                                $tabEntityFactoryExecConfig :
                                null
                        )
                );
                $result = $objEntityFactory->getObjEntity(array(), $tabConfig);
                $result = (
                    (
                        (!is_null($result)) &&
                        ($result instanceof SaveEntityInterface)
                    ) ?
                        $result:
                        null
                );
                $result->hydrateSave($tabData, $boolCheckExist);

                // Format entity, if required
                if(!is_null($result))
                {
                    $result->setIsNew($boolIsNew);
                    $result = (
                        (!is_null($callableGetObjEntityFormat)) ?
                            $callableGetObjEntityFormat($result, $tabData) :
                            $result
                    );
                }

                // Return result
                return $result;
            },
            $tabData
        );

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if save getting enabled,
     * for specified entity attribute.
     * To be savable, specified entity attribute must different,
     * from specified default value.
     *
     * @param SaveEntityInterface $objEntity
     * @param string $strKey
     * @param null|mixed $default = null
     * @return boolean
     */
    public static function checkEntityAttrSaveGetEnabled(
        SaveEntityInterface $objEntity,
        $strKey,
        $default = null
    )
    {
        return ($objEntity->getAttributeValue($strKey, false) !== $default);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of data array to save,
     * from specified entity collection.
     *
     * Get formatted entity data callable format:
     * mixed|array function(SaveEntityInterface $objEntity, array $tabData):
     * Get specified formatted entity data array to save.
     *
     * Return format:
     * [
     *     // Data 1
     *     [@see SaveEntityInterface::getTabDataSave() return format],
     *     ...,
     *     // Data N
     *     [...]
     * ]
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param null|callable $callableGetTabEntityDataFormat = null
     * @return array
     */
    public static function getTabEntityCollectionDataSave(
        EntityCollectionInterface $objEntityCollection,
        callable $callableGetTabEntityDataFormat = null
    )
    {
        // Init var
        $result = ToolBoxEntityRepository::getTabEntityCollectionDataSave(
            $objEntityCollection,
            function(SaveEntityInterface $objEntity) use ($callableGetTabEntityDataFormat)
            {
                // Get entity data to save
                $result = $objEntity->getTabDataSave();
                $result = (
                    (!is_null($callableGetTabEntityDataFormat)) ?
                        $callableGetTabEntityDataFormat($objEntity, $result) :
                        $result
                );

                // Return result
                return $result;
            }
        );

        // Return result
        return $result;
    }



}