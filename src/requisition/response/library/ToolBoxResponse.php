<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use people_sdk\library\requisition\response\library\ConstResponse;



class ToolBoxResponse extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified response is a valid result.
     *
     * @param DataHttpResponse $objResponse
     * @return boolean
     */
    public static function checkIsResult(DataHttpResponse $objResponse)
    {
        // Return result
        return (!is_null(static::getTabResultData($objResponse)));
    }



    /**
     * Check if specified response is a valid error.
     *
     * @param DataHttpResponse $objResponse
     * @return boolean
     */
    public static function checkIsError(DataHttpResponse $objResponse)
    {
        // Return result
        return (!is_null(static::getTabErrorData($objResponse)));
    }



    /**
     * Check if specified response has valid warnings.
     *
     * @param DataHttpResponse $objResponse
     * @return boolean
     */
    public static function checkHasWarning(DataHttpResponse $objResponse)
    {
        // Return result
        return (count(static::getTabWarnData($objResponse)) > 0);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get result data array,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|array
     */
    public static function getTabResultData(DataHttpResponse $objResponse)
    {
        // Init var
        $intStatusCode = $objResponse->getIntStatusCode();
        $tabData = $objResponse->getTabBodyData();
        $result = (
            (
                (!is_null($intStatusCode)) &&
                ($intStatusCode >= 200) &&
                ($intStatusCode < 300) &&
                isset($tabData[ConstResponse::BODY_KEY_RESULT]) &&
                is_array($tabData[ConstResponse::BODY_KEY_RESULT])
            ) ?
                $tabData[ConstResponse::BODY_KEY_RESULT] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get error data array,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|array
     */
    public static function getTabErrorData(DataHttpResponse $objResponse)
    {
        // Init var
        $intStatusCode = $objResponse->getIntStatusCode();
        $tabData = $objResponse->getTabBodyData();
        $result = (
            (
                (!is_null($intStatusCode)) &&
                ($intStatusCode >= 400) &&
                ($intStatusCode < 500) &&
                isset($tabData[ConstResponse::BODY_KEY_ERROR]) &&
                is_array($tabData[ConstResponse::BODY_KEY_ERROR])
            ) ?
                $tabData[ConstResponse::BODY_KEY_ERROR] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get error code,
     * from specified response error data.
     *
     * @param array $tabErrorData
     * @return null|integer
     */
    public static function getIntErrorCodeFromData(array $tabErrorData)
    {
        // Init var
        $result = (
            (
                isset($tabErrorData[ConstResponse::ERROR_KEY_CODE]) &&
                is_numeric($tabErrorData[ConstResponse::ERROR_KEY_CODE])
            ) ?
                intval($tabErrorData[ConstResponse::ERROR_KEY_CODE]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get error code,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|integer
     */
    public static function getIntErrorCode(DataHttpResponse $objResponse)
    {
        // Init var
        $tabErrorData = static::getTabErrorData($objResponse);
        $result = (
            (!is_null($tabErrorData)) ?
                static::getIntErrorCodeFromData($tabErrorData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get error message,
     * from specified response error data.
     *
     * @param array $tabErrorData
     * @return null|string
     */
    public static function getStrErrorMsgFromData(array $tabErrorData)
    {
        // Init var
        $result = (
            (
                isset($tabErrorData[ConstResponse::ERROR_KEY_MSG]) &&
                ToolBoxString::checkConvertString($tabErrorData[ConstResponse::ERROR_KEY_MSG])
            ) ?
                strval($tabErrorData[ConstResponse::ERROR_KEY_MSG]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get error message,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return null|string
     */
    public static function getStrErrorMsg(DataHttpResponse $objResponse)
    {
        // Init var
        $tabErrorData = static::getTabErrorData($objResponse);
        $result = (
            (!is_null($tabErrorData)) ?
                static::getStrErrorMsgFromData($tabErrorData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of warning data,
     * from specified response.
     *
     * @param DataHttpResponse $objResponse
     * @return array
     */
    public static function getTabWarnData(DataHttpResponse $objResponse)
    {
        // Init var
        $tabData = $objResponse->getTabBodyData();
        $result = (
            (
                isset($tabData[ConstResponse::BODY_KEY_WARNING]) &&
                is_array($tabData[ConstResponse::BODY_KEY_WARNING])
            ) ?
                $tabData[ConstResponse::BODY_KEY_WARNING] :
                array()
        );
        $result = array_values(array_filter(
            $result,
            function($data) {return is_array($data);}
        ));

        // Return result
        return $result;
    }



    /**
     * Get specified warning code,
     * from specified response warning data.
     *
     * @param array $tabWarnData
     * @return null|integer
     */
    public static function getIntWarnCodeFromData(array $tabWarnData)
    {
        // Init var
        $result = (
            (
                isset($tabWarnData[ConstResponse::WARNING_KEY_CODE]) &&
                is_numeric($tabWarnData[ConstResponse::WARNING_KEY_CODE])
            ) ?
                intval($tabWarnData[ConstResponse::WARNING_KEY_CODE]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified warning message,
     * from specified response warning data.
     *
     * @param array $tabWarnData
     * @return null|string
     */
    public static function getStrWarnMsgFromData(array $tabWarnData)
    {
        // Init var
        $result = (
            (
                isset($tabWarnData[ConstResponse::WARNING_KEY_MSG]) &&
                ToolBoxString::checkConvertString($tabWarnData[ConstResponse::WARNING_KEY_MSG])
            ) ?
                strval($tabWarnData[ConstResponse::WARNING_KEY_MSG]) :
                null
        );

        // Return result
        return $result;
    }



}