<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\response\library;



class ConstResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Body configuration
    const BODY_KEY_RESULT = 'result';
    const BODY_KEY_ERROR = 'error';
    const BODY_KEY_WARNING = 'warning';

    // Error configuration
    const ERROR_KEY_CODE = 'code';
    const ERROR_KEY_MSG = 'message';

    // Warning configuration
    const WARNING_KEY_CODE = 'code';
    const WARNING_KEY_MSG = 'message';



}