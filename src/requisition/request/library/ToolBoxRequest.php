<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\library\ToolBoxHttpRequest;
use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;



class ToolBoxRequest extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string URL,
     * from specified sending information factory,
     * and specified route.
     *
     * Route argument array format:
     * @see ToolBoxHttpRequest::getStrUrl() route argument array format.
     *
     * Route argument specification array format:
     * @see ToolBoxHttpRequest::getStrUrl() route argument specification array format.
     *
     * Argument format:
     * @see ToolBoxHttpRequest::getStrUrl() argument format.
     *
     * @param SndInfoFactoryInterface $objSndInfoFactory
     * @param string $strRoute
     * @param null|array $tabRouteArg = null
     * @param null|array $tabRouteArgSpec = null
     * @param null|string|array $arg = null
     * @return null|string
     */
    public static function getStrUrl(
        SndInfoFactoryInterface $objSndInfoFactory,
        $strRoute,
        array $tabRouteArg = null,
        array $tabRouteArgSpec = null,
        $arg = null
    )
    {
        // Init var
        $tabSndInfo = (
            is_string($strRoute) ?
                ToolBoxSndInfo::getTabMergeSndInfo(
                    array(ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => $strRoute),
                    $objSndInfoFactory->getTabSndInfo()
                ) :
                null
        );
        $strSchema = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA] :
                null
        );
        $strHost = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST] :
                null
        );
        $intPort = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT] :
                null
        );
        $strRoute = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE] :
                null
        );
        $result = (
            (
                (!is_null($strHost)) &&
                (!is_null($strRoute))
            ) ?
            ToolBoxHttpRequest::getStrUrl(
                $strHost,
                $strSchema,
                $intPort,
                $strRoute,
                $tabRouteArg,
                $tabRouteArgSpec,
                $arg
            ) :
             null
        );

        // Return result
        return $result;
    }



}