<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\library;



class ConstSndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // URL route configuration
    const URL_ROUTE_PATTERN = '/api%s';



    // Headers configuration
    const HEADER_KEY_LOC_GET_TIMEZONE_NAME = 'Dt-Config-Get-Tz-Name';
    const HEADER_KEY_LOC_GET_DATETIME_FORMAT = 'Dt-Config-Get-Dt-Format';
    const HEADER_KEY_LOC_SET_TIMEZONE_NAME = 'Dt-Config-Set-Tz-Name';
    const HEADER_KEY_LOC_SET_DATETIME_FORMAT = 'Dt-Config-Set-Dt-Format';
    const HEADER_KEY_SPACE_CONNECT_NAME = 'Space-Connection-Name';
    const HEADER_KEY_SPACE_CONNECT_KEY = 'Space-Connection-Key';
    const HEADER_KEY_CONTENT_TYPE = 'Content-Type';
    const HEADER_KEY_CONTENT_SRC_PATH = 'Content-Source-Path';
    const HEADER_KEY_CONTENT_SRC_PATH_SEPARATOR = 'Content-Source-Path-Separator';
    const HEADER_KEY_AUTH_TYPE = 'Authentication-Type';
    const HEADER_KEY_AUTH_AUTHORIZATION = 'Authorization';
    const HEADER_KEY_AUTH_API_KEY = 'Api-Key';



    // URL arguments configuration
    const URL_ARG_KEY_LOC_GET_TIMEZONE_NAME = 'dt-config-get-tz-name';
    const URL_ARG_KEY_LOC_GET_DATETIME_FORMAT = 'dt-config-get-dt-format';
    const URL_ARG_KEY_LOC_SET_TIMEZONE_NAME = 'dt-config-set-tz-name';
    const URL_ARG_KEY_LOC_SET_DATETIME_FORMAT = 'dt-config-set-dt-format';
    const URL_ARG_KEY_SPACE_CONNECT_NAME = 'space-connection-name';
    const URL_ARG_KEY_SPACE_CONNECT_KEY = 'space-connection-key';
    const URL_ARG_KEY_CONTENT_TYPE = 'content-type';
    const URL_ARG_KEY_CONTENT_SRC_PATH = 'content-source-path';
    const URL_ARG_KEY_CONTENT_SRC_PATH_SEPARATOR = 'content-source-path-separator';
    const URL_ARG_KEY_AUTH_TYPE = 'authentication-type';
    const URL_ARG_KEY_AUTH_AUTHORIZATION = 'authorization';
    const URL_ARG_KEY_AUTH_API_KEY = 'api-key';



    // Content type configuration
    const CONTENT_TYPE_JSON = 'application/json';
    const CONTENT_TYPE_XML = 'application/xml';
    const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';
    const CONTENT_TYPE_MULTIPART_FORM_PATTERN = 'multipart/form-data; boundary=%1$s';
    const CONTENT_TYPE_MULTIPART_FORM_REGEXP = '#^multipart/form-data; boundary=\w+$#';



    // Authentication type configuration
    const AUTH_TYPE_USER_BASIC = 'user_basic';
    const AUTH_TYPE_USER_API_KEY = 'user_api_key';
    const AUTH_TYPE_USER_BEARER = 'user_bearer';
    const AUTH_TYPE_APP_BASIC = 'app_basic';
    const AUTH_TYPE_APP_API_KEY = 'app_api_key';
    const AUTH_TYPE_APP_BEARER = 'app_bearer';
    const AUTH_TYPE_SUPER_ADMIN_BASIC = 'super_admin_basic';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get array of content types.
     *
     * @return array
     */
    public static function getTabContentType()
    {
        // Init var
        $result = array(
            self::CONTENT_TYPE_JSON,
            self::CONTENT_TYPE_XML,
            self::CONTENT_TYPE_FORM
        );

        // Return result
        return $result;
    }



    /**
     * Get array of content type REGEXPs.
     *
     * @return array
     */
    public static function getTabContentTypeRegexp()
    {
        // Init var
        $result = array(
            self::CONTENT_TYPE_MULTIPART_FORM_REGEXP
        );

        // Return result
        return $result;
    }



    /**
     * Get array of authentication types.
     *
     * @return array
     */
    public static function getTabAuthType()
    {
        // Init var
        $result = array(
            self::AUTH_TYPE_USER_BASIC,
            self::AUTH_TYPE_USER_API_KEY,
            self::AUTH_TYPE_USER_BEARER,
            self::AUTH_TYPE_APP_BASIC,
            self::AUTH_TYPE_APP_API_KEY,
            self::AUTH_TYPE_APP_BEARER,
            self::AUTH_TYPE_SUPER_ADMIN_BASIC
        );

        // Return result
        return $result;
    }



}