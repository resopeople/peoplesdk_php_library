<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable as BaseToolBoxTable;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\model\HttpRequest;
use people_sdk\library\table\library\ToolBoxTable;
use people_sdk\library\requisition\request\authentication\library\ToolBoxBasicAuthentication;
use people_sdk\library\requisition\request\authentication\library\ToolBoxApiKeyAuthentication;
use people_sdk\library\requisition\request\authentication\library\ToolBoxBearerAuthentication;
use people_sdk\library\requisition\request\info\library\ConstSndInfo;



class ToolBoxSndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified merged sending information array.
     *
     * Sending information array format:
     * Additional sending information array format:
     * Return format:
     * @see HttpRequest sending information array format.
     *
     * @param array $tabInfo
     * @param array $tabAddInfo
     * @return array
     */
    public static function getTabMergeSndInfo(array $tabInfo, array $tabAddInfo)
    {
        // Init var
        $result = $tabInfo;

        // Format additional sending info: resolve sprintf patterns, if required
        $tabKey = array(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST,
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE
        );
        foreach($tabKey as $strKey)
        {
            if(
                array_key_exists($strKey, $tabAddInfo) &&
                is_string($tabAddInfo[$strKey]) &&
                (
                    (substr_count($tabAddInfo[$strKey], '%s') === 1) ||
                    (substr_count($tabAddInfo[$strKey], '%1$s') === 1)
                ) &&
                array_key_exists($strKey, $result) &&
                is_string($result[$strKey])
            ) {
                $tabAddInfo[$strKey] = sprintf($tabAddInfo[$strKey], $result[$strKey]);
            }
        }

        // Merge additional sending info
        $result = BaseToolBoxTable::getTabMerge($result, $tabAddInfo);

        // Return result
        return $result;
    }



    /**
     * Get content type multipart form,
     * from specified boundary.
     *
     * @param string $strBoundary
     * @return null|string
     */
    public static function getStrContentTypeMultipartForm($strBoundary)
    {
        // Init var
        $result = (
            (
                is_string($strBoundary) &&
                (preg_match(
                    ConstSndInfo::CONTENT_TYPE_MULTIPART_FORM_REGEXP,
                    $contentType = sprintf(ConstSndInfo::CONTENT_TYPE_MULTIPART_FORM_PATTERN, $strBoundary)
                ) == 1)
            ) ?
                $contentType :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified header.
     *
     * Sending information array format:
     * Null or @see HttpRequest sending information array format.
     *
     * Return format:
     * Null or @see HttpRequest sending information array format.
     *
     * @param string $strKey
     * @param mixed $value
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithHeader($strKey, $value, array $tabInfo = null)
    {
        // Init var
        $tabSrcInfo = $tabInfo;
        $tabInfo = (
            is_string($strKey) ?
                array(
                    ConstHttpRequest::TAB_SND_INFO_KEY_HEADER => [
                        $strKey => $value
                    ]
                ) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabInfo, $tabSrcInfo);

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified URL argument.
     *
     * Sending information array format:
     * @see getTabSndInfoWithHeader() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithHeader() return format.
     *
     * @param string $strKey
     * @param mixed $value
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithUrlArg($strKey, $value, array $tabInfo = null)
    {
        // Init var
        $tabSrcInfo = $tabInfo;
        $tabInfo = (
            is_string($strKey) ?
                array(
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG => [
                        $strKey => $value
                    ]
                ) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabInfo, $tabSrcInfo);

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified parameter.
     * Set parameter on headers, if header key provided,
     * Set parameter on URL arguments, if URL argument key provided,
     * nothing to do, else.
     *
     * Sending information array format:
     * @see getTabSndInfoWithHeader() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithHeader() return format.
     *
     * @param mixed $value
     * @param null|string $strHeaderKey = null
     * @param null|string $strUrlArgKey = null
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithParam(
        $value,
        $strHeaderKey = null,
        $strUrlArgKey = null,
        array $tabInfo = null
    )
    {
        // Init var
        $strHeaderKey = (is_string($strHeaderKey) ? $strHeaderKey : null);
        $strUrlArgKey = (is_string($strUrlArgKey) ? $strUrlArgKey : null);
        $result = (
            (!is_null($strHeaderKey)) ?
                // Set parameter, on headers, if required
                static::getTabSndInfoWithHeader($strHeaderKey, $value, $tabInfo) :
                (
                    (!is_null($strUrlArgKey)) ?
                        // Set parameter, on URL arguments, if required
                        static::getTabSndInfoWithUrlArg($strUrlArgKey, $value, $tabInfo) :
                        ToolBoxTable::getTabItemFromSrc(null, $tabInfo)
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified user basic authentication,
     * from specified user profile login and password,
     * to connect to specific user profile, using HTTP basic format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strLogin
     * @param null|string $strPw = null
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthUserBasic(
        $strLogin,
        $strPw = null,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set basic authentication, if required
        $strBasic = ToolBoxBasicAuthentication::getStrBasic($strLogin, $strPw);
        if(!is_null($strBasic))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_USER_BASIC,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strBasic,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_AUTHORIZATION : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_AUTHORIZATION : null),
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified user API key authentication,
     * from specified user profile API key,
     * to connect to specific user profile, using HTTP API key format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strKey
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthUserApiKey(
        $strKey,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set API key authentication, if required
        $strApiKey = ToolBoxApiKeyAuthentication::getStrApiKey($strKey);
        if(is_string($strKey))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_USER_API_KEY,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strApiKey,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_API_KEY : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_API_KEY : null),
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified user bearer authentication,
     * from specified user profile token,
     * to connect to specific user profile, using HTTP bearer format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strToken
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthUserBearer(
        $strToken,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set bearer authentication, if required
        $strBearer = ToolBoxBearerAuthentication::getStrBearer($strToken);
        if(!is_null($strBearer))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_USER_BEARER,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strBearer,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_AUTHORIZATION : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_AUTHORIZATION : null),
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified application basic authentication,
     * from specified application profile name and secret,
     * to connect to specific application profile, using HTTP basic format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strName
     * @param null|string $strSecret = null
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthAppBasic(
        $strName,
        $strSecret = null,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set basic authentication, if required
        $strBasic = ToolBoxBasicAuthentication::getStrBasic($strName, $strSecret);
        if(!is_null($strBasic))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_APP_BASIC,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strBasic,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_AUTHORIZATION : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_AUTHORIZATION : null),
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified application API key authentication,
     * from specified application profile API key,
     * to connect to specific application profile, using HTTP API key format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strKey
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthAppApiKey(
        $strKey,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set API key authentication, if required
        $strApiKey = ToolBoxApiKeyAuthentication::getStrApiKey($strKey);
        if(is_string($strKey))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_APP_API_KEY,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strApiKey,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_API_KEY : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_API_KEY : null),
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified application bearer authentication,
     * from specified application profile token,
     * to connect to specific application profile, using HTTP bearer format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strToken
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthAppBearer(
        $strToken,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set bearer authentication, if required
        $strBearer = ToolBoxBearerAuthentication::getStrBearer($strToken);
        if(!is_null($strBearer))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_APP_BEARER,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strBearer,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_AUTHORIZATION : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_AUTHORIZATION : null),
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified super administrator basic authentication,
     * from specified super administrator profile name and secret,
     * to connect to specific super administrator profile, using HTTP basic format.
     *
     * Sending information array format:
     * @see getTabSndInfoWithParam() sending information array format.
     *
     * Return format:
     * @see getTabSndInfoWithParam() return format.
     *
     * @param string $strName
     * @param null|string $strSecret = null
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthSuperAdminBasic(
        $strName,
        $strSecret = null,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Init var
        $result = ToolBoxTable::getTabItemFromSrc(null, $tabInfo);
        $boolOnHeaderRequired = (is_bool($boolOnHeaderRequired) ? $boolOnHeaderRequired : true);

        // Set basic authentication, if required
        $strBasic = ToolBoxBasicAuthentication::getStrBasic($strName, $strSecret);
        if(!is_null($strBasic))
        {
            $result = static::getTabSndInfoWithParam(
                ConstSndInfo::AUTH_TYPE_SUPER_ADMIN_BASIC,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_TYPE : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_TYPE : null),
                $tabInfo
            );

            $result = static::getTabSndInfoWithParam(
                $strBasic,
                ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_AUTH_AUTHORIZATION : null),
                ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_AUTH_AUTHORIZATION : null),
                $result
            );
        }

        // Return result
        return $result;
    }



}