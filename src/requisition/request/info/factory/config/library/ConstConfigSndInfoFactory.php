<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\library;



class ConstConfigSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_SRC_CONFIG = 'objSrcConfig';



    // Exception message constants
    const EXCEPT_MSG_SRC_CONFIG_INVALID_FORMAT =
        'Following source configuration "%1$s" invalid! It must be a configuration object.';
    const EXCEPT_MSG_SRC_CONFIG_VALUE_INVALID_FORMAT =
        'Following value "%1$s" invalid, from source configuration!';



}