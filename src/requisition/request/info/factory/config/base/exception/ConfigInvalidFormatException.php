<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\base\exception;

use Exception;

use people_sdk\library\requisition\request\info\factory\config\base\library\ConstBaseConfigSndInfoFactory;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstBaseConfigSndInfoFactory::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $tabConfigKey = array(
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_GET_TIMEZONE_NAME_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_GET_DATETIME_FORMAT_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SET_TIMEZONE_NAME_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SET_DATETIME_FORMAT_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_NAME_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_AUTH_KEY_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_TYPE_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SRC_PATH_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SRC_PATH_SEPARATOR_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_TYPE_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_LOGIN_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_PW_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_API_KEY_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_TOKEN_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_NAME_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_SECRET_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_API_KEY_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_TOKEN_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_NAME_CONFIG_KEY,
            ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_SECRET_CONFIG_KEY
        );

        // Run each config
        $result = true;
        for($intCpt = 0; ($intCpt < count($tabConfigKey)) && $result; $intCpt++)
        {
            $strConfigKey = $tabConfigKey[$intCpt];
            $result =
                // Check valid config key
                (
                    (!isset($config[$strConfigKey])) ||
                    (
                        is_string($config[$strConfigKey]) &&
                        (trim($config[$strConfigKey]) != '')
                    )
                );
        };

        if($result)
        {
            // Init var
            $tabConfigKey = array(
                ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SUPPORT_TYPE,
                ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_SUPPORT_TYPE,
                ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SUPPORT_TYPE,
                ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE
            );

            // Run each config
            for($intCpt = 0; ($intCpt < count($tabConfigKey)) && $result; $intCpt++)
            {
                $strConfigKey = $tabConfigKey[$intCpt];
                $result =
                    // Check valid support type
                    (
                        (!isset($config[$strConfigKey])) ||
                        (
                            is_string($config[$strConfigKey]) &&
                            in_array(
                                $config[$strConfigKey],
                                ConstBaseConfigSndInfoFactory::getTabConfigSupportType()
                            )
                        )
                    );
            };
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}