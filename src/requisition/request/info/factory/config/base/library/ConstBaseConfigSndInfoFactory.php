<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\base\library;



class ConstBaseConfigSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_SND_INFO_CONFIG_KEY = 'snd_info_config_key';
    const TAB_CONFIG_KEY_LOC_SUPPORT_TYPE = 'loc_support_type';
    const TAB_CONFIG_KEY_LOC_GET_TIMEZONE_NAME_CONFIG_KEY = 'loc_get_timezone_name_config_key';
    const TAB_CONFIG_KEY_LOC_GET_DATETIME_FORMAT_CONFIG_KEY = 'loc_get_datetime_format_config_key';
    const TAB_CONFIG_KEY_LOC_SET_TIMEZONE_NAME_CONFIG_KEY = 'loc_set_timezone_name_config_key';
    const TAB_CONFIG_KEY_LOC_SET_DATETIME_FORMAT_CONFIG_KEY = 'loc_set_datetime_format_config_key';
    const TAB_CONFIG_KEY_SPACE_CONNECT_SUPPORT_TYPE = 'space_connect_support_type';
    const TAB_CONFIG_KEY_SPACE_CONNECT_NAME_CONFIG_KEY = 'space_connect_name_config_key';
    const TAB_CONFIG_KEY_SPACE_CONNECT_AUTH_KEY_CONFIG_KEY = 'space_connect_auth_key_config_key';
    const TAB_CONFIG_KEY_CONTENT_SUPPORT_TYPE = 'content_support_type';
    const TAB_CONFIG_KEY_CONTENT_TYPE_CONFIG_KEY = 'content_type_config_key';
    const TAB_CONFIG_KEY_CONTENT_SRC_PATH_CONFIG_KEY = 'content_src_path_config_key';
    const TAB_CONFIG_KEY_CONTENT_SRC_PATH_SEPARATOR_CONFIG_KEY = 'content_src_path_separator_config_key';
    const TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE = 'auth_support_type';
    const TAB_CONFIG_KEY_AUTH_TYPE_CONFIG_KEY = 'auth_type_config_key';
    const TAB_CONFIG_KEY_AUTH_USER_PROFILE_LOGIN_CONFIG_KEY = 'auth_user_profile_login_config_key';
    const TAB_CONFIG_KEY_AUTH_USER_PROFILE_PW_CONFIG_KEY = 'auth_user_profile_pw_config_key';
    const TAB_CONFIG_KEY_AUTH_USER_PROFILE_API_KEY_CONFIG_KEY = 'auth_user_profile_api_key_config_key';
    const TAB_CONFIG_KEY_AUTH_USER_PROFILE_TOKEN_CONFIG_KEY = 'auth_user_profile_token_config_key';
    const TAB_CONFIG_KEY_AUTH_APP_PROFILE_NAME_CONFIG_KEY = 'auth_app_profile_name_config_key';
    const TAB_CONFIG_KEY_AUTH_APP_PROFILE_SECRET_CONFIG_KEY = 'auth_app_profile_secret_config_key';
    const TAB_CONFIG_KEY_AUTH_APP_PROFILE_API_KEY_CONFIG_KEY = 'auth_app_profile_api_key_config_key';
    const TAB_CONFIG_KEY_AUTH_APP_PROFILE_TOKEN_CONFIG_KEY = 'auth_app_profile_token_config_key';
    const TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_NAME_CONFIG_KEY = 'auth_super_admin_profile_name_config_key';
    const TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_SECRET_CONFIG_KEY = 'auth_super_admin_profile_secret_config_key';

    // Support type configuration
    const CONFIG_SUPPORT_TYPE_URL_ARG = 'url_argument';
    const CONFIG_SUPPORT_TYPE_HEADER = 'header';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the base configuration sending information factory standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of support types.
     *
     * @return array
     */
    public static function getTabConfigSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_HEADER
        );

        // Return result
        return $result;
    }



}