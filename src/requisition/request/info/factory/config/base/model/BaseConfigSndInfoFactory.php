<?php
/**
 * Description :
 * This class allows to define base configuration sending information factory class.
 * Base configuration sending information factory uses basic values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * Base configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     snd_info_config_key(optional):
 *         "string configuration key, to get sending information",
 *
 *     loc_support_type(optional: got "header", if not found):
 *         "string support type, to set location parameters.
 *         Scope of available values: @see ConstBaseConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     loc_get_timezone_name_config_key(optional):
 *         "string configuration key, to get timezone name,
 *         used to get location datetime",
 *
 *     loc_get_datetime_format_config_key(optional):
 *         "string configuration key, to get datetime format,
 *         used to get location datetime",
 *
 *     loc_set_timezone_name_config_key(optional):
 *         "string configuration key, to get timezone name,
 *         used to set location datetime",
 *
 *     loc_set_datetime_format_config_key(optional):
 *         "string configuration key, to get datetime format,
 *         used to set location datetime",
 *
 *     space_connect_support_type(optional: got "header", if not found):
 *         "string support type, to set space connection parameters.
 *         Scope of available values: @see ConstBaseConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     space_connect_name_config_key(optional):
 *         "string configuration key, to get space name,
 *         from space to connect",
 *
 *     space_connect_auth_key_config_key(optional):
 *         "string configuration key, to get space authentication key,
 *         from space to connect",
 *
 *     content_support_type(optional: got "header", if not found):
 *         "string support type, to set content parameters.
 *         Scope of available values: @see ConstBaseConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     content_type_config_key(optional):
 *         "string configuration key, to get content type,
 *         used to set body payload",
 *
 *     content_src_path_config_key(optional):
 *         "string configuration key, to get content source path,
 *         used to set body payload",
 *
 *     content_src_path_separator_config_key(optional):
 *         "string configuration key, to get content source path separator,
 *         used to set body payload",
 *
 *     auth_support_type(optional: got "header", if not found):
 *         "string support type, to set authentication parameters.
 *         Scope of available values: @see ConstBaseConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     auth_type_config_key(optional):
 *         "string configuration key, to get authentication type",
 *
 *     auth_user_profile_login_config_key(optional):
 *         "string configuration key, to get authentication user profile login,
 *         from user profile to connect",
 *
 *     auth_user_profile_pw_config_key(optional):
 *         "string configuration key, to get authentication user profile password,
 *         from user profile to connect",
 *
 *     auth_user_profile_api_key_config_key(optional):
 *         "string configuration key, to get authentication user profile API key,
 *         from user profile to connect",
 *
 *     auth_user_profile_token_config_key(optional):
 *         "string configuration key, to get authentication user profile token,
 *         from user profile to connect"
 *
 *     auth_app_profile_name_config_key(optional):
 *         "string configuration key, to get authentication application profile name,
 *         from application profile to connect",
 *
 *     auth_app_profile_secret_config_key(optional):
 *         "string configuration key, to get authentication application profile secret,
 *         from application profile to connect",
 *
 *     auth_app_profile_api_key_config_key(optional):
 *         "string configuration key, to get authentication application profile API key,
 *         from application profile to connect",
 *
 *     auth_app_profile_token_config_key(optional):
 *         "string configuration key, to get authentication application profile token,
 *         from application profile to connect",
 *
 *     auth_super_admin_profile_name_config_key(optional):
 *         "string configuration key, to get authentication super administrator profile name,
 *         from super administrator profile to connect",
 *
 *     auth_super_admin_profile_secret_config_key(optional):
 *         "string configuration key, to get authentication super administrator profile secret,
 *         from super administrator profile to connect"
 * ]
 *
 * Configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <snd_info_config_key>(optional): [
 *         @see HttpRequest sending information array format
 *     ],
 *
 *     Value <loc_get_timezone_name_config_key>(optional):
 *         "string timezone name,
 *         used to get location datetime",
 *
 *     Value <loc_get_datetime_format_config_key>(optional):
 *         "string datetime format,
 *         used to get location datetime",
 *
 *     Value <loc_set_timezone_name_config_key>(optional):
 *         "string timezone name,
 *         used to set location datetime",
 *
 *     Value <loc_set_datetime_format_config_key>(optional):
 *         "string datetime format,
 *         used to set location datetime",
 *
 *     Value <space_connect_name_config_key>(optional):
 *         "string space name,
 *         from space to connect",
 *
 *     Value <space_connect_auth_key_config_key>(optional):
 *         "string space authentication key,
 *         from space to connect",
 *
 *     Value <content_type_config_key>(optional):
 *         "string content type,
 *         used to set body payload.
 *         Scope of available values:
 *             @see ConstSndInfo::getTabContentType()
 *             OR
 *             values matching with following REGEXP patterns: @see ConstSndInfo::getTabContentTypeRegexp()",
 *
 *     Value <content_src_path_config_key>(optional):
 *         "string content source path,
 *         used to set body payload",
 *
 *     Value <content_src_path_separator_config_key>(optional):
 *         "string content source path separator,
 *         used to set body payload",
 *
 *     Value <auth_type_config_key>(optional):
 *         "string authentication type.
 *         Scope of available values: @see ConstSndInfo::getTabAuthType()",
 *
 *     Value <auth_user_profile_login_config_key>(optional):
 *         "string authentication user profile login,
 *         from user profile to connect",
 *
 *     Value <auth_user_profile_pw_config_key>(optional):
 *         "string authentication user profile password,
 *         from user profile to connect",
 *
 *     Value <auth_user_profile_api_key_config_key>(optional):
 *         "string authentication user profile API key,
 *         from user profile to connect",
 *
 *     Value <auth_user_profile_token_config_key>(optional):
 *         "string authentication user profile token,
 *         from user profile to connect",
 *
 *     Value <auth_app_profile_name_config_key>(optional):
 *         "string authentication application profile name,
 *         from application profile to connect",
 *
 *     Value <auth_app_profile_secret_config_key>(optional):
 *         "string authentication application profile secret,
 *         from application profile to connect",
 *
 *     Value <auth_app_profile_api_key_config_key>(optional):
 *         "string authentication application profile API key,
 *         from application profile to connect",
 *
 *     Value <auth_app_profile_token_config_key>(optional):
 *         "string authentication application profile token,
 *         from application profile to connect",
 *
 *     Value <auth_super_admin_profile_name_config_key>(optional):
 *         "string authentication super administrator profile name,
 *         from super administrator profile to connect",
 *
 *     Value <auth_super_admin_profile_secret_config_key>(optional):
 *         "string authentication super administrator profile secret,
 *         from super administrator profile to connect"
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\base\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;
use people_sdk\library\requisition\request\info\library\ConstSndInfo;
use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\config\base\library\ConstBaseConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\config\base\exception\ConfigInvalidFormatException;



class BaseConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured sending information.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithSndInfo(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){return $value;},
            $tabInfo,
            function($value){return SndInfoInvalidFormatException::checkInfoIsValid($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured timezone name, used to get location datetime.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithLocGetTimezoneName(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_GET_TIMEZONE_NAME_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_GET_TIMEZONE_NAME_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_LOC_GET_TIMEZONE_NAME : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_LOC_GET_TIMEZONE_NAME : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured datetime format, used to get location datetime.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithLocGetDateTimeFormat(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_GET_DATETIME_FORMAT_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_GET_DATETIME_FORMAT_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_LOC_GET_DATETIME_FORMAT : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_LOC_GET_DATETIME_FORMAT : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured timezone name, used to set location datetime.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithLocSetTimezoneName(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SET_TIMEZONE_NAME_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SET_TIMEZONE_NAME_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_LOC_SET_TIMEZONE_NAME : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_LOC_SET_TIMEZONE_NAME : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured datetime format, used to set location datetime.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithLocSetDateTimeFormat(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SET_DATETIME_FORMAT_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SET_DATETIME_FORMAT_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_LOC_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_LOC_SET_DATETIME_FORMAT : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_LOC_SET_DATETIME_FORMAT : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured space name, from space to connect.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithSpaceConnectName(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_NAME_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_NAME_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_SPACE_CONNECT_NAME : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_SPACE_CONNECT_NAME : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured space authentication key, from space to connect.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithSpaceConnectAuthKey(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_AUTH_KEY_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_AUTH_KEY_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_CONNECT_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_SPACE_CONNECT_KEY : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_SPACE_CONNECT_KEY : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured content type.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithContentType(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_TYPE_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_TYPE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_CONTENT_TYPE : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_CONTENT_TYPE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    is_string($value) &&
                    (
                        in_array($value, ConstSndInfo::getTabContentType()) ||
                        (count(array_filter(
                            ConstSndInfo::getTabContentTypeRegexp(),
                            function($strPattern) use($value) {return (preg_match($strPattern, $value) == 1);}
                        )) > 0)
                    )
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured content source path, used to set body payload.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithContentSrcPath(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SRC_PATH_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SRC_PATH_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ? ConstSndInfo::HEADER_KEY_CONTENT_SRC_PATH : null),
                    ((!$boolOnHeaderRequired) ?  ConstSndInfo::URL_ARG_KEY_CONTENT_SRC_PATH : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured content source path separator, used to set body payload.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithContentSrcPathSeparator(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SRC_PATH_SEPARATOR_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SRC_PATH_SEPARATOR_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_CONTENT_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    $value,
                    ($boolOnHeaderRequired ?ConstSndInfo::HEADER_KEY_CONTENT_SRC_PATH_SEPARATOR : null),
                    ((!$boolOnHeaderRequired) ? ConstSndInfo::URL_ARG_KEY_CONTENT_SRC_PATH_SEPARATOR : null)
                );
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured user basic authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthUserBasic(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strLgKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_LOGIN_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_LOGIN_CONFIG_KEY] :
                null
        );
        $strPwKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_PW_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_PW_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            array($strLgKey, $strPwKey),
            function($strLg, $strPw) {
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthUserBasic($strLg, $strPw, $boolOnHeaderRequired);
            },
            $tabInfo,
            array(
                $strLgKey => function($strLg){return is_string($strLg);},
                $strPwKey => function($strPw){return (is_null($strPw) || is_string($strPw));}
            )
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured user API key authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthUserApiKey(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_API_KEY_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_API_KEY_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthUserApiKey($value, $boolOnHeaderRequired);
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured user bearer authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthUserBearer(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_TOKEN_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_USER_PROFILE_TOKEN_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthUserBearer($value, $boolOnHeaderRequired);
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured application basic authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthAppBasic(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strNmKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_NAME_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_NAME_CONFIG_KEY] :
                null
        );
        $strSecretKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_SECRET_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_SECRET_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            array($strNmKey, $strSecretKey),
            function($strNm, $strSecret) {
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthAppBasic($strNm, $strSecret, $boolOnHeaderRequired);
            },
            $tabInfo,
            array(
                $strNmKey => function($strNm){return is_string($strNm);},
                $strSecretKey => function($strSecret){return (is_null($strSecret) || is_string($strSecret));}
            )
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured application API key authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthAppApiKey(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_API_KEY_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_API_KEY_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthAppApiKey($value, $boolOnHeaderRequired);
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured application bearer authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthAppBearer(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_TOKEN_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_APP_PROFILE_TOKEN_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthAppBearer($value, $boolOnHeaderRequired);
            },
            $tabInfo,
            function($value){return is_string($value);}
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured super administrator basic authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuthSuperAdminBasic(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strNmKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_NAME_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_NAME_CONFIG_KEY] :
                null
        );
        $strSecretKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_SECRET_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPER_ADMIN_PROFILE_SECRET_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            array($strNmKey, $strSecretKey),
            function($strNm, $strSecret) {
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_SUPPORT_TYPE) ==
                    ConstBaseConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithAuthSuperAdminBasic($strNm, $strSecret, $boolOnHeaderRequired);
            },
            $tabInfo,
            array(
                $strNmKey => function($strNm){return is_string($strNm);},
                $strSecretKey => function($strSecret){return (is_null($strSecret) || is_string($strSecret));}
            )
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured authentication.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAuth(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_TYPE_CONFIG_KEY]) ?
                $tabConfig[ConstBaseConfigSndInfoFactory::TAB_CONFIG_KEY_AUTH_TYPE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $result = array();

                switch($value)
                {
                    case ConstSndInfo::AUTH_TYPE_USER_BASIC:
                        $result = $this->getTabSndInfoWithAuthUserBasic();
                        break;
                    case ConstSndInfo::AUTH_TYPE_USER_API_KEY:
                        $result = $this->getTabSndInfoWithAuthUserApiKey();
                        break;
                    case ConstSndInfo::AUTH_TYPE_USER_BEARER:
                        $result = $this->getTabSndInfoWithAuthUserBearer();
                        break;
                    case ConstSndInfo::AUTH_TYPE_APP_BASIC:
                        $result = $this->getTabSndInfoWithAuthAppBasic();
                        break;
                    case ConstSndInfo::AUTH_TYPE_APP_API_KEY:
                        $result = $this->getTabSndInfoWithAuthAppApiKey();
                        break;
                    case ConstSndInfo::AUTH_TYPE_APP_BEARER:
                        $result = $this->getTabSndInfoWithAuthAppBearer();
                        break;
                    case ConstSndInfo::AUTH_TYPE_SUPER_ADMIN_BASIC:
                        $result = $this->getTabSndInfoWithAuthSuperAdminBasic();
                        break;
                }

                return $result;
            },
            $tabInfo,
            function($value){
                return (
                    is_string($value) &&
                    in_array($value, ConstSndInfo::getTabAuthType())
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithSndInfo();
        $result = $this->getTabSndInfoWithLocGetTimezoneName($result);
        $result = $this->getTabSndInfoWithLocGetDateTimeFormat($result);
        $result = $this->getTabSndInfoWithLocSetTimezoneName($result);
        $result = $this->getTabSndInfoWithLocSetDateTimeFormat($result);
        $result = $this->getTabSndInfoWithSpaceConnectName($result);
        $result = $this->getTabSndInfoWithSpaceConnectAuthKey($result);
        $result = $this->getTabSndInfoWithContentType($result);
        $result = $this->getTabSndInfoWithContentSrcPath($result);
        $result = $this->getTabSndInfoWithContentSrcPathSeparator($result);
        $result = $this->getTabSndInfoWithAuth($result);

        // Return result
        return $result;
    }



}