<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\exception;

use Exception;

use liberty_code\config\config\api\ConfigInterface;
use people_sdk\library\requisition\request\info\factory\config\library\ConstConfigSndInfoFactory;



class SrcConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstConfigSndInfoFactory::EXCEPT_MSG_SRC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified configuration has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result = (
            (!is_null($config)) &&
			($config instanceof ConfigInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($config);
		}
		
		// Return result
		return $result;
    }
	
	
	
}