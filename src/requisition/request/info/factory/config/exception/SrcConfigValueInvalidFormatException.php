<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\exception;

use Exception;

use people_sdk\library\requisition\request\info\factory\config\library\ConstConfigSndInfoFactory;



class SrcConfigValueInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param string $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstConfigSndInfoFactory::EXCEPT_MSG_SRC_CONFIG_VALUE_INVALID_FORMAT,
            $strKey
        );
	}
	
	
	
}