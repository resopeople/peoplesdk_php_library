<?php
/**
 * Description :
 * This class allows to define default configuration sending information factory class.
 * Default configuration sending information factory uses source configuration object,
 * to provide HTTP request sending information.
 * Can be consider is base of all configuration sending information factory types.
 *
 * Default configuration sending information factory uses the following specified configuration:
 * [
 *     Default sending information factory
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\config\model;

use people_sdk\library\requisition\request\info\factory\model\DefaultSndInfoFactory;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\config\config\api\ConfigInterface;
use people_sdk\library\requisition\request\info\factory\config\library\ConstConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\config\exception\SrcConfigInvalidFormatException;
use people_sdk\library\requisition\request\info\factory\config\exception\SrcConfigValueInvalidFormatException;



/**
 * @method ConfigInterface getObjSrcConfig() Get source configuration object.
 * @method void setObjSrcConfig(ConfigInterface $objSrcConfig) Set source configuration object.
 */
abstract class DefaultConfigSndInfoFactory extends DefaultSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objSrcConfig
     */
    public function __construct(
        ConfigInterface $objSrcConfig,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init source configuration
        $this->setObjSrcConfig($objSrcConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstConfigSndInfoFactory::DATA_KEY_DEFAULT_SRC_CONFIG))
        {
            $this->__beanTabData[ConstConfigSndInfoFactory::DATA_KEY_DEFAULT_SRC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstConfigSndInfoFactory::DATA_KEY_DEFAULT_SRC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConfigSndInfoFactory::DATA_KEY_DEFAULT_SRC_CONFIG:
                    SrcConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get new or specified sending information array.
     * with specified value(s), from source configuration.
     *
     * In case of multiple values, all values provided are process together.
     * If at least one value is missing (not found on source configuration or on default values),
     * all values are not processed.
     *
     * Key format:
     * String key or index array of string keys, must be provided,
     * to retrieve one or multiple values, from source configuration.
     *
     * Get sending information callback format:
     * array function(mixed $value...):
     * Get sending information array,
     * with specified value(s), from source configuration.
     * In case of multiple values, each value are provided in same order like provided keys.
     *
     * Check is valid callback format:
     * Callable can be provided,
     * to check if specified value, from source configuration, is valid.
     * Null means no specific validation required.
     * - For specified value:
     * [
     *     "key 1": @see loadSubRepo() check is valid callback format (for all values)
     *     ...,
     *     "key N": ...
     * ]
     * - For all values:
     * boolean function(mixed $value).
     *
     * Default value array format:
     * Associative array of default values can be provided.
     * Default value is used, when not found in source configuration.
     * Key: string key => Value: mixed default value.
     *
     * @param string|array $key
     * @param callable $callableGetTabInfo
     * @param null|array $tabInfo = null
     * @param null|callable|array $checkIsValid = null
     * @param array $tabDefaultValue = array()
     * @return array
     * @throws SrcConfigValueInvalidFormatException
     */
    protected function getTabSndInfoWithSrcConfigValue(
        $key,
        callable $callableGetTabInfo,
        array $tabInfo = null,
        $checkIsValid = null,
        array $tabDefaultValue = array()
    )
    {
        // Init var
        $objSrcConfig = $this->getObjSrcConfig();
        $tabKey = (
            (
                is_array($key) &&
                (count(
                    $tabFormatKey = array_filter(
                        array_values($key),
                        function($strKey) {return is_string($strKey);}
                    )
                ) == count($key))
            ) ?
                $tabFormatKey :
                (is_string($key) ? array($key) : array())
        );
        $tabKey = (
            ((!is_array($key)) || (count($tabKey) == count($key))) ?
                $tabKey :
                array()
        );
        $checkIsValid = (
            is_array($checkIsValid) ?
                array_filter($checkIsValid, function($checkIsValid) {return is_callable($checkIsValid);}) :
                (is_callable($checkIsValid) ? $checkIsValid : null)
        );
        $result = ((!is_null($tabInfo)) ? $tabInfo : array());

        // Set values, if required
        if(count($tabKey) > 0)
        {
            // Init values
            $tabValue = array();
            $boolFound = false;
            for(
                $intCpt = 0;
                ($intCpt < count($tabKey)) &&
                (($intCpt == 0) || $boolFound);
                $intCpt++
            )
            {
                $strKey = $tabKey[$intCpt];
                $boolExistsOnConfig = $objSrcConfig->checkValueExists($strKey);
                $boolFound = ($boolExistsOnConfig || array_key_exists($strKey, $tabDefaultValue));

                // Set value, if required
                if($boolFound)
                {
                    // Get value
                    $value = (
                        $boolExistsOnConfig ?
                            $objSrcConfig->getValue($strKey) :
                            $tabDefaultValue[$strKey]
                    );

                    // Check valid value, if required
                    $boolValid = (
                        is_null($checkIsValid) ||
                        (is_callable($checkIsValid) && $checkIsValid($value)) ||
                        (
                            is_array($checkIsValid) &&
                            array_key_exists($strKey, $checkIsValid) &&
                            is_callable($checkKeyIsValid = $checkIsValid[$strKey]) &&
                            $checkKeyIsValid($value)
                        )
                    );
                    if(!$boolValid)
                    {
                        throw new SrcConfigValueInvalidFormatException($strKey);
                    }

                    // Set value
                    $tabValue[] = $value;
                }
            }

            // Set values on result, if required
            if($boolFound)
            {
                $tabInfoWithValue = call_user_func_array($callableGetTabInfo, $tabValue);
                $result = ToolBoxTable::getTabMerge($result, $tabInfoWithValue);
            }
        }

        // Return result
        return $result;
    }



}