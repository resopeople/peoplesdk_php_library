<?php
/**
 * Description :
 * This class allows to define default sending information factory class.
 * Can be consider is base of all sending information factory types.
 *
 * Default sending information factory uses the following specified configuration:
 * []
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\model;

use liberty_code\library\bean\model\FixBean;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;

use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;
use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\exception\ConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
abstract class DefaultSndInfoFactory extends FixBean implements SndInfoFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array engine.
     *
     * @return array
     */
    abstract protected function getTabSndInfoEngine();



    /**
     * @inheritdoc
     * @throws SndInfoInvalidFormatException
     */
    public function getTabSndInfo(array $tabInfo = null)
    {
        // Init var
        $result = ((!is_null($tabInfo)) ? $tabInfo : array());
        $result = ToolBoxSndInfo::getTabMergeSndInfo(
            $result,
            $this->getTabSndInfoEngine()
        );

        // Valid result
        SndInfoInvalidFormatException::setCheck($result);

        // Return result
        return $result;
    }



}