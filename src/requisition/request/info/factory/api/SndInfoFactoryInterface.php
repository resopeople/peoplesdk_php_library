<?php
/**
 * Description :
 * This class allows describe behavior of sending information factory class.
 * Sending information factory allows to provide new or specified HTTP request sending information,
 * enriched and/or formatted, from specific feature(s).
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\api;

use liberty_code\http\requisition\request\model\HttpRequest;



interface SndInfoFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get new or specified sending information array.
     *
     * Sending information array format:
     * Null or @see HttpRequest sending information array format.
     *
     * Return format:
     * @see HttpRequest sending information array format.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    public function getTabSndInfo(array $tabInfo = null);



}