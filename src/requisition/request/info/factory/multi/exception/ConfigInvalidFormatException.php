<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\multi\exception;

use Exception;

use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\request\info\factory\multi\library\ConstMultiSndInfoFactory;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiSndInfoFactory::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init sending information factory configuration array check function
        $checkTabSndInfoFactoryConfigIsValid = function($tabSndInfoFactoryConfig)
        {
            $result = is_array($tabSndInfoFactoryConfig) && (count($tabSndInfoFactoryConfig) > 0);

            // Check each sending information factory configuration valid, if required
            if($result)
            {
                $tabSndInfoFactoryConfig = array_values($tabSndInfoFactoryConfig);
                for($intCpt = 0; ($intCpt < count($tabSndInfoFactoryConfig)) && $result; $intCpt++)
                {
                    $sndInfoFactoryConfig = $tabSndInfoFactoryConfig[$intCpt];
                    $result = (
                        is_array($sndInfoFactoryConfig) &&

                        // Check valid sending information factory
                        isset($sndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY]) &&
                        (
                            ($sndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY] instanceof SndInfoFactoryInterface) ||
                            (
                                is_string($sndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY]) &&
                                (trim($sndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY]) != '')
                            )
                        ) &&

                        // Check valid order
                        (
                            (!isset($sndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_ORDER])) ||
                            is_int($sndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_ORDER])
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid sending information factory configuration
            isset($config[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY]) &&
            $checkTabSndInfoFactoryConfigIsValid($config[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY]);

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}