<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\multi\library;



class ConstMultiSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PROVIDER = 'objProvider';



    // Configuration
    const TAB_CONFIG_KEY_SND_INFO_FACTORY = 'snd_info_factory';
    const TAB_CONFIG_KEY_ORDER = 'order';



    // Exception message constants
    const EXCEPT_MSG_PROVIDER_INVALID_FORMAT = 'Following DI provider "%1$s" invalid! It must be a provider object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the multi sending information factory standard.';



}