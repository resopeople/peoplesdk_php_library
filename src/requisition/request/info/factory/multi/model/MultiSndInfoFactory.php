<?php
/**
 * Description :
 * This class allows to define multi sending information factory class.
 * Multi sending information factory uses list of sending information factories,
 * to provide HTTP request sending information.
 *
 * Multi sending information factory uses the following specified configuration:
 * [
 *     Default sending information factory,
 *
 *     snd_info_factory(required): [
 *         // Sending information factory 1
 *         [
 *             snd_info_factory(required):
 *                 object sending information factory interface 1
 *                 OR
 *                 "string sending information factory dependency key|class path",
 *
 *             order(optional: 0 got if not found): integer
 *         ],
 *
 *         ...,
 *
 *         // Sending information factory N
 *         ...
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\info\factory\multi\model;

use people_sdk\library\requisition\request\info\factory\model\DefaultSndInfoFactory;

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\multi\library\ConstMultiSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\multi\exception\ProviderInvalidFormatException;
use people_sdk\library\requisition\request\info\factory\multi\exception\ConfigInvalidFormatException;



/**
 * @method null|ProviderInterface getObjProvider() Get provider object.
 * @method void setObjProvider(null|ProviderInterface $objProvider) Set provider object.
 */
class MultiSndInfoFactory extends DefaultSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param ProviderInterface $objProvider = null
     */
	public function __construct(
        array $tabConfig = null,
        ProviderInterface $objProvider = null
    )
	{
		// Call parent constructor
		parent::__construct($tabConfig);

        // Init provider
        $this->setObjProvider($objProvider);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMultiSndInfoFactory::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstMultiSndInfoFactory::DATA_KEY_DEFAULT_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMultiSndInfoFactory::DATA_KEY_DEFAULT_PROVIDER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMultiSndInfoFactory::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of sending information factory configurations.
     *
     * @param null|boolean $sortAsc = null
     * @return array
     */
    protected function getTabSndInfoFactoryConfig($sortAsc = null)
    {
        // Init var
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);
        $tabConfig = $this->getTabConfig();
        $result = array_values($tabConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY]);

        // Sort configurations, if required
        if(is_bool($boolSortAsc))
        {
            usort(
                $result,
                function($sndInfoFactoryConfig1, $sndInfoFactoryConfig2)
                {
                    $intOrder1 = (
                        array_key_exists(ConstMultiSndInfoFactory::TAB_CONFIG_KEY_ORDER, $sndInfoFactoryConfig1) ?
                            $sndInfoFactoryConfig1[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $intOrder2 = (
                        array_key_exists(ConstMultiSndInfoFactory::TAB_CONFIG_KEY_ORDER, $sndInfoFactoryConfig2) ?
                            $sndInfoFactoryConfig2[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of sending information factory objects.
     *
     * @return SndInfoFactoryInterface[]
     * @throws ConfigInvalidFormatException
     */
    protected function getTabSndInfoFactory()
    {
        // Init var
        $objProvider = $this->getObjProvider();
        $tabConfig = $this->getTabConfig();
        $tabSndInfoFactoryConfig = $this->getTabSndInfoFactoryConfig(true);
        $result = array_map(
            function(array $tabSndInfoFactoryConfig) use ($objProvider, $tabConfig) {
                // Get sending information factory
                $result = $tabSndInfoFactoryConfig[ConstMultiSndInfoFactory::TAB_CONFIG_KEY_SND_INFO_FACTORY];
                $result = (
                    (is_string($result) && (!is_null($objProvider))) ?
                        $objProvider->get($result) :
                        $result
                );

                // Check valid sending information factory
                if(!($result instanceof SndInfoFactoryInterface))
                {
                    throw new ConfigInvalidFormatException(serialize($tabConfig));
                }

                // Return result
                return $result;
            },
            $tabSndInfoFactoryConfig
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = array();
        $tabSndInfoFactory = $this->getTabSndInfoFactory();

        // Run each sending information factory, and get sending information
        foreach($tabSndInfoFactory as $objSndInfoFactory)
        {
            $result = $objSndInfoFactory->getTabSndInfo($result);
        }

        // Return result
        return $result;
    }



}