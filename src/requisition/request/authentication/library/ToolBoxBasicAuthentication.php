<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\authentication\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxBasicAuthentication extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get basic value,
     * from specified identifier,
     * and specified secret.
     *
     * @param string $strIdentifier
     * @param string $strSecret = null
     * @return null|string
     */
    public static function getStrBasic($strIdentifier, $strSecret = null)
    {
        // Init var
        $strIdentifier = (is_string($strIdentifier) ? $strIdentifier : null);
        $strSecret = (is_string($strSecret) ? $strSecret : '');
        $result = (
            (!is_null($strIdentifier)) ?
                sprintf(
                    'Basic %1$s',
                    base64_encode(sprintf('%1$s:%2$s', $strIdentifier, $strSecret))
                ) :
                null
        );

        // Return result
        return $result;
    }



}