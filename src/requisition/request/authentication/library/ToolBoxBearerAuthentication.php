<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\authentication\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxBearerAuthentication extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get bearer value,
     * from specified token.
     *
     * @param string $strToken
     * @return null|string
     */
    public static function getStrBearer($strToken)
    {
        // Init var
        $strToken = (is_string($strToken) ? $strToken : null);
        $result = (
            (!is_null($strToken)) ?
                sprintf(
                    'Bearer %1$s',
                    base64_encode($strToken)
                ) :
                null
        );

        // Return result
        return $result;
    }



}