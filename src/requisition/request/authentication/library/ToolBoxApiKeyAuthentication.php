<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\requisition\request\authentication\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxApiKeyAuthentication extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get API key value,
     * from specified key.
     *
     * @param string $strKey
     * @return null|string
     */
    public static function getStrApiKey($strKey)
    {
        // Init var
        $strKey = (is_string($strKey) ? $strKey : null);
        $result = ((!is_null($strKey)) ? base64_encode($strKey) : null);

        // Return result
        return $result;
    }



}