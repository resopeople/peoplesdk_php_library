<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\library\table\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable as BaseToolBoxTable;



class ToolBoxTable extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified array of items,
     * from specified array of source items.
     *
     * Return format:
     * Null: If no items and no source provided.
     * or
     * Specified items array: If only items provided.
     * or
     * Specified source array: If only source provided.
     * or
     * Merged array, from specified items array and specified source array: If items and source provided.
     *
     * @param null|array $tabItem
     * @param null|array $tabSrcItem = null
     * @return null|array
     */
    public static function getTabItemFromSrc($tabItem, array $tabSrcItem = null)
    {
        // Init var
        $tabItem = (is_array($tabItem) ? $tabItem : null);
        $result = (
            (!is_null($tabItem)) ?
                (
                    (!is_null($tabSrcItem)) ?
                        BaseToolBoxTable::getTabMerge($tabSrcItem, $tabItem) :
                        $tabItem
                ) :
                ((!is_null($tabSrcItem)) ? $tabSrcItem : null)
        );

        // Return result
        return $result;
    }



}